<?php

use App\Http\Controllers\DataController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(DataController::class)->group(function () {
    Route::post('/check-stat', 'checkStatus');
    Route::post('/login', 'login');
    Route::post('/break', 'break');
    Route::post('/stop-break', 'unbreak');
    Route::post('/lainnya', 'lainnya');
    Route::post('/stop-lainnya', 'akhiriLainnya');
    Route::post('/logout', 'signout');
    Route::post('/change-pass', 'changePass');
    Route::post('/eat', 'eatTime');
    Route::post('/stop-eat', 'doneEatTime');
    Route::post('/break-w', 'breakW');
    Route::post('/stop-break-w', 'unbreakW');
    Route::post('/overtime', 'overtime');
});
