<?php

use App\Http\Controllers\CutiController;
use App\Http\Controllers\DivisiController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\IPController;
use App\Http\Controllers\LevelController;
use App\Http\Controllers\LogController;
use App\Http\Controllers\OTController;
use App\Http\Controllers\PerformanceController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
});

Auth::routes();

//FileDownload
Route::controller(FileController::class)->group(function () {
    Route::get('file-upload', 'index')->name('file.index');
    Route::post('store', 'store')->name('file.store');
    Route::get('delete/{path}/{filename}', 'delete')->name('file.delete');
    Route::get('download/{path}/{filename}', 'getFile')->name('file.download');
});

Route::get('/linkstorage', function () {
    Artisan::call('storage:link');
});

Route::group(['middleware' => ['auth','eligible','ipcheck']], function () {
    // Home
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    // User
    Route::controller(UserController::class)->group(function(){
        Route::get('/user/show', 'show')->name('changePass');

        Route::get('/user', 'index')->name('user.index');
        Route::get('/user/create', 'create')->name('user.create');
        Route::get('/user/{user}/edit', 'edit')->name('user.edit');
        Route::post('/user/store', 'store')->name('user.store');
        Route::put('/user/{user}/update', 'update')->name('user.update');
        // Route::get('/user/{user}/destroy', [App\Http\Controllers\UserController::class, 'destroy'])->name('user.destroy');
    });

    // User Cuti
    Route::controller(CutiController::class)->group(function () {
        Route::get('/cuti', 'index')->name('cuti.index');
        Route::get('/cuti/create', 'create')->name('cuti.create');
        Route::get('/cuti/{cuti}/edit', 'edit')->name('cuti.edit');
        Route::post('/cuti/store', 'store')->name('cuti.store');
        Route::put('/cuti/{cuti}/update', 'update')->name('cuti.update');
        Route::get('/cuti/{cuti}/approve', 'approve')->name('cuti.approve');
        Route::put('/cuti/{cuti}/reject', 'reject')->name('cuti.reject');
    });

    // Divisi
    Route::controller(DivisiController::class)->group(function () {
        Route::get('/divisi', 'index')->name('divisi.index');
        Route::get('/divisi/create', 'create')->name('divisi.create');
        Route::get('/divisi/{divisi}/edit', 'edit')->name('divisi.edit');
        Route::post('/divisi/store', 'store')->name('divisi.store');
        Route::put('/divisi/{divisi}/update', 'update')->name('divisi.update');
        Route::get('/divisi/{divisi}/destroy', 'destroy')->name('divisi.destroy');
    });

    // Level
    Route::controller(LevelController::class)->group(function () {
        Route::get('/level', 'index')->name('level.index');
        Route::get('/level/create', 'create')->name('level.create');
        Route::get('/level/{level}/edit', 'edit')->name('level.edit');
        Route::post('/level/store', 'store')->name('level.store');
        Route::put('/level/{level}/update', 'update')->name('level.update');
        Route::get('/level/{level}/destroy', 'destroy')->name('level.destroy');
    });
    

    // Log
    Route::controller(LogController::class)->group(function () {
        //Activity Log
        Route::get('/log', 'index')->name('log.index');

        //In-Out
        Route::get('/inoutlog', 'inoutLog')->name('inout.log');
        Route::get('/inoutsetting', 'inoutSetting')->name('inout.setting');
        Route::post('/inout/update', 'settingUpdate')->name('inout.update');
    });

    //Performance
    Route::controller(PerformanceController::class)->group(function () {
        Route::get('/performance', 'index')->name('perf.index');
        Route::get('/performance/create', 'create')->name('perf.create');
        Route::post('/performance/store', 'store')->name('perf.store');
    });

    //Whitelist IP
    Route::controller(IPController::class)->group(function () {
        Route::get('/whitelist', 'index')->name('wl.index');
        Route::post('/whitelist/store', 'store')->name('wl.store');
        Route::get('/whitelist/{whitelist}/destroy', 'destroy')->name('wl.destroy');
    });

    //OT
    Route::controller(OTController::class)->group(function () {
        Route::get('/overtime', 'index')->name('ot.index');
        Route::get('/overtime/{ot}/approve', 'approve')->name('ot.approve');
        Route::get('/overtime/{ot}/reject', 'reject')->name('ot.reject');
        Route::put('/overtime/{ot}/update', 'update')->name('ot.update');
        Route::post('/overtime/approveall', 'approveall')->name('ot.approveall');

        //temporary
        Route::get('/temporaryotthatonlyiknow', 'temporary')->name('ot.temporary');
        Route::put('/temporary/{ot}/update', 'temporaryupdate')->name('ot.temporaryupdate');

        Route::get('/notice', 'notice')->name('ot.notice');
    });
});
