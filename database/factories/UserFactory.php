<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => 'Admin',
            'username' => 'admin',
            'email' => 'admin@admin.com',
            'divisi' => 1,
            'bank' => 'BCA',
            'nomor_rekening' => "123-123-1231",
            'office' => 'GV',
            'gaji' => 7000000,
            'um_harian' => 30000,
            'subsidi_lainnya' => 300000,
            'status_level' => 1,
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'already_change_password' => 1,
            'status_work' => 1,
            'tanggal_gabung' => "2022-01-01 01:01:01",
            'remember_token' => Str::random(10),
            'cuti' => 12,
            'sisa_cuti' => 12
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return static
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
