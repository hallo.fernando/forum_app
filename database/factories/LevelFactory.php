<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class LevelFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => 'Administrator',
            'permission_view_home' => 1,
            'permission_edit_home' => 1,
            'permission_view_user' => 1,
            'permission_edit_user' => 1,
            'permission_view_user_tingkatan' => 1,
            'permission_edit_user_tingkatan' => 1,
            'permission_view_divisi' => 1,
            'permission_edit_divisi' => 1,
            'permission_view_log' => 1,
            'permission_edit_log' => 1,
            'permission_view_performance' => 1,
            'permission_edit_performance' => 1,
            'view_usercuti' => 1,
            'edit_usercuti' => 1,
            'view_forumsetting' => 1,
            'edit_forumsetting' => 1,
            'view_whitelist' => 1,
            'edit_whitelist' => 1,
            'view_inoutlog' => 1,
            'edit_inoutlog' => 1,
            'view_overtime' => 1,
            'edit_overtime' => 1
        ];
    }
}
