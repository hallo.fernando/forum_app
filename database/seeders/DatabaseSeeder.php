<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Divisi::factory()->create(['name'=> 'Operasional']);
        \App\Models\Level::factory()->create(['name'=>'Administrator',
        'permission_view_home' => 1,
        'permission_edit_home' => 1,
        'permission_view_user' => 1,
        'permission_edit_user' => 1,
        'permission_view_user_tingkatan' => 1,
        'permission_edit_user_tingkatan' => 1,
        'permission_view_divisi' => 1,
        'permission_edit_divisi' => 1,
        'permission_view_log' => 1,
        'permission_edit_log' => 1,
        'permission_view_performance' => 1,
        'permission_edit_performance' => 1,
        'view_usercuti' => 1,
        'edit_usercuti' => 1,
        'view_forumsetting' => 1,
        'edit_forumsetting' => 1,
        'view_whitelist' => 1,
        'edit_whitelist' => 1,
        'view_inoutlog' => 1,
        'edit_inoutlog' => 1,
        'view_overtime' => 1,
        'edit_overtime' => 1]);

        \App\Models\Level::factory()->create(['name'=>'User',
        'permission_view_home' => 1,
        'permission_edit_home' => 1,
        'permission_view_user' => 1,
        'permission_edit_user' => 0,
        'permission_view_user_tingkatan' => 1,
        'permission_edit_user_tingkatan' => 0,
        'permission_view_divisi' => 1,
        'permission_edit_divisi' => 0,
        'permission_view_log' => 1,
        'permission_edit_log' => 0,
        'permission_view_performance' => 1,
        'permission_edit_performance' => 0,
        'view_usercuti' => 1,
        'edit_usercuti' => 0,
        'view_forumsetting' => 1,
        'edit_forumsetting' => 0,
        'view_whitelist' => 0,
        'edit_whitelist' => 0,
        'view_inoutlog' => 1,
        'edit_inoutlog' => 0,
        'view_overtime' => 1,
        'edit_overtime' => 0]);
        
        \App\Models\User::factory(1)->create();
        
        \App\Models\ForumSetting::create([
            'name' => 'Operasional',
            'breakT' => 20,
            'breakW' => 20,
            'eatTime' => 30,
            'workHours' => 600,
            'overTime' => 660,
            'oBreakT' => 40,
            'oBreakW' => 40,
            'oEatTime' => 60,
            'divisi' => 1,
            'created_at' => '2022-01-01 00:00:00'
        ]);
    }
}
