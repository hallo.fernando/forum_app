<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collectlog', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('start')->nullable();
            $table->foreign('start')->references('id')->on('forumlog')->onDelete('cascade');
            $table->unsignedBigInteger('end')->nullable();
            $table->foreign('end')->references('id')->on('forumlog')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collect_forumlog_data');
    }
};
