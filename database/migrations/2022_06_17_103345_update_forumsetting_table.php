<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('forumsetting', function (Blueprint $table) {
            $table->Integer('overTime')->after('workHours')->nullable();
            $table->Integer('oBreakT')->after('overTime')->nullable();
            $table->Integer('oBreakW')->after('oBreakT')->nullable();
            $table->Integer('oEatTime')->after('oBreakW')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
