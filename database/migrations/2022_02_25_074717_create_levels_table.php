<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('level', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->boolean('permission_view_home');
            $table->boolean('permission_edit_home');
            $table->boolean('permission_view_user');
            $table->boolean('permission_edit_user');
            $table->boolean('permission_view_user_tingkatan');
            $table->boolean('permission_edit_user_tingkatan');
            $table->boolean('permission_view_divisi');
            $table->boolean('permission_edit_divisi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('level');
    }
};
