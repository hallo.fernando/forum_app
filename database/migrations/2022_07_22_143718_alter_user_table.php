<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->date('tanggal_gabung')->change();
            $table->date('tanggal_cuti')->change();
            $table->date('tanggal_resign')->change();
            if (Schema::hasColumn('user', 'hak_akses_fitur')) {
                $table->dropColumn('hak_akses_fitur');
            }
            if (Schema::hasColumn('user', 'status')) {
                $table->dropColumn('status');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
