<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usercuti', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade');
            $table->dateTime('from');
            $table->dateTime('till');
            $table->string('leaveDay')->nullable();
            $table->text('description');
            $table->boolean('isApproved');
            $table->string('approvedBy')->nullable();
            $table->boolean('isEdited');
            $table->string('editedBy')->nullable();
            $table->text('editReason')->nullable();
            $table->text('reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usercuti');
    }
};
