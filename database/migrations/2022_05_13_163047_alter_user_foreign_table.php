<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->dropForeign(['divisi']);
            $table->foreign('divisi')->references('id')->on('divisi');
            $table->dropForeign(['status_level']);
            $table->foreign('status_level')->references('id')->on('level');
            $table->dropForeign(['hak_akses_fitur']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
