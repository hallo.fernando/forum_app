<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('level', function (Blueprint $table) {
            $table->boolean('view_usercuti');
            $table->boolean('edit_usercuti');
            $table->boolean('view_forumsetting');
            $table->boolean('edit_forumsetting');
            $table->boolean('view_whitelist');
            $table->boolean('edit_whitelist');
            $table->boolean('view_inoutlog');
            $table->boolean('edit_inoutlog');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
