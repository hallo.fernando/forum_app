<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('forumsetting', function (Blueprint $table) {
            $table->dropColumn('check_in');
            $table->dropColumn('check_out');
            $table->dropColumn('break');
            $table->dropColumn('break_duration');
            $table->Integer('breakT')->after('name')->nullable();
            $table->Integer('breakW')->after('breakT')->nullable();
            $table->Integer('eatTime')->after('breakW')->nullable();
            $table->Integer('workHours')->after('eatTime')->nullable();
            $table->unsignedBigInteger('divisi')->after('workHours')->nullable();
            $table->foreign('divisi')->references('id')->on('divisi')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
