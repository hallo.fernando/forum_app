<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->string('bank')->nullable();
            $table->bigInteger('nomor_rekening')->nullable();
            $table->string('office')->nullable();
            $table->bigInteger('gaji')->nullable();
            $table->bigInteger('um_harian')->nullable();
            $table->bigInteger('subsidi_lainnya')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->dropColumn('bank');
            $table->dropColumn('nomor_rekening');
            $table->dropColumn('office');
            $table->dropColumn('gaji');
            $table->dropColumn('um_harian');
            $table->dropColumn('subsidi_lainnya');
        });
    }
};

