<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('collectlog', function (Blueprint $table) {
            $table->text('description')->after('actionBy')->nullable();
            $table->boolean('isEdited')->after('description');
            $table->string('editedBy')->after('isEdited')->nullable();
            $table->text('reason')->after('editedBy')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
