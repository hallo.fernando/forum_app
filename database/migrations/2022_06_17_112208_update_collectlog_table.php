<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('collectlog', function (Blueprint $table) {
            $table->boolean('isOTRequested')->after('end');
            $table->boolean('isOTApproved')->after('isOTRequested');
            $table->string('actionBy')->after('isOTApproved')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
