<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class ForumLog extends Model
{
    use HasFactory;

    protected $table = 'forumlog';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'auth_id',
        'description',
        'datetime',
        'type',
    ];

    public function GetUser(){
        return $this->belongsTo(User::class,'auth_id');
    }

    public function GetStart(){
        return $this->hasOne(CollectLog::class, 'start');
    }

    public function GetEnd(){
        return $this->hasOne(CollectLog::class, 'end');
    }
}
