<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Performance extends Model
{
    use HasFactory;

    protected $table = 'performance';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'auth_id',
        'user_id',
        'description',
        'type',
        'datetime',
    ];

    public function GetAuth(){
        return $this->belongsTo(User::class,'auth_id');
    }
    public function GetUser(){
        return $this->belongsTo(User::class,'user_id');
    }
}
