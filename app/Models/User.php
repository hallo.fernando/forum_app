<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'username',
        'password',
        'divisi',
        'status_level',
        'already_change_password',
        'status_work',
        'tanggal_gabung',
        'tanggal_resign',
        'tanggal_cuti',
        'bank',
        'nomor_rekening',
        'office',
        'gaji',
        'um_harian',
        'subsidi_lainnya',
        'cuti',
        'sisa_cuti'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function GetDivisi(){
        return $this->belongsTo(Divisi::class,'divisi');
    }
    public function GetLevel(){
        return $this->belongsTo(Level::class,'status_level');
    }
    public function forumlog(){
        return $this->hasMany(ForumLog::class);
    }
    public function applog(){
        return $this->hasMany(AppLog::class);
    }
    // public function GetShift(){
    //     return $this->belongsTo(ForumSetting::class, 'shift');
    // }
    public function performance(){
        return $this->hasMany(Performance::class);
    }
    public function GetDetails(){
        return $this->hasMany(UserDetails::class, 'user_id');
    }
}
