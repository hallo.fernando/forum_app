<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserCuti extends Model
{
    use HasFactory;

    protected $table = 'usercuti';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'from',
        'till',
        'leaveDay',
        'description',
        'isApproved',
        'approvedBy',
        'isEdited',
        'editedBy',
        'editReason',
        'reason',
    ];

    public function GetUser(){
        return $this->belongsTo(User::class,'user_id');
    }
}
