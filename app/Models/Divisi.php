<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Divisi extends Model
{
    use HasFactory;

    protected $table = 'divisi';
    protected $primaryKey = 'id';

        /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'createdBy',
        'updatedBy'
    ];

    public function users(){
        return $this->hasMany(User::class);
    }

    public function userCreate(){
        return $this->belongsTo(User::class, 'createdBy');
    }

    public function userUpdate(){
        return $this->belongsTo(User::class, 'updatedBy');
    }

    public function GetSetting(){
        return $this->hasOne(ForumSetting::class, 'divisi');
    }

    // public function levels(){
    //     return $this->hasMany(Level::class, 'divisi');
    // }

}
