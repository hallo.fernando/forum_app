<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ForumSetting extends Model
{
    use HasFactory;

    protected $table = 'forumsetting';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'breakT',
        'breakW',
        'eatTime',
        'workHours',
        'overTime',
        'oBreakT',
        'oBreakW',
        'oEatTime',
        'divisi',
        'created_at'
    ];

    public function GetDivisi(){
        return $this->belongsTo(Divisi::class, 'divisi');
    }

    // public function users(){
    //     return $this->hasMany(User::class, 'shift');
    // }
}
