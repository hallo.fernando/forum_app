<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    use HasFactory;

    protected $table = 'level';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'permission_view_home',
        'permission_edit_home',
        'permission_view_user',
        'permission_edit_user',
        'permission_view_user_tingkatan',
        'permission_edit_user_tingkatan',
        'permission_view_divisi',
        'permission_edit_divisi',
        'permission_view_log',
        'permission_edit_log',
        'permission_view_performance',
        'permission_edit_performance',
        'view_usercuti',
        'edit_usercuti',
        'view_forumsetting',
        'edit_forumsetting',
        'view_whitelist',
        'edit_whitelist',
        'view_inoutlog',
        'edit_inoutlog',
        'view_overtime',
        'edit_overtime',
        // 'divisi',
    ];

    public function users()
    {
        return $this->hasMany(User::class);
    }

    // public function GetDivisi(){
    //     return $this->belongsTo(Divisi::class, 'divisi');
    // }
}
