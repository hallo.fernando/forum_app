<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CollectLog extends Model
{
    use HasFactory;

    protected $table = 'collectlog';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'start',
        'end',
        'isOTRequested',
        'isOTApproved',
        'isStaffAcc',
        'isLeaderAcc',
        'actionBy',
        'isEdited',
        'editedBy',
        'reason',
    ];

    public function CollectStart(){
        return $this->belongsTo(ForumLog::class,'start');
    }

    public function CollectEnd(){
        return $this->belongsTo(ForumLog::class,'end');
    }
}
