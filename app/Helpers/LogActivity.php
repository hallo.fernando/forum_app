<?php

namespace App\Helpers;
use Request;
use App\Models\AppLog as AppLog;
use Auth;
Use Carbon\Carbon;

class LogActivity
{
    public static function addToLog($desc, $type)
    {
        $log = [];
        $log['auth_id'] = Auth::check() ? Auth::user()->id : 1;
        $log['description'] = $desc;
        $log['type'] = $type;
        $log['datetime'] = Carbon::now()->format('Y-m-d H:i:s');
        AppLog::create($log);
    }
}