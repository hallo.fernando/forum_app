<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Performance;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use Auth;

class PerformanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $type = request('type');
        $Sdesc = request('Sdesc');
        $start_date = Carbon::parse(request()->start_date)
            ->startofDay()
            ->toDateTimeString();
        $end_date = Carbon::parse(request()->end_date)
            ->endOfDay()
            ->toDateTimeString();

        $performance = Performance::select('*')
            ->whereHas('GetAuth', function ($query) {
                $Sauth = (request('Sauth'));
                $query->where('name', 'LIKE', "%{$Sauth}%");
            })
            ->whereHas('GetUser', function ($query) {
                $Suser = (request('Suser'));
                $query->where('name', 'LIKE', "%{$Suser}%");
            })
            ->where('type', 'LIKE', $type)
            ->where('description', 'LIKE', "%{$Sdesc}%")
            ->orderByDesc('datetime');

        if (request()->start_date || request()->end_date) {
            $performance->whereBetween('datetime', [$start_date, $end_date]);
            $performance = $performance->get();
            return view('performance.index', compact('performance'));
        }

        $performance = $performance->get();

        return view('performance.index', compact('performance'));
    }

    public function create()
    {
        $user = User::all();
        return view('performance.create', compact('user'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user' => 'required',
            'type' => 'required',
            'description' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        if ($request->user == Auth::user()->id) {
            return redirect()->route('perf.create')->with(['msg' => "You can't judge yourself", 'class' => 'error']);
        }
        Performance::create([
            'auth_id' => $request->auth,
            'user_id' => $request->user,
            'description' => $request->description,
            'type' => $request->type,
            'datetime' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $findauth = User::select('name')->where('id', $request->auth)->first();
        $finduser = User::select('name')->where('id', $request->user)->first();
        $authname = $findauth->name;
        $username = $finduser->name;

        \LogActivity::addToLog("New Performance Report from: [" . $authname . "] to [" . $username . "].", 1);

        return redirect()->route('perf.create')->with(['msg' => 'Performance Created Successfully!', 'class' => 'success']);
    }
}
