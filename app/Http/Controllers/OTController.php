<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\CollectLog;
use App\Models\Divisi;
use Auth;
use Carbon\Carbon;

class OTController extends Controller
{
    public function notice()
    {
        $overtime = DB::table('collectlog')
            ->join('forumlog as t1', 't1.id', '=', 'collectlog.start')
            ->leftJoin('forumlog as t2', 't2.id', '=', 'collectlog.end')
            ->join('user', 'user.id', '=', 't1.auth_id')
            ->join('divisi', 'divisi.id', '=', 'user.divisi')
            ->select('collectlog.*', 't1.datetime as d1', 't2.datetime as d2', 'user.divisi', 'user.id as uid', 'user.name as n1', 'divisi.name as n2')
            ->where('collectlog.isOTRequested', 1)
            ->where('collectlog.actionBy', null)
            ->orderByDesc('collectlog.id');

        $overtimefull = $overtime->get();

        $overtimedetail = $overtime->groupBy('uid')->take(3)->get();

        return view('layouts.notifdetail', compact('overtimefull', 'overtimedetail'));
    }

    public function temporary()
    {
        $overtime = DB::table('collectlog')
            ->join('forumlog as t1', 't1.id', '=', 'collectlog.start')
            ->leftJoin('forumlog as t2', 't2.id', '=', 'collectlog.end')
            ->join('user', 'user.id', '=', 't1.auth_id')
            ->join('divisi', 'divisi.id', '=', 'user.divisi')
            ->join('level', 'level.id', '=', 'user.status_level')
            ->select('collectlog.*', 't1.datetime as d1', 't2.datetime as d2', 'user.divisi', 'user.name as n1', 'divisi.name as n2', 'level.name as n3')
            ->where('collectlog.isOTRequested', 1)
            ->orderByDesc('collectlog.id');

        $overtime = $overtime->get();

        return view('monitoring.temporaryOT', compact('overtime'));
    }

    public function temporaryupdate(Request $request, CollectLog $ot)
    {
        $ot->isOTApproved = 1;
        $ot->isLeaderAcc = $request->LeaderAcc;
        $ot->isStaffAcc = $request->StaffAcc;

        $ot->save();

        return redirect()->route('ot.temporary')->with(['msg' => 'Overtime Updated!', 'class' => 'success']);
    }

    public function index()
    {
        $start_date = Carbon::parse(request()->start_date)
            ->startofDay()
            ->toDateTimeString();
        $end_date = Carbon::parse(request()->end_date)
            ->endOfDay()
            ->toDateTimeString();

        $divisi = Divisi::all();

        $Sname = (request('Sname'));
        $Sdivisi = (request('Sdivisi'));

        $overtime = DB::table('collectlog')
            ->join('forumlog as t1', 't1.id', '=', 'collectlog.start')
            ->leftJoin('forumlog as t2', 't2.id', '=', 'collectlog.end')
            ->join('user', 'user.id', '=', 't1.auth_id')
            ->join('divisi', 'divisi.id', '=', 'user.divisi')
            ->join('level', 'level.id', '=', 'user.status_level')
            ->select('collectlog.*', 't1.datetime as d1', 't2.datetime as d2', 'user.divisi', 'user.name as n1', 'divisi.name as n2', 'level.name as n3')
            ->where('collectlog.isOTRequested', 1)
            ->where('user.name', 'LIKE', "%{$Sname}%")
            ->where('divisi.name', 'LIKE', "%{$Sdivisi}%")
            ->orderByDesc('collectlog.id');

        if (request()->start_date || request()->end_date) {
            $overtime = $overtime->whereBetween('t1.datetime', [$start_date, $end_date])
                ->get();

            return view('monitoring.indexOT', compact('overtime', 'divisi'));
        }

        $overtime = $overtime->get();

        return view('monitoring.indexOT', compact('overtime', 'divisi'));
    }

    public function approveall(Request $request)
    {
        $id = $request->id;
        $OTName = [];
        if ($request->id) {
            foreach ($id as $collectId) {
                $cLog = CollectLog::find($collectId);
                $cLog->isOTApproved = 1;
                $cLog->isLeaderAcc = 1;
                $cLog->actionBy = Auth::user()->name;
                $cLog->save();
                array_push($OTName, $cLog->CollectStart->GetUser->name);
            }
        }
        $OTName = array_unique($OTName);
        $impOTName = implode(', ', $OTName);

        if ($impOTName) {
            \LogActivity::addToLog("Request OT [" . $impOTName . "] Approved.", 3);
        }

        return redirect()->route('ot.index')->with(['msg' => 'Overtime Approved!', 'class' => 'success', 'modal_open' => 1]);
    }

    public function approve(Request $request, CollectLog $ot)
    {
        $ot->isOTApproved = 1;
        $ot->isLeaderAcc = 1;
        $ot->actionBy = Auth::user()->name;
        $ot->save();

        \LogActivity::addToLog("Request OT [" . $ot->CollectStart->GetUser->name . "] Approved.", 3);

        return redirect()->route('ot.index')->with(['msg' => 'Overtime Approved!', 'class' => 'success', 'modal_open' => 1]);
    }

    public function reject(Request $request, CollectLog $ot)
    {
        $ot->isOTApproved = 1;
        $ot->isLeaderAcc = 0;
        $ot->actionBy = Auth::user()->name;
        $ot->save();

        \LogActivity::addToLog("Request OT [" . $ot->CollectStart->GetUser->name . "] Rejected.", 4);

        return redirect()->route('ot.index')->with(['msg' => 'Overtime Rejected!', 'class' => 'error', 'modal_open' => 1]);
    }

    public function update(Request $request, CollectLog $ot)
    {
        //OLD DATA DETAILS
        if ($ot->isStaffAcc == $ot->isLeaderAcc) {
            if ($ot->isLeaderAcc == 1 && $ot->isStaffAcc == 1) {
                $oldotdetail = "Approved";
            } else {
                $oldotdetail = "Rejected";
            }
        } else {
            $oldotdetail = "Not Match";
        }
        //END OF OLD DATA
        $ot->isOTApproved = 1;
        if ($request->isOTPaid == 1) {
            $ot->isLeaderAcc = 1;
            $ot->isStaffAcc = 1;
            $otdetail = "Approved";
        } else {
            $ot->isLeaderAcc = 0;
            $ot->isStaffAcc = 0;
            $otdetail = "Rejected";
        }
        $ot->editedBy = Auth::user()->name;
        $ot->isEdited = 1;
        $ot->reason = $request->reason;
        $ot->save();

        \LogActivity::addToLog("Update Request OT [" . $ot->CollectStart->GetUser->name . "]-> '" . $oldotdetail . "' to '" . $otdetail . "'.", 2);

        return redirect()->route('ot.index')->with(['msg' => 'Overtime Updated!', 'class' => 'success']);
    }
}
