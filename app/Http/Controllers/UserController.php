<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserStoreRequest;
use App\Models\Divisi;
use App\Models\Level;
use App\Models\User;
use App\Models\UserDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $divisi = Divisi::all();

        $users = User::where('name', 'LIKE', "%{$request->Sname}%")
            ->where('status_work', 'LIKE', "%{$request->Sstatus}%");
        if (request('Sdivisi')) {
            $users->whereHas('GetDivisi', function ($query) use ($request) {
                $query->where('name', 'LIKE', "%{$request->Sdivisi}%");
            });
        }

        //Search Query Join Date
        if (request('daterange')) {
            $daterange = explode(' ', request('daterange'));
            $start_date = Carbon::parse($daterange[0])->toDateString();
            $end_date = Carbon::parse($daterange[2])->toDateString();

            $users->whereBetween('tanggal_gabung', [$start_date, $end_date]);
            $users = $users->get();

            return view('user.index', compact('users', 'divisi'));
        }

        $users = $users->get();

        return view('user.index', compact('users', 'divisi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $divisi = Divisi::select('id', 'name')->latest()->get();
        $level = Level::select('id', 'name')->latest()->get();

        return view('user.create', compact('divisi', 'level'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'password' => Hash::make($request->password),
            'divisi' => $request->divisi_id,
            'status_level' => $request->level_id,
            'bank' => $request->bank,
            'gaji' => str_replace(',', '', $request->gaji),
            'nomor_rekening' => $request->nomor_rekening,
            'office' => $request->office,
            'um_harian' => str_replace(',', '', $request->um_harian),
            'subsidi_lainnya' => str_replace(',', '', $request->subsidi_lainnya),
            'status_work' => 1,
            'already_change_password' => 0,
            'tanggal_gabung' => $request->tanggal_gabung,
            'cuti' => $request->cuti,
            'sisa_cuti' => $request->cuti
        ]);

        \LogActivity::addToLog("Create New User [" . $request->name . "].", 1);

        return redirect()->route('user.create')->with(['msg' => 'User Created Successfully!', 'class' => 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $users)
    {
        return view('user.change', compact('users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $divisi = Divisi::select('id', 'name')->latest()->get();
        $level = Level::select('id', 'name')->latest()->get();

        $upperName = Str::upper($user->username);

        if ($upperName == 'ADMIN') {
            if (Auth::user()->id != $user->id) {
                return back()->with(['msg' => "You can't edit an Administrator", 'class' => 'error']);
            }
        }
        return view('user.edit', compact('user', 'divisi', 'level'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\staff  $staff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //Change Password
        if ($request->changepassword) {
            if ($request->password && $request->conf_password) {
                if ($request->password == $request->conf_password) {
                    $user->update([
                        'password' => Hash::make($request->password),
                        'already_change_password' => 1,
                    ]);

                    \LogActivity::addToLog("Change password for the first time.", 2);

                    return redirect()->route('home');
                } else {
                    $errors = ['password' => 'Password not matched!'];
                    return back()->withErrors($errors);
                }
            }
        }

        //Validation
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:4',
            'email' => 'required|string|email|unique:user,email,' . $user->id,
        ], [
            'email.unique' => 'Email already used'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        //Activity Log Text
        if ($request->name != $user->name) {
            $uName = " Name '" . $user->name . "' to '" . $request->name . "',";
        } else {
            $uName = '';
        }
        if ($request->password) {
            $uPass = " Reset Password,";
        } else {
            $uPass = '';
        }
        if ($request->tanggal_gabung != $user->tanggal_gabung) {
            $uJoin = " Join Date '" . Carbon::parse($user->tanggal_gabung)->isoFormat('DD-MMMM-Y') . "' to '" . Carbon::parse($request->tanggal_gabung)->isoFormat('DD-MMMM-Y') . "',";
        } else {
            $uJoin = '';
        }
        if ($request->email != $user->email) {
            $uEmail = " Email '" . $user->email . "' to '" . $request->email . "',";
        } else {
            $uEmail = '';
        }
        if ($request->cuti != $user->cuti) {
            $cutiday = " Cuti '" . $user->cuti . " days' to '" . $request->cuti . " days',";
        } else {
            $cutiday = '';
        }
        if ($request->divisi_id != $user->divisi) {
            $findOldDivisi = Divisi::select('name')->where('id', $user->divisi)->first();
            $findDivisi = Divisi::select('name')->where('id', $request->divisi_id)->first();
            $divisi = " Divisi '" . $findOldDivisi->name . "' to '" . $findDivisi->name . "',";
        } else {
            $divisi = '';
        }
        if ($request->level_id != $user->status_level) {
            $findOldLevel = Level::select('name')->where('id', $user->status_level)->first();
            $findLevel = Level::select('name')->where('id', $request->level_id)->first();
            $level = " Posisi '" . $findOldLevel->name . "' to '" . $findLevel->name . "',";
        } else {
            $level = '';
        }
        if ($request->bank != $user->bank) {
            $bank = " Bank '" . $user->bank . "' to '" . $request->bank . "',";
        } else {
            $bank = '';
        }
        if ($request->office != $user->office) {
            $office = " Office '" . $user->office . "' to '" . $request->office . "',";
        } else {
            $office = '';
        }
        if ($request->nomor_rekening != $user->nomor_rekening) {
            $rekening = " Rekening Number '" . $user->nomor_rekening . "' to '" . $request->nomor_rekening . "',";
        } else {
            $rekening = '';
        }
        if ($request->gaji != number_format($user->gaji)) {
            $gaji = " Salary 'Rp. " . number_format($user->gaji) . "' to 'Rp. " . $request->gaji . "',";
        } else {
            $gaji = '';
        }
        if ($request->um_harian != number_format($user->um_harian)) {
            $uangMakan = " UM/Daily 'Rp. " . number_format($user->um_harian) . "' to 'Rp. " . $request->um_harian . "',";
        } else {
            $uangMakan = '';
        }
        if ($request->subsidi_lainnya != number_format($user->subsidi_lainnya)) {
            $subsidi = " Subsidi Lainnya 'Rp. " . number_format($user->subsidi_lainnya) . "' to 'Rp. " . $request->subsidi_lainnya . "',";
        } else {
            $subsidi = '';
        }
        if ($request->status_work != $user->status_work) {
            if ($user->status_work == 0) {
                $uStatusWork = "Nonaktif";
            }
            if ($user->status_work == 1) {
                $uStatusWork = "Aktif";
            }
            if ($user->status_work == 2) {
                $uStatusWork = "Cuti";
            }
            if ($user->status_work == 3) {
                $uStatusWork = "Resign";
            }
            if ($request->status_work == 0) {
                $rStatusWork = "Nonaktif";
            }
            if ($request->status_work == 1) {
                $rStatusWork = "Aktif";
            }
            if ($request->status_work == 2) {
                $rStatusWork = "Cuti";
            }
            if ($request->status_work == 3) {
                $rStatusWork = "Resign";
            }
            $uSwork = " Status Work '" . $uStatusWork . "' to '" . $rStatusWork . "',";
        } else {
            $uSwork = '';
        }
        //End of Log Text

        //UserDetails
        if ($request->divisi_id != $user->divisi) {
            $findOldDivisi = Divisi::select('name')->where('id', $user->divisi)->first();
            $findNewDivisi = Divisi::select('name')->where('id', $request->divisi_id)->first();
            $userdetails = UserDetails::select('*')->where('user_id', $user->id)->where('type', 1)->first();
            if (empty($userdetails)) {
                if ($user->divisi == NULL) {
                    UserDetails::create([
                        'name' => $findNewDivisi->name,
                        'user_id' => $user->id,
                        'datetime' => Carbon::now()->format('Y-m-d H:i:s'),
                        'type' => 1
                    ]);
                } else {
                    UserDetails::create([
                        'name' => $findOldDivisi->name,
                        'user_id' => $user->id,
                        'datetime' => $user->tanggal_gabung,
                        'type' => 1
                    ]);
                    UserDetails::create([
                        'name' => $findNewDivisi->name,
                        'user_id' => $user->id,
                        'datetime' => Carbon::now()->format('Y-m-d H:i:s'),
                        'type' => 1
                    ]);
                }
            } else {
                UserDetails::create([
                    'name' => $findNewDivisi->name,
                    'user_id' => $user->id,
                    'datetime' => Carbon::now()->format('Y-m-d H:i:s'),
                    'type' => 1
                ]);
            }
        }

        if ($request->level_id != $user->status_level) {
            $findOldLevel = Level::select('name')->where('id', $user->status_level)->first();
            $findNewLevel = Level::select('name')->where('id', $request->level_id)->first();
            $userdetails = UserDetails::select('*')->where('user_id', $user->id)->where('type', 2)->first();
            if (empty($userdetails)) {
                if ($user->status_level == NULL) {
                    UserDetails::create([
                        'name' => $findNewLevel->name,
                        'user_id' => $user->id,
                        'datetime' => Carbon::now()->format('Y-m-d H:i:s'),
                        'type' => 2
                    ]);
                } else {
                    UserDetails::create([
                        'name' => $findOldLevel->name,
                        'user_id' => $user->id,
                        'datetime' => $user->tanggal_gabung,
                        'type' => 2
                    ]);
                    UserDetails::create([
                        'name' => $findNewLevel->name,
                        'user_id' => $user->id,
                        'datetime' => Carbon::now()->format('Y-m-d H:i:s'),
                        'type' => 2
                    ]);
                }
            } else {
                UserDetails::create([
                    'name' => $findNewLevel->name,
                    'user_id' => $user->id,
                    'datetime' => Carbon::now()->format('Y-m-d H:i:s'),
                    'type' => 2
                ]);
            }
        }
        //End of UserDetails

        $user->name = $request->name;
        $user->email = $request->email;
        $user->tanggal_gabung = $request->tanggal_gabung;
        $user->divisi = $request->divisi_id;
        $user->status_level = $request->level_id;
        $user->bank = $request->bank;
        $user->office = $request->office;
        $user->nomor_rekening = $request->nomor_rekening;
        $user->gaji = str_replace(',', '', $request->gaji);
        $user->um_harian = str_replace(',', '', $request->um_harian);
        $user->subsidi_lainnya = str_replace(',', '', $request->subsidi_lainnya);
        $user->status_work = $request->status_work;
        $user->cuti = $request->cuti;

        if ($user->sisa_cuti == NULL) {
            $user->sisa_cuti = $request->cuti;
        }

        if ($request->password) {
            $user->password = Hash::make($request->password);
        }

        if ($request->status_work != $user->status_work) {
            if ($request->status_work == 2) {
                $user->tanggal_cuti = Carbon::now()->format('Y-m-d H:i:s');
            }
            if ($request->status_work == 3) {
                $user->tanggal_resign = Carbon::now()->format('Y-m-d H:i:s');
            }
        }

        if ($user->isDirty()) {
            $user->save();
            \LogActivity::addToLog("Update Data User [" . $request->name . "] ->" . $uSwork . $uName . $uPass . $uEmail . $divisi . $level . $uJoin . $cutiday . $bank . $office . $rekening . $gaji . $uangMakan . $subsidi, 2);
        }

        return redirect()->route('user.edit', $user->id)->with(['msg' => 'User Updated Successfully!', 'class' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\staff  $staff
     * @return \Illuminate\Http\Response
     */
    // public function destroy(User $user)
    // {
    //     $user->delete();
    //     return redirect()->route('user.index')->with('message', ['text' => __('user.status4'), 'class' => 'success']);
    // }
}
