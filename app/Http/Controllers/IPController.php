<?php

namespace App\Http\Controllers;

use App\Models\WhitelistedIp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class IPController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $whitelist = WhitelistedIp::all();
        return view('whitelist.index', compact('whitelist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ips' => [
                'required',
                'regex:/^(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]?|0)\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]?|0)\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]?|0)\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]?|0)(\/([0-9]|[1-2][0-9]|3[0-2])){0,1}$/'
            ]
        ], [
            'ips.regex' => "The IP's must be a valid IPv4 address."
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $noRange = strpos($request->ips, "/");
        if ($noRange == false) {
            WhitelistedIp::create([
                'ips' => $request->ips . "/32"
            ]);
        } else {
            WhitelistedIp::create([
                'ips' => $request->ips
            ]);
        }

        \LogActivity::addToLog("Create New Whitelist IP [" . $request->ips . "].", 1);

        return redirect()->route('wl.index')->with(['msg' => 'IP Added Successfully!', 'class' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Divisi  $divisi
     * @return \Illuminate\Http\Response
     */
    public function destroy(WhitelistedIp $whitelist)
    {
        $whitelist->delete();
        $noRangeIP = str_replace('/32', '', $whitelist->ips);
        \LogActivity::addToLog("Delete Whitelist IP [" . $noRangeIP . "].", 2);
        return redirect()->route('wl.index')->with(['msg' => 'IP Deleted Successfully!', 'class' => 'success']);
    }
}
