<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Divisi;
use App\Models\AppLog;
use App\Models\ForumLog;
use App\Models\CollectLog;
use App\Models\ForumSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\DB;
use Auth;

class LogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $Sdesc = request('Sdesc');
        $start_date = Carbon::parse(request()->start_date)
            ->startofDay()
            ->toDateTimeString();
        $end_date = Carbon::parse(request()->end_date)
            ->endOfDay()
            ->toDateTimeString();

        $applog = AppLog::select('*')
            ->whereHas('GetUser', function ($query) {
                $Sname = (request('Sname'));
                $query->where('name', 'LIKE', "%{$Sname}%");
            })
            ->where('description', 'LIKE', "%{$Sdesc}%")
            ->orderByDesc('datetime');

        if (request()->start_date || request()->end_date) {
            $applog->whereBetween('datetime', [$start_date, $end_date]);
            $applog = $applog->get();
            return view('log.index', compact('applog'));
        }

        $applog = $applog->get();

        return view('log.index', compact('applog'));
    }

    public function inoutLog()
    {
        $start_date = Carbon::parse(request()->start_date)
            ->startofDay()
            ->toDateTimeString();
        $end_date = Carbon::parse(request()->end_date)
            ->endOfDay()
            ->toDateTimeString();

        $divisi = Divisi::all();

        $Sname = (request('Sname'));
        $Sdivisi = (request('Sdivisi'));

        $chk = DB::table('collectlog')
            ->select('collectlog.*', 't1.datetime as d1', 't1.auth_id', 't2.datetime as d2', 'user.name as n1', 'divisi.name as n2', 'level.name as n3', 'f1.*')
            ->join('forumlog as t1', 't1.id', '=', 'collectlog.start')
            ->leftJoin('forumlog as t2', 't2.id', '=', 'collectlog.end')
            ->join('user', 'user.id', '=', 't1.auth_id')
            ->join('divisi', 'divisi.id', '=', 'user.divisi')
            ->join('level', 'level.id', '=', 'user.status_level')
            ->join('forumsetting as f1', function ($join) {
                $join->on('divisi.id', '=', 'f1.divisi')
                    ->whereRaw('f1.id = (SELECT MAX(f2.id) from forumsetting f2 WHERE f2.divisi = divisi.id and DATE_FORMAT(f2.created_at, "%Y-%m-%d") <= DATE_FORMAT(t1.datetime, "%Y-%m-%d"))');
            })
            ->where('t1.type', '=', 1)

            //If want to remove row that doesnt have logout value
            // ->where('t2.type', 2)
            ->orderByRaw('d1 DESC, auth_id ASC, f1.id DESC')

            ->where('user.name', 'LIKE', "%{$Sname}%")
            ->where('divisi.name', 'LIKE', "%{$Sdivisi}%");

        if (request()->start_date || request()->end_date) {
            $chk = $chk->whereBetween('t1.datetime', [$start_date, $end_date]);
            if (Auth::user()->GetLevel->edit_inoutlog) {
                $chk = $chk->paginate(100);
            } else {
                $chk = $chk->where('user.id', Auth::user()->id)->paginate(100);
            }
            return view('monitoring.indexlog', compact('chk', 'divisi'));
        }

        if (Auth::user()->GetLevel->edit_inoutlog) {
            $chk = $chk->paginate(100);
        } else {
            $chk = $chk->where('user.id', Auth::user()->id)->paginate(100);
        }

        return view('monitoring.indexlog', compact('chk', 'divisi'));
    }

    // public function inoutReport(Request $request) 
    // {
    //     $start_date = Carbon::parse(request()->start_date)
    //     ->startofDay()
    //     ->toDateTimeString();
    //     $end_date = Carbon::parse(request()->end_date)
    //     ->endOfDay()
    //     ->toDateTimeString();

    //     $forumlog = ForumLog::select('*')
    //     ->whereHas('GetUser', function($query){
    //         $Sname = (request('Sname'));
    //         $query->where('name', 'LIKE', "%{$Sname}%");
    //     })
    //     ->groupByRaw('DATE(datetime)')
    //     ->groupBy('auth_id')
    //     ->groupBy('type')
    //     ->orderBy('datetime');

    //     if (request()->start_date || request()->end_date) {
    //         $forumlog->whereBetween('datetime', [$start_date,$end_date]);
    //         $forumlog = $forumlog->get();
    //         return view('monitoring.indexreport', compact('forumlog'));
    //     }

    //     $forumlog = $forumlog->get();

    //     return view('monitoring.indexreport', compact('forumlog'));
    // }

    public function inoutSetting()
    {
        $forumsetting = DB::table('forumsetting')
            ->join('divisi', 'divisi.id', '=', 'forumsetting.divisi')
            ->select('divisi.name as divName', 'forumsetting.*')
            ->whereIn('forumsetting.id', (function ($query) {
                $query->from('forumsetting')
                    ->select(DB::raw("MAX(id)"))
                    ->groupBy('divisi');
            }))
            ->orderBy('divName')
            ->get();

        $sDetails = ForumSetting::all();
        return view('monitoring.indexsetting', compact('forumsetting', 'sDetails'));
    }

    // public function settingCreate()
    // {
    //     return view('monitoring.createsetting');
    // }

    // public function settingStore(Request $request)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'name' => 'required',
    //         'check_in' => 'required',
    //         'check_out' => 'required',
    //         'break' => 'required',
    //         'break_duration' => 'required'
    //     ]);
    //     if ($validator->fails()) {
    //         return redirect()->back()->withErrors($validator);
    //     }
    //     ForumSetting::create([
    //         'name' => $request->name,
    //         'breakT' => $request->breakT,
    //         'breakW' => $request->breakW,
    //         'eatTime' => $request->eatTime,
    //         'workHours' => $request->workHours,
    //     ]);

    //     \LogActivity::addToLog("Create New Division Setting: [" .$request->name. "]." , 1);

    //     return redirect()->route('inout.setting')->with('message', ['text' => 'Setting Created Successfully!', 'class' => 'success']);
    // }

    // public function settingEdit(ForumSetting $forumsetting) {
    //     return view('monitoring.editsetting', compact('forumsetting'));
    // }

    public function settingUpdate(Request $request)
    {
        $forumsetting = DB::table('forumsetting')
            ->join('divisi', 'divisi.id', '=', 'forumsetting.divisi')
            ->select('divisi.name as divName', 'forumsetting.*')
            ->whereIn('forumsetting.id', (function ($query) {
                $query->from('forumsetting')
                    ->select(DB::raw("MAX(id)"))
                    ->groupBy('divisi');
            }))
            ->get();

        $divisiId = [];
        $fSetId = [];
        $storeDivName = [];
        foreach ($forumsetting as $f) {
            array_push($divisiId, $f->divisi);
            array_push($fSetId, $f->id);
        }
        $count = count($divisiId);

        for ($i = 0; $i < $count; $i++) {
            $nm = 'name' . $fSetId[$i];
            $wh = 'workHours' . $fSetId[$i];
            $bt = 'breakT' . $fSetId[$i];
            $bw = 'breakW' . $fSetId[$i];
            $et = 'eatTime' . $fSetId[$i];
            $ot = 'overTime' . $fSetId[$i];
            $obt = 'oBreakT' . $fSetId[$i];
            $obw = 'oBreakW' . $fSetId[$i];
            $oet = 'oEatTime' . $fSetId[$i];

            $workHoursSeconds = $request->$wh;
            $workHoursSeconds *= 60;

            $overTimeSeconds = $request->$ot;
            $overTimeSeconds *= 60;

            //Reference to check if there's update on indexsetting blade
            $fId = ForumSetting::find($fSetId[$i]);
            $fId->breakT = $request->$bt;
            $fId->breakW = $request->$bw;
            $fId->eatTime = $request->$et;
            $fId->workHours = $workHoursSeconds;
            $fId->overTime = $overTimeSeconds;
            $fId->oBreakT = $request->$obt;
            $fId->oBreakW = $request->$obw;
            $fId->oEatTime = $request->$oet;

            if ($fId->isDirty()) {
                $NfId = new ForumSetting;
                $NfId->name = $request->$nm;
                $NfId->breakT = $request->$bt;
                $NfId->breakW = $request->$bw;
                $NfId->eatTime = $request->$et;
                $NfId->workHours = $workHoursSeconds;
                $NfId->overTime = $overTimeSeconds;
                $NfId->oBreakT = $request->$obt;
                $NfId->oBreakW = $request->$obw;
                $NfId->oEatTime = $request->$oet;
                $NfId->divisi = $divisiId[$i];
                $NfId->save();
                array_push($storeDivName, $request->$nm);
            }
        }

        $impDivName = implode(', ', $storeDivName);
        if ($impDivName) {
            \LogActivity::addToLog("Update Setting In-Out [" . $impDivName . "].", 2);
        }

        return redirect()->back()->with(['msg' => 'Setting In-Out Updated Successfully!', 'class' => 'success']);
    }
}
