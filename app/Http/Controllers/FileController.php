<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use Response;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    function index()
    {
        $files = Storage::disk('public')->allFiles();
        return view('file.index', compact('files'));
    }

    function store(Request $request)
    {
        $path = $request->path;
        $file = $request->file('file');
        $filename = $file->getClientOriginalName();
        $file->storeAs('public/' . $path . '/', $filename);

        return redirect()->route('file.index')->with(['msg' => 'File uploaded successfully!', 'class' => 'success']);
    }

    public function delete($path, $filename)
    {
        if (Storage::exists('public/' . $path . '/' . $filename)) {
            Storage::delete('public/' . $path . '/' . $filename);
            return redirect()->route('file.index')->with(['msg' => 'File deleted successfully!', 'class' => 'success']);
        } else {
            return redirect()->route('file.index')->with(['msg' => 'File not found!', 'class' => 'error']);
        }
    }

    function getFile($path, $filename)
    {
        $filepath = public_path('storage/' . $path . '/' . $filename);
        return Response::download($filepath);
    }
}
