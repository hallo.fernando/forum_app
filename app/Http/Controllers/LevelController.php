<?php

namespace App\Http\Controllers;

use App\Models\Divisi;
use App\Models\Level;
use App\Models\User;
use App\Models\UserDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class LevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $divisi = Divisi::select('id', 'name')->latest()->get();
        // $level = Level::select('id', 'name','divisi')->latest()->get();

        $Sname = (request('Sname'));
        $level = Level::where('name', 'LIKE', "%{$Sname}%")
            ->orderBy('name')
            // ->whereHas('GetDivisi', function($query){
            //     $Sdivisi = (request('Sdivisi'));
            //     $query->where('name', 'LIKE', "%{$Sdivisi}%");
            // })
            ->get();

        return view('level.index', compact('level'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('level.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:4'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        \LogActivity::addToLog("Create New Level Control [" . $request->name . "].", 1);

        Level::create([
            'name' => $request->name,
            'permission_view_home' => !$request->has('view_home') ? 0 : 1,
            'permission_edit_home' => !$request->has('edit_home') ? 0 : 1,
            'permission_view_user' => !$request->has('view_user') ? 0 : 1,
            'permission_edit_user' => !$request->has('edit_user') ? 0 : 1,
            'permission_view_user_tingkatan' => !$request->has('view_user_tingkatan') ? 0 : 1,
            'permission_edit_user_tingkatan' => !$request->has('edit_user_tingkatan') ? 0 : 1,
            'permission_view_divisi' => !$request->has('view_divisi') ? 0 : 1,
            'permission_edit_divisi' => !$request->has('edit_divisi') ? 0 : 1,
            'permission_view_log' => !$request->has('view_log') ? 0 : 1,
            'permission_edit_log' => !$request->has('edit_log') ? 0 : 1,
            'permission_view_performance' => !$request->has('view_performance') ? 0 : 1,
            'permission_edit_performance' => !$request->has('edit_performance') ? 0 : 1,
            'view_usercuti' => !$request->has('view_usercuti') ? 0 : 1,
            'edit_usercuti' => !$request->has('edit_usercuti') ? 0 : 1,
            'view_forumsetting' => !$request->has('view_forumsetting') ? 0 : 1,
            'edit_forumsetting' => !$request->has('edit_forumsetting') ? 0 : 1,
            'view_whitelist' => !$request->has('view_whitelist') ? 0 : 1,
            'edit_whitelist' => !$request->has('edit_whitelist') ? 0 : 1,
            'view_inoutlog' => !$request->has('view_inout') ? 0 : 1,
            'edit_inoutlog' => !$request->has('edit_inout') ? 0 : 1,
            'view_overtime' => !$request->has('view_overtime') ? 0 : 1,
            'edit_overtime' => !$request->has('edit_overtime') ? 0 : 1,
        ]);
        return redirect()->route('level.create')->with(['msg' => 'Level Added Successfully!', 'class' => 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Level  $level
     * @return \Illuminate\Http\Response
     */
    public function show(Level $level)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Level  $level
     * @return \Illuminate\Http\Response
     */
    public function edit(Level $level)
    {
        return view('level.edit', compact('level'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Level  $level
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Level $level)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:4'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $lName = $level->name;

        $level->name = $request->name;
        $level->permission_view_home = !$request->has('view_home') ? 0 : 1;
        $level->permission_edit_home = !$request->has('edit_home') ? 0 : 1;
        $level->permission_view_user = !$request->has('view_user') ? 0 : 1;
        $level->permission_edit_user = !$request->has('edit_user') ? 0 : 1;
        $level->permission_view_user_tingkatan = !$request->has('view_user_tingkatan') ? 0 : 1;
        $level->permission_edit_user_tingkatan = !$request->has('edit_user_tingkatan') ? 0 : 1;
        $level->permission_view_divisi = !$request->has('view_divisi') ? 0 : 1;
        $level->permission_edit_divisi = !$request->has('edit_divisi') ? 0 : 1;
        $level->permission_view_log = !$request->has('view_log') ? 0 : 1;
        $level->permission_edit_log = !$request->has('edit_log') ? 0 : 1;
        $level->permission_view_performance = !$request->has('view_performance') ? 0 : 1;
        $level->permission_edit_performance = !$request->has('edit_performance') ? 0 : 1;
        $level->view_usercuti = !$request->has('view_usercuti') ? 0 : 1;
        $level->edit_usercuti = !$request->has('edit_usercuti') ? 0 : 1;
        $level->view_forumsetting = !$request->has('view_forumsetting') ? 0 : 1;
        $level->edit_forumsetting = !$request->has('edit_forumsetting') ? 0 : 1;
        $level->view_whitelist = !$request->has('view_whitelist') ? 0 : 1;
        $level->edit_whitelist = !$request->has('edit_whitelist') ? 0 : 1;
        $level->view_inoutlog = !$request->has('view_inout') ? 0 : 1;
        $level->edit_inoutlog = !$request->has('edit_inout') ? 0 : 1;
        $level->view_overtime = !$request->has('view_overtime') ? 0 : 1;
        $level->edit_overtime = !$request->has('edit_overtime') ? 0 : 1;

        if ($level->isDirty()) {
            $level->save();

            //UserDetails
            if ($request->name != $lName) {
                $user = User::where('status_level', $level->id)->get();
                $cUser = count($user);

                for ($i = 0; $i < $cUser; $i++) {
                    $userdetails = UserDetails::select('*')->where('user_id', $user[$i]->id)->where('type', 2)->get();
                    $cUD = count($userdetails);

                    if (empty($cUD)) {
                        UserDetails::create([
                            'name' => $request->name,
                            'user_id' => $user[$i]->id,
                            'datetime' => $user[$i]->tanggal_gabung,
                            'type' => 2
                        ]);
                    } else {
                        $userDId = UserDetails::where('user_id', $user[$i]->id)->where('name', $lName)->where('type', 2)->latest()->first();
                        if ($userDId == null) {
                            \LogActivity::addToLog("Update Level Control [" . $lName . "] to [" . $request->name . "].", 2);
                            return redirect()->route('level.edit', $level->id)->with(['msg' => 'Level Updated Successfully!', 'class' => 'success']);
                        }
                        $userDId->name = $request->name;
                        $userDId->save();
                    }
                }
                \LogActivity::addToLog("Update Level Control [" . $lName . "] to [" . $request->name . "].", 2);
            }
            //End of UserDetails
            else {
                \LogActivity::addToLog("Update Level Control [" . $level->name . "].", 2);
            }
        }

        return redirect()->route('level.edit', $level->id)->with(['msg' => 'Level Updated Successfully!', 'class' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Level  $level
     * @return \Illuminate\Http\Response
     */
    public function destroy(Level $level)
    {
        try {
            $level->delete();
            \LogActivity::addToLog("Delete Level Control [" . $level->name . "].", 2);
            return redirect()->route('level.index')->with(['msg' => 'Level Deleted Successfully!', 'class' => 'success']);
        } catch (\Throwable $th) {
            $user = User::where('status_level', $level->id)->get();
            $uName = [];

            foreach ($user as $user) {
                $name = $user->name;
                array_push($uName, $name);
            }
            $users = implode(", ", $uName);

            return redirect()->route('level.index')->with(['msg' => 'There is user with this level-> ' . $users . '. Assign them before delete!', 'class' => 'error']);
        }
    }
}
