<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

use Illuminate\Support\Facades\DB;

use App\Models\User;
use App\Models\UserCuti;
use App\Models\Performance;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::user()->already_change_password) {
            $user = User::where('id', Auth::user()->id)->get();
            $usercuti = UserCuti::where('user_id', Auth::user()->id)->get();
            $appreciation = Performance::where('user_id', Auth::user()->id)
                ->whereNotNull('user_id')
                ->whereNotNull('auth_id')
                ->where('type', 1)
                ->get();
            $wrongdoings = Performance::where('user_id', Auth::user()->id)
                ->whereNotNull('user_id')
                ->whereNotNull('auth_id')
                ->where('type', 2)
                ->get();
            $overtime = DB::table('collectlog')
                ->join('forumlog as t1', 't1.id', '=', 'collectlog.start')
                ->leftJoin('forumlog as t2', 't2.id', '=', 'collectlog.end')
                ->join('user', 'user.id', '=', 't1.auth_id')
                ->select('collectlog.*', 't1.datetime as d1', 't2.datetime as d2', 'user.id')
                ->where('collectlog.isOTRequested', 1)
                ->where('user.id', Auth::user()->id)
                ->orderByDesc('collectlog.id')
                ->get();

            return view('home', compact('user', 'usercuti', 'overtime', 'appreciation', 'wrongdoings'));
        } else {
            return redirect()->route('changePass');
        }
    }
}
