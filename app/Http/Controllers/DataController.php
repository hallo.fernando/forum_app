<?php

namespace App\Http\Controllers;

use App\Models\ForumLog;
use App\Models\CollectLog;
use App\Models\ForumSetting;
use App\Models\WhitelistedIp;
use Illuminate\Http\Request;
use App\Models\User;
use DateTimeInterface;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

use Illuminate\Support\Facades\DB;

class DataController extends Controller
{
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function checkStatus(Request $request)
    {
        try {
            $getUser = User::where('username', request('user_id'))->first();
            if ($getUser) {
                $startId = ForumLog::select('description')
                    ->where('auth_id', $getUser->id)
                    ->where('type', 1)
                    ->latest()
                    ->first();

                if (empty($startId->description)) {
                    return response()->json(['message' => '(Testing from Postman)']);
                }

                //OT Request
                $now = Carbon::now();
                $date1 = Carbon::createFromFormat('Y-m-d H:i:s', $now)->subHours(12)->subMinutes(30);

                $fId = ForumLog::select('id', 'datetime', 'auth_id')
                    ->where('auth_id', $getUser->id)
                    ->whereBetween('datetime', [$date1, $now])
                    ->where('type', 1)
                    ->get();

                $cFID = count($fId);

                $forumsetting = DB::table('collectlog')
                    ->join('forumlog as t1', 't1.id', '=', 'collectlog.start')
                    ->join('user', 'user.id', '=', 't1.auth_id')
                    ->join('divisi', 'divisi.id', '=', 'user.divisi')
                    ->join('forumsetting', 'forumsetting.divisi', '=', 'divisi.id')
                    ->select('divisi.name as divName', 'forumsetting.*', 't1.auth_id')
                    ->whereIn('forumsetting.id', (function ($query) {
                        $query->from('forumsetting')
                            ->select(DB::raw("MAX(id)"))
                            ->groupBy('divisi');
                    }))
                    ->where('t1.auth_id', $getUser->id)
                    ->first();

                //PAID OT BUTTON C#
                $OT = 0;

                for ($i = 0; $i < $cFID; $i++) {
                    if ($forumsetting->overTime) {
                        // $requirement = $forumsetting->workHours + $forumsetting->overTime;
                        $requirement = $forumsetting->overTime;

                        $parseDate = Carbon::createFromFormat('Y-m-d H:i:s', $fId[$i]->datetime);
                        $totalWork = $parseDate->diffInMinutes($now);

                        if ($totalWork >= $requirement) {
                            // if ($totalWork >= 1){
                            $collectId = CollectLog::select('id', 'isOTRequested', 'isStaffAcc')
                                ->where('start', $fId[$i]->id)
                                ->where('end', null)
                                ->first();

                            if ($collectId) {
                                if ($collectId->isOTRequested == 0) {
                                    CollectLog::where('id', $collectId->id)
                                        ->update(['isOTRequested' => 1, 'isOTApproved' => 1, 'isStaffAcc' => 0]);
                                }
                            }
                        }
                    }
                    $collectId = CollectLog::select('id', 'isOTRequested', 'isStaffAcc')
                        ->where('start', $fId[$i]->id)
                        ->where('end', null)
                        ->first();

                    if ($collectId) {
                        if ($collectId->isOTRequested && $collectId->isStaffAcc === 0) {
                            $OT = 1;
                        } else if ($collectId->isStaffAcc == 1) {
                            $OT = 2;
                        }
                    }
                }
                //End of OT Request

                $message = $startId->description;
                return response()->json(compact('message', 'OT'), 200);
            } else {
                return response()->json(['message' => 'User Not Found'], 400);
            }
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 400);
        }
    }

    public function login(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                'password' => 'required',
                'name_pc' => 'required',
                'timestamp' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()->toJson()], 400);
            }

            if (Auth::attempt(['username' => request('user_id'), 'password' => request('password')])) {
                $user = Auth::user();
                $token = $user->createToken('auth_token')->plainTextToken;

                $now = Carbon::parse(request('timestamp'))->format('Y-m-d H:i:s');
                $date1 = Carbon::createFromFormat('Y-m-d H:i:s', $now)->subHours(8);

                //START WHITELIST IP
                $whitelist = WhitelistedIp::all();
                $IP = [];

                foreach ($whitelist as $wl) {
                    $WLIP = $wl->ips;
                    array_push($IP, $WLIP);
                }

                function cidr_match($ip, $range)
                {
                    list($subnet, $bits) = explode('/', $range);
                    if ($bits === null) {
                        $bits = 32;
                    }
                    $ip = ip2long($ip);
                    $subnet = ip2long($subnet);
                    $mask = -1 << (32 - $bits);
                    $subnet &= $mask;
                    return ($ip & $mask) == $subnet;
                }

                $cIP = count($IP);

                $listrespIP = [];

                for ($i = 0; $i < $cIP; $i++) {
                    $noRange = strpos($IP[$i], '/');
                    if ($noRange == false) {
                        $findIP = $IP[$i];
                        $IP[$i] = $IP[$i] . "/32";

                        $WhiteIP = WhitelistedIP::where('ips', $findIP)->first();
                        $WhiteIP->ips = $IP[$i];
                        $WhiteIP->save();
                    }
                    $respIP = cidr_match($request->ip(), $IP[$i]);
                    array_push($listrespIP, $respIP);
                }
                //END OF WHITELIST IP

                //IP NOT IN THE WHITELIST
                if (!in_array(true, $listrespIP)) {
                    return response()
                        ->json(['name' => $user->name, 'username' => $user->username, 'level' => Str::upper($user->GetLevel->name), 'status' => '404', 'message' => 'Your IP is not in the whitelist', 'access_token' => $token, 'token_type' => 'Bearer']);
                }

                //STATUS WORK IS CUTI, NONAKTIF OR RESIGN
                if ($user->status_work == 0 || $user->status_work == 2 || $user->status_work == 3) {
                    return response()
                        ->json(['name' => $user->name, 'username' => $user->username, 'level' => Str::upper($user->GetLevel->name), 'status' => $user->status_work, 'message' => 'User Locked', 'access_token' => $token, 'token_type' => 'Bearer']);
                }

                //USER IS SUPERUSER
                $levelName = Str::upper($user->GetLevel->name);
                if ($levelName == 'SUPERUSER') {
                    return response()
                        ->json(['name' => $user->name, 'username' => $user->username, 'level' => Str::upper($user->GetLevel->name), 'status' => $user->status_work, 'message' => 'Hi Superuser, welcome back!', 'access_token' => $token, 'token_type' => 'Bearer']);
                }


                //Start of Interval Stopwatch
                $FirstLogin = DB::table('collectlog')
                    ->join('forumlog as t1', 't1.id', '=', 'collectlog.start')
                    ->join('user', 'user.id', '=', 't1.auth_id')
                    ->select('t1.datetime', 't1.auth_id', 't1.id')
                    ->where('collectlog.end', null)
                    ->where('t1.auth_id', $user->id)
                    ->whereBetween('t1.datetime', [$date1, $now])
                    ->where('t1.type', 1)
                    ->first();

                $lTime = Carbon::createFromFormat('Y-m-d H:i:s', $now);

                if (empty($FirstLogin->datetime)) {
                    $fTime = $lTime;

                    $forumlog = new ForumLog;
                    $forumlog->auth_id = $user->id;
                    $forumlog->type = 1;
                    $forumlog->datetime = $now;
                    $forumlog->description = request('name_pc');
                    $forumlog->save();

                    $ForumLogId = ForumLog::select('id')
                        ->where('auth_id', $user->id)
                        ->where('datetime', 'LIKE', "%{$now}%")
                        ->where('type', 1)
                        ->first();
                    $ForumLogId = $ForumLogId->id;

                    CollectLog::create([
                        'start' => $ForumLogId,
                        'isOTRequested' => 0,
                        'isOTApproved' => 0,
                        'isEdited' => 0
                    ]);
                } else {
                    $fTime = $FirstLogin->datetime;

                    ForumLog::where('id', $FirstLogin->id)
                        ->update(['description' => request('name_pc')]);
                }

                $interval = $lTime->diffInSeconds($fTime);
                //End of Interval Stopwatch

                return response()
                    ->json(['name' => $user->name, 'username' => $user->username, 'level' => Str::upper($user->GetLevel->name), 'status' => $user->status_work, 'message' => 'Hi User ' . $user->name . ', welcome to home', 'stopwatch' => $interval, 'access_token' => $token, 'token_type' => 'Bearer']);
            } else {
                return response()->json(['message' => 'Unauthorized'], 401);
            }
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 400);
        }
    }

    // public function changePass(Request $request)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'user_id' => 'required',
    //         'password' => 'required',
    //         'conf_password' => 'required',
    //         'timestamp' => 'required',
    //     ]);

    //     if ($validator->fails()) {
    //         return response()->json($validator->errors()->toJson(), 400);
    //     }

    //     $getUser = User::where('username', request('user_id'))->first();
    //     if (!empty($getUser)) {
    //         if (request('password') == request('conf_password')) {
    //             User::where('username', request('user_id'))->update(['password' => Hash::make(request('password'))]);
    //             User::where('username', request('user_id'))->update(['already_change_password' => 1]);

    //             ForumLog::create([
    //                 'auth_id' => $getUser->id,
    //                 'description' => request('user_id') . ' Change Password',
    //                 'type' => 7,
    //                 'datetime' => request('timestamp')
    //             ]);

    //             return response()->json(['Message' => 'Password Has Been Changed'], 200);
    //         } else {
    //             return response()->json(['message' => 'Password Does Not Matched'], 400);
    //         }
    //     } else {
    //         return response()->json(['message' => 'User Not Found'], 400);
    //     }
    // }

    public function break(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                'timestamp' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()->toJson()], 400);
            }

            $getUser = User::where('username', request('user_id'))->first();
            if ($getUser) {
                $now = Carbon::parse(request('timestamp'))->format('Y-m-d H:i:s');

                ForumLog::create([
                    'auth_id' => $getUser->id,
                    'description' => request('user_id') . ' Break (T)',
                    'type' => 3,
                    'datetime' => $now
                ]);

                $ForumLogId = ForumLog::select('id')
                    ->where('auth_id', $getUser->id)
                    ->where('datetime', 'LIKE', "%{$now}%")
                    ->where('type', 3)
                    ->first();
                $ForumLogId = $ForumLogId->id;

                CollectLog::create([
                    'start' => $ForumLogId,
                    'isOTRequested' => 0,
                    'isOTApproved' => 0,
                    'isEdited' => 0
                ]);

                $message = 'OK';
                return response()->json(compact('message'), 200);
            } else {
                return response()->json(['message' => 'User Not Found'], 400);
            }
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 400);
        }
    }

    public function unbreak(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                'timestamp' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()->toJson()], 400);
            }

            $getUser = User::where('username', request('user_id'))->first();
            if ($getUser) {
                $now = Carbon::parse(request('timestamp'))->format('Y-m-d H:i:s');
                $date1 = Carbon::createFromFormat('Y-m-d H:i:s', $now)->subHours(2);

                $startId = ForumLog::select('id')
                    ->where('auth_id', $getUser->id)
                    ->whereBetween('datetime', [$date1, $now])
                    ->where('type', 3)
                    ->get();

                $cId = count($startId);

                for ($i = 0; $i <= $cId; $i++) {
                    if (empty($startId[$i]->id)) {
                        return response()->json([
                            'message' => 'Undefined Array Key'
                        ]);
                    } else {
                        $collectId = CollectLog::select('id')
                            ->where('start', $startId[$i]->id)
                            ->where('end', NULL)
                            ->first();

                        if ($collectId != NULL) {
                            break;
                        }
                    }
                }

                ForumLog::create([
                    'auth_id' => $getUser->id,
                    'description' => request('user_id') . ' Selesai Break (T)',
                    'type' => 4,
                    'datetime' => $now
                ]);

                $endId = ForumLog::select('id')
                    ->where('auth_id', $getUser->id)
                    ->where('datetime', 'LIKE', "%{$now}%")
                    ->where('type', 4)
                    ->first();
                $endId = $endId->id;

                CollectLog::where('id', $collectId->id)
                    ->update(['end' => $endId]);

                $message = "OK";
                return response()->json(compact('message'), 200);
            } else {
                return response()->json(['message' => 'User Not Found'], 400);
            }
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 400);
        }
    }

    public function breakW(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                'timestamp' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()->toJson()], 400);
            }

            $getUser = User::where('username', request('user_id'))->first();
            if ($getUser) {
                $now = Carbon::parse(request('timestamp'))->format('Y-m-d H:i:s');

                ForumLog::create([
                    'auth_id' => $getUser->id,
                    'description' => request('user_id') . ' Break (W)',
                    'type' => 10,
                    'datetime' => $now
                ]);

                $ForumLogId = ForumLog::select('id')
                    ->where('auth_id', $getUser->id)
                    ->where('datetime', 'LIKE', "%{$now}%")
                    ->where('type', 10)
                    ->first();
                $ForumLogId = $ForumLogId->id;

                CollectLog::create([
                    'start' => $ForumLogId,
                    'isOTRequested' => 0,
                    'isOTApproved' => 0,
                    'isEdited' => 0
                ]);

                $message = 'OK';
                return response()->json(compact('message'), 200);
            } else {
                return response()->json(['message' => 'User Not Found'], 400);
            }
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 400);
        }
    }

    public function unbreakW(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                'timestamp' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()->toJson()], 400);
            }

            $getUser = User::where('username', request('user_id'))->first();
            if ($getUser) {
                $now = Carbon::parse(request('timestamp'))->format('Y-m-d H:i:s');
                $date1 = Carbon::createFromFormat('Y-m-d H:i:s', $now)->subHours(2);

                $startId = ForumLog::select('id')
                    ->where('auth_id', $getUser->id)
                    ->whereBetween('datetime', [$date1, $now])
                    ->where('type', 10)
                    ->get();

                $cId = count($startId);

                for ($i = 0; $i <= $cId; $i++) {
                    if (empty($startId[$i]->id)) {
                        return response()->json([
                            'message' => 'Undefined Array Key'
                        ]);
                    } else {
                        $collectId = CollectLog::select('id')
                            ->where('start', $startId[$i]->id)
                            ->where('end', NULL)
                            ->first();

                        if ($collectId != NULL) {
                            break;
                        }
                    }
                }

                ForumLog::create([
                    'auth_id' => $getUser->id,
                    'description' => request('user_id') . ' Selesai Break (W)',
                    'type' => 11,
                    'datetime' => $now
                ]);

                $endId = ForumLog::select('id')
                    ->where('auth_id', $getUser->id)
                    ->where('datetime', 'LIKE', "%{$now}%")
                    ->where('type', 11)
                    ->first();
                $endId = $endId->id;

                CollectLog::where('id', $collectId->id)
                    ->update(['end' => $endId]);

                $message = "OK";
                return response()->json(compact('message'), 200);
            } else {
                return response()->json(['message' => 'User Not Found'], 400);
            }
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 400);
        }
    }

    public function eatTime(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                'timestamp' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()->toJson()], 400);
            }

            $getUser = User::where('username', request('user_id'))->first();
            if ($getUser) {
                $now = Carbon::parse(request('timestamp'))->format('Y-m-d H:i:s');

                ForumLog::create([
                    'auth_id' => $getUser->id,
                    'description' => request('user_id') . ' Eating',
                    'type' => 8,
                    'datetime' => $now
                ]);

                $ForumLogId = ForumLog::select('id')
                    ->where('auth_id', $getUser->id)
                    ->where('datetime', 'LIKE', "%{$now}%")
                    ->where('type', 8)
                    ->first();
                $ForumLogId = $ForumLogId->id;

                CollectLog::create([
                    'start' => $ForumLogId,
                    'isOTRequested' => 0,
                    'isOTApproved' => 0,
                    'isEdited' => 0
                ]);

                $message = 'OK';
                return response()->json(compact('message'), 200);
            } else {
                return response()->json(['message' => 'User Not Found'], 400);
            }
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 400);
        }
    }

    public function doneEatTime(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                'timestamp' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()->toJson()], 400);
            }

            $getUser = User::where('username', request('user_id'))->first();
            if ($getUser) {
                $now = Carbon::parse(request('timestamp'))->format('Y-m-d H:i:s');
                $date1 = Carbon::createFromFormat('Y-m-d H:i:s', $now)->subHours(3);

                $startId = ForumLog::select('id')
                    ->where('auth_id', $getUser->id)
                    ->whereBetween('datetime', [$date1, $now])
                    ->where('type', 8)
                    ->get();

                $cId = count($startId);

                for ($i = 0; $i <= $cId; $i++) {
                    if (empty($startId[$i]->id)) {
                        return response()->json([
                            'message' => 'Undefined Array Key'
                        ]);
                    } else {
                        $collectId = CollectLog::select('id')
                            ->where('start', $startId[$i]->id)
                            ->where('end', NULL)
                            ->first();

                        if ($collectId != NULL) {
                            break;
                        }
                    }
                }

                ForumLog::create([
                    'auth_id' => $getUser->id,
                    'description' => request('user_id') . ' Done Eating',
                    'type' => 9,
                    'datetime' => $now
                ]);

                $endId = ForumLog::select('id')
                    ->where('auth_id', $getUser->id)
                    ->where('datetime', 'LIKE', "%{$now}%")
                    ->where('type', 9)
                    ->first();
                $endId = $endId->id;

                CollectLog::where('id', $collectId->id)
                    ->update(['end' => $endId]);

                $message = "OK";
                return response()->json(compact('message'), 200);
            } else {
                return response()->json(['message' => 'User Not Found'], 400);
            }
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 400);
        }
    }

    // public function lainnya(Request $request)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'user_id' => 'required',
    //         'reason' => 'required',
    //         'timestamp' => 'required',
    //     ]);
    //     if ($validator->fails()) {
    //         return response()->json($validator->errors()->toJson(), 400);
    //     }

    //     $getUser = User::where('username', request('user_id'))->first();
    //     if ($getUser) {
    //         ForumLog::create([
    //             'auth_id' => $getUser->id,
    //             'description' => request('user_id') . " - " . request('reason'),
    //             'type' => 5,
    //             'datetime' => request('timestamp')
    //         ]);

    //         $message = "OK";
    //         return response()->json(compact('message'), 200);
    //     } else {
    //         return response()->json(['message' => 'User Not Found'], 400);
    //     }
    // }


    // public function akhiriLainnya(Request $request)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'user_id' => 'required',
    //         'timestamp' => 'required',
    //     ]);
    //     if ($validator->fails()) {
    //         return response()->json($validator->errors()->toJson(), 400);
    //     }
    //     $getUser = User::where('username', request('user_id'))->first();

    //     if ($getUser) {

    //         ForumLog::create([
    //             'auth_id' => $getUser->id,
    //             'description' => request('user_id') . ' - Done Others',
    //             'type' => 6,
    //             'datetime' => request('timestamp')
    //         ]);

    //         $message = "OK";
    //         return response()->json(compact('message'), 200);
    //     } else {
    //         return response()->json(['message' => 'User Not Found'], 400);
    //     }
    // }

    public function overtime(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                'password' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()->toJson()], 400);
            }

            $getUser = User::where('username', request('user_id'))->first();
            if ($getUser) {
                if (Hash::check(request('password'), $getUser->password)) {
                    $now = Carbon::now();
                    $date1 = Carbon::createFromFormat('Y-m-d H:i:s', $now)->subHours(15);

                    $startId = ForumLog::select('id')
                        ->where('auth_id', $getUser->id)
                        ->whereBetween('datetime', [$date1, $now])
                        ->where('type', 1)
                        ->get();

                    $cId = count($startId);

                    for ($i = 0; $i < $cId; $i++) {
                        $collectId = CollectLog::select('id', 'isStaffAcc', 'isOTRequested')
                            ->where('start', $startId[$i]->id)
                            ->where('end', null)
                            ->first();

                        if ($collectId) {
                            if ($collectId->isOTRequested == 1) {
                                $collectlog = CollectLog::where('id', $collectId->id)->first();
                                $collectlog->isStaffAcc = 1;
                                $collectlog->save();
                            }
                        }
                    }

                    return response()->json(['message' => 'OT PAID']);
                }

                return response()->json(['message' => 'Incorrect Password'], 400);
            } else {
                return response()->json(['message' => 'User Not Found'], 400);
            }
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 400);
        }
    }

    public function signout(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                'password' => 'required',
                'timestamp' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()->toJson()], 400);
            }

            $getUser = User::where('username', request('user_id'))->first();
            if ($getUser) {
                if (Hash::check(request('password'), $getUser->password)) {
                    $now = Carbon::parse(request('timestamp'))->format('Y-m-d H:i:s');
                    $date1 = Carbon::createFromFormat('Y-m-d H:i:s', $now)->subHours(22);

                    $startId = ForumLog::select('id')
                        ->where('auth_id', $getUser->id)
                        ->whereBetween('datetime', [$date1, $now])
                        ->where('type', 1)
                        ->get();

                    $cId = count($startId);

                    for ($i = 0; $i <= $cId; $i++) {
                        if (empty($startId[$i]->id)) {
                            return response()->json([
                                'message' => 'Exceed the limit time to logout (24 hours)'
                            ]);
                        } else {
                            $collectId = CollectLog::select('id')
                                ->where('start', $startId[$i]->id)
                                ->where('end', NULL)
                                ->first();

                            if ($collectId != NULL) {
                                break;
                            }
                        }
                    }

                    ForumLog::create([
                        'auth_id' => $getUser->id,
                        'description' => request('user_id') . ' Logout',
                        'type' => 2,
                        'datetime' => $now
                    ]);

                    $endId = ForumLog::select('id')
                        ->where('auth_id', $getUser->id)
                        ->where('datetime', 'LIKE', "%{$now}%")
                        ->where('type', 2)
                        ->first();
                    $endId = $endId->id;

                    // if ($collectId->isStaffAcc === null){
                    //     CollectLog::where('id', $collectId->id)
                    //     ->update(['isStaffAcc' => 0]);
                    // }

                    CollectLog::where('id', $collectId->id)
                        ->update(['end' => $endId]);

                    return response()->json([
                        'message' => 'You have been logged out'
                    ]);
                }

                return response()->json(['message' => 'Incorrect Password'], 400);
            } else {
                return response()->json(['message' => 'User Not Found'], 400);
            }
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage()], 400);
        }
    }
}
