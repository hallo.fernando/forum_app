<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Divisi;
use App\Models\User;
use App\Models\UserCuti;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Auth;

class CutiController extends Controller
{
    public function index(Request $request)
    {
        // Function to reset sisa_cuti to cuti every year (App/Console/Kernel), CRONJOB
        // DB::table('user')->update(array('sisa_cuti' => DB::raw('cuti')));

        $divisi = Divisi::all();

        $start_date = Carbon::parse(request()->start_date)
            ->startofDay()
            ->toDateTimeString();
        $end_date = Carbon::parse(request()->end_date)
            ->endOfDay()
            ->toDateTimeString();

        $usercuti = UserCuti::whereHas('GetUser', function ($query) {
            $Sname = (request('Sname'));
            $query->where('name', 'LIKE', "%{$Sname}%");
            if (request('Sdivisi')) {
                $query->whereHas('GetDivisi', function ($query2) {
                    $Sdivisi = (request('Sdivisi'));
                    $query2->where('name', 'LIKE', "%{$Sdivisi}%");
                });
            }
        });

        if (request()->start_date || request()->end_date) {
            $usercuti->whereBetween('from', [$start_date, $end_date]);
            $usercuti = $usercuti->get();

            return view('usercuti.index', ['usercuti' => $usercuti, 'divisi' => $divisi]);
        }

        $usercuti = $usercuti->get();

        return view('usercuti.index', ['usercuti' => $usercuti, 'divisi' => $divisi]);
    }

    public function create(Request $request)
    {
        return view('usercuti.create');
    }

    public function store(Request $request)
    {
        $daterange = explode(' ', request('daterange'));
        $from = Carbon::parse($daterange[0])
            ->startofDay()
            ->toDateTimeString();
        $till = Carbon::parse($daterange[2])
            ->endOfDay()
            ->toDateTimeString();

        if (Carbon::parse($from)->toDateString() < Carbon::now()->format('Y-m-d')) {
            return back()->with(['msg' => 'The requested date is expired!', 'class' => 'error']);
        }

        $start = Carbon::parse($from);
        $end = Carbon::parse($till);
        $total = $start->diffInDays($end);
        $totalDay = $total + 1;

        Usercuti::create([
            'user_id' => $request->user_id,
            'from' => $from,
            'till' => $till,
            'description' => $request->description,
            'leaveDay' => $totalDay,
            'isApproved' => 0,
            'isEdited' => 0
        ]);

        if ($daterange[0] == $daterange[2]) {
            \LogActivity::addToLog("Create New Request Cuti for [" . Carbon::parse($daterange[0])->isoFormat('DD MMMM Y') . "].", 1);
        } else {
            \LogActivity::addToLog("Create New Request Cuti for [" . Carbon::parse($daterange[0])->isoFormat('DD MMMM Y') . " - " . Carbon::parse($daterange[2])->isoFormat('DD MMMM Y') . "].", 1);
        }

        return redirect()->route('cuti.index')->with(['msg' => 'Cuti have been requested!', 'class' => 'success']);
    }

    public function approve(Request $request, UserCuti $cuti)
    {
        $cuti->isApproved = 1;
        $cuti->approvedBy = Auth::user()->name;
        $cuti->save();

        $user = User::where('id', $cuti->user_id)->first();
        $user->sisa_cuti = $user->sisa_cuti - $cuti->leaveDay;
        $user->save();

        \LogActivity::addToLog("Request Cuti by [" . $cuti->GetUser->name . "] Approved.", 3);

        return redirect()->route('cuti.index')->with(['msg' => 'Cuti Approved!', 'class' => 'success']);
    }

    public function reject(Request $request, UserCuti $cuti)
    {
        $cuti->isApproved = 0;
        $cuti->approvedBy = Auth::user()->name;
        $cuti->reason = $request->reason;
        $cuti->save();

        \LogActivity::addToLog("Request Cuti by [" . $cuti->GetUser->name . "] Rejected.", 4);

        return redirect()->route('cuti.index')->with(['msg' => 'Cuti Rejected!', 'class' => 'error']);
    }

    public function edit(UserCuti $cuti)
    {
        $cuti->from = Carbon::parse($cuti->from)
            ->format('m/d/Y');
        $cuti->till = Carbon::parse($cuti->till)
            ->format('m/d/Y');

        $cutiArr = array($cuti->from, $cuti->till);
        $daterange = implode(' - ', $cutiArr);

        $TotalLeave = UserCuti::select('leaveDay')
            ->where('user_id', $cuti->user_id)
            ->where('isApproved', 1)
            ->get();

        $uTotal = 0;
        foreach ($TotalLeave as $leave) {
            $uTotal += $leave->leaveDay;
        }

        return view('usercuti.edit', compact('cuti', 'daterange', 'uTotal'));
    }

    public function update(Request $request, UserCuti $cuti)
    {
        $daterange = explode(' ', request('daterange'));
        $from = Carbon::parse($daterange[0])
            ->startofDay()
            ->toDateTimeString();
        $till = Carbon::parse($daterange[2])
            ->endOfDay()
            ->toDateTimeString();

        $start = Carbon::parse($from);
        $end = Carbon::parse($till);
        $total = $start->diffInDays($end);
        $totalDay = $total + 1;

        $cuti->from = $from;
        $cuti->till = $till;
        $cuti->leaveDay = $totalDay;
        $cuti->isEdited = 1;
        $cuti->editedBy = Auth::user()->name;
        $cuti->editReason = $request->editReason;
        $cuti->save();

        $TotalLeave = UserCuti::select('leaveDay')
            ->where('user_id', $cuti->user_id)
            ->where('isApproved', 1)
            ->get();

        $uTotal = 0;
        foreach ($TotalLeave as $leave) {
            $uTotal += $leave->leaveDay;
        }

        $sisacuti = $cuti->GetUser->cuti - ($uTotal - $cuti->leaveDay + $totalDay);

        $user = User::where('id', $cuti->user_id)->first();
        $user->sisa_cuti = $sisacuti;
        $user->save();

        \LogActivity::addToLog("Update Request Cuti by [" . $cuti->GetUser->name . "].", 2);

        return redirect()->route('cuti.index')->with(['msg' => 'Cuti Updated Successfully!', 'class' => 'success']);
    }
}
