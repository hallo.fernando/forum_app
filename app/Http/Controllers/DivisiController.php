<?php

namespace App\Http\Controllers;

use App\Models\Divisi;
use App\Models\Level;
use App\Models\ForumSetting;
use App\Models\User;
use App\Models\UserDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Auth;

class DivisiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Sdivisi = request('Sdivisi');

        $divisi = Divisi::select('*')
            ->where('name', 'LIKE', "%{$Sdivisi}%")
            ->get();
        return view('divisi.index', compact('divisi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('divisi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:4|unique:divisi'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        Divisi::create([
            'name' => $request->name,
            'createdBy' => Auth::user()->id
        ]);

        $getDivisiID = Divisi::select('id', 'name')->where('name', $request->name)->first();

        ForumSetting::create([
            'name' => $request->name,
            'breakT' => 30,
            'breakW' => 20,
            'eatTime' => 30,
            'workHours' => 600,
            'overTime' => 660,
            'oBreakT' => 40,
            'oBreakW' => 30,
            'oEatTime' => 60,
            'divisi' => $getDivisiID->id,
            'created_at' => Carbon::createFromFormat('Y-m-d H:i:s', '2022-01-01 00:00:00')
        ]);

        $createdposisi = [];

        $count = count($request->posisi);
        for ($i = 0; $i < $count; $i++) {
            if ($request->posisi[$i]) {
                Level::create([
                    'name' => $request->posisi[$i],
                    'permission_view_home' => 1,
                    'permission_edit_home' => 1,
                    'permission_view_user' => 1,
                    'permission_edit_user' => 1,
                    'permission_view_user_tingkatan' => 1,
                    'permission_edit_user_tingkatan' => 1,
                    'permission_view_divisi' => 1,
                    'permission_edit_divisi' => 1,
                    'permission_view_log' => 1,
                    'permission_edit_log' => 1,
                    'permission_view_performance' => 1,
                    'permission_edit_performance' => 1,
                    'view_usercuti' => 1,
                    'edit_usercuti' => 1,
                    'view_forumsetting' => 1,
                    'edit_forumsetting' => 1,
                    'view_whitelist' => 1,
                    'edit_whitelist' => 1,
                    'view_inoutlog' => 1,
                    'edit_inoutlog' => 1,
                    'view_overtime' => 1,
                    'edit_overtime' => 1,
                    // 'divisi' => $getDivisiID->id,
                ]);
                array_push($createdposisi, $request->posisi[$i]);
            }
        }

        $impPosisi = implode(', ', $createdposisi);
        if (empty($impPosisi)) {
            \LogActivity::addToLog("Create New Divisi [" . $request->name . "].", 1);
        } else {
            \LogActivity::addToLog("Create New Divisi [" . $request->name . "], Create New Posisi [" . $impPosisi . "].", 1);
        }

        return redirect()->route('divisi.create')->with(['msg' => 'Divisi Created Successfully!', 'class' => 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Divisi  $divisi
     * @return \Illuminate\Http\Response
     */
    public function show(Divisi $divisi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Divisi  $divisi
     * @return \Illuminate\Http\Response
     */
    public function edit(Divisi $divisi)
    {
        //Testing edit divisi with level
        // $level = Level::select('*')
        // ->where('divisi', $divisi->id)
        // ->get();

        return view('divisi.edit', compact('divisi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Divisi  $divisi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Divisi $divisi)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:4|unique:divisi,name,' . $divisi->id
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $divName = $divisi->name;

        $divisi->name = $request->name;
        if ($divisi->isDirty()) {
            $divisi->updatedBy = Auth::user()->id;
            $divisi->save();
            \LogActivity::addToLog("Update Data Divisi [" . $divName . "] to [" . $request->name . "].", 2);

            //UserDetails
            $user = User::where('divisi', $divisi->id)->get();
            $cUser = count($user);

            for ($i = 0; $i < $cUser; $i++) {
                $userdetails = UserDetails::select('*')->where('user_id', $user[$i]->id)->where('type', 1)->get();
                $cUD = count($userdetails);

                if (empty($cUD)) {
                    UserDetails::create([
                        'name' => $request->name,
                        'user_id' => $user[$i]->id,
                        'datetime' => $user[$i]->tanggal_gabung,
                        'type' => 1
                    ]);
                } else {
                    $userDId = UserDetails::where('user_id', $user[$i]->id)->where('name', $divName)->where('type', 1)->latest()->first();
                    if ($userDId == null) {
                        return redirect()->route('divisi.edit', $divisi->id)->with(['msg' => 'Divisi Updated Successfully!', 'class' => 'success']);
                    }
                    $userDId->name = $request->name;
                    $userDId->save();
                }
            }
            //End of UserDetails
        }

        return redirect()->route('divisi.edit', $divisi->id)->with(['msg' => 'Divisi Updated Successfully!', 'class' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Divisi  $divisi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Divisi $divisi)
    {
        try {
            $divisi->delete();
            \LogActivity::addToLog("Delete Data Divisi [" . $divisi->name . "].", 2);
            return redirect()->route('divisi.index')->with(['msg' => 'Divisi Deleted Successfully!', 'class' => 'success']);
        } catch (\Throwable $th) {
            $user = User::where('divisi', $divisi->id)->get();
            $uName = [];

            foreach ($user as $user) {
                $name = $user->name;
                array_push($uName, $name);
            }
            $users = implode(", ", $uName);

            return redirect()->route('divisi.index')->with(['msg' => 'There is user in this divisi-> ' . $users . '. Assign them before delete!', 'class' => 'error']);
        }
    }
}
