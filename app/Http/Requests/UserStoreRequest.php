<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:4',
            'username' => [
                'required',
                'min:5',
                'unique:user',
                'regex:/^[a-zA-Z0-9]*([a-zA-Z][0-9]|[0-9][a-zA-Z])[a-zA-Z0-9]*$/'
            ],
            'email'=> 'required|string|email|unique:user',
        ];
    }
    public function messages(){
        return [
            'username.unique' => 'Username already used',
            'username.regex' => 'Username must contains letter & number',
            'email.unique' => 'Email already used',
        ];
    }
}
