<?php

namespace App\Http\Middleware;

use App\Models\WhitelistedIp;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Auth;

class WhitelistIP
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $whitelist = WhitelistedIp::all();
        $IP = [];

        foreach ($whitelist as $wl) {
            $WLIP = $wl->ips;
            array_push($IP, $WLIP);
        }

        $AuthUser = Str::upper(Auth::user()->username);
        if ($AuthUser == "ADMIN")
        {
            return $next($request);
        }

        if (empty($IP))
        {
            return $next($request);
        }

        function cidr_match($ip, $range)
        {
            list ($subnet, $bits) = explode('/', $range);
            if ($bits === null) {
                $bits = 32;
            }
            $ip = ip2long($ip);
            $subnet = ip2long($subnet);
            $mask = -1 << (32 - $bits);
            $subnet &= $mask;
            return ($ip & $mask) == $subnet;
        }

        $cIP = count($IP);

        for ($i = 0; $i < $cIP; $i++) {
            $noRange = strpos($IP[$i], '/');
            if ($noRange == false){
                $findIP = $IP[$i];
                $IP[$i] = $IP[$i]."/32";

                $WhiteIP = WhitelistedIP::where('ips', $findIP)->first();
                $WhiteIP->ips = $IP[$i];
                $WhiteIP->save();
            }
            if (cidr_match($request->ip(),$IP[$i]))
            {
                return $next($request);
            }
        }
        Auth::logout();
        $errors = ['user' => 'Your IP is not in the whitelist'];
        return back()->withErrors($errors);
        

        // if (!in_array($request->ip(), $IP)) {
        //     Auth::logout();
        //     $errors = ['user' => 'Your IP is not in the whitelist'];
        //     return back()->withErrors($errors);
        // }

        // return $next($request);
    }
}
