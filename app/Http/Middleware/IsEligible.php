<?php

namespace App\Http\Middleware;

use App\Models\Level;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IsEligible
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        //Define Level
        $level = Level::find(Auth::user()->status_level);

        // Changepass
        if($request->route()->getName() == 'changePass'){
            if(Auth::user()->already_change_password){
                return redirect()->back()->with(['msg' => 'Change your password from data user menu', 'class' => 'error']);
            }
            return $next($request);
        }

        //Home
        if($request->route()->getName() == 'home'){
            if($level->permission_view_home){
                return $next($request);
            }
            else{
                return redirect()->back()->with(['msg' => "You don't have permission to access this page", 'class' => 'error']);
            }
        }


        // User
        if($request->route()->getName() == 'user.index'){
            if($level->permission_view_user){
                return $next($request);
            }
            else{
                return redirect()->back()->with(['msg' => "You don't have permission to access this page", 'class' => 'error']);
            }
        }
        if($request->route()->getName() == 'user.create'
        || $request->route()->getName() == 'user.edit'
        || $request->route()->getName() == 'user.store'
        || $request->route()->getName() == 'user.update'){
            if($level->permission_edit_user){
                return $next($request);
            }
            else{
                if ($request->route()->getname() == 'user.update'){
                    if (!Auth::user()->already_change_password){
                        return $next($request);
                    }
                }
                return redirect()->back()->with(['msg' => 'You need permission to perform this action', 'class' => 'error']);
            }
        }
        // if($request->route()->getName() == 'user.destroy'){
        //     if($level->permission_edit_user){
        //         return $next($request);
        //     }
        //     else{
        //         return redirect()->back()->with(['msg' => 'You need permission to perform this action', 'class' => 'error']);
        //     }
        // }


        // User Cuti
        if($request->route()->getName() == 'cuti.index'
        || $request->route()->getName() == 'cuti.create'
        || $request->route()->getName() == 'cuti.store'){
            if($level->view_usercuti){
                return $next($request);
            }
            else{
                return redirect()->back()->with(['msg' => "You don't have permission to access this page", 'class' => 'error']);
            }
        }
        if($request->route()->getName() == 'cuti.edit'
        || $request->route()->getName() == 'cuti.update'
        || $request->route()->getName() == 'cuti.approve'
        || $request->route()->getName() == 'cuti.reject'){
            if($level->edit_usercuti){
                return $next($request);
            }
            else{
                return redirect()->back()->with(['msg' => 'You need permission to perform this action', 'class' => 'error']);
            }
        }


        // Divisi
        if($request->route()->getName() == 'divisi.index'){
            if($level->permission_view_divisi){
                return $next($request);
            }
            else{
                return redirect()->back()->with(['msg' => "You don't have permission to access this page", 'class' => 'error']);
            }
        }
        if($request->route()->getName() == 'divisi.create'
        || $request->route()->getName() == 'divisi.edit'
        || $request->route()->getName() == 'divisi.store'
        || $request->route()->getName() == 'divisi.update'
        || $request->route()->getName() == 'divisi.destroy'){
            if($level->permission_edit_divisi){
                return $next($request);
            }
            else{
                return redirect()->back()->with(['msg' => 'You need permission to perform this action', 'class' => 'error']);
            }
        }


        // Level
        if($request->route()->getName() == 'level.index'){
            if($level->permission_view_user_tingkatan){
                return $next($request);
            }
            else{
                return redirect()->back()->with(['msg' => "You don't have permission to access this page", 'class' => 'error']);
            }
        }
        if($request->route()->getName() == 'level.create'
        || $request->route()->getName() == 'level.edit'
        || $request->route()->getName() == 'level.store'
        || $request->route()->getName() == 'level.update'
        || $request->route()->getName() == 'level.destroy'){
            if($level->permission_edit_user_tingkatan){
                return $next($request);
            }
            else{
                return redirect()->back()->with(['msg' => 'You need permission to perform this action', 'class' => 'error']);
            }
        }


        // Activity Log
        if($request->route()->getName() == 'log.index'){
            if($level->permission_view_log){
                return $next($request);
            }
            else{
                return redirect()->back()->with(['msg' => "You don't have permission to access this page", 'class' => 'error']);
            }
        }


        //Monitoring User
        if($request->route()->getName() == 'inout.log'){
            if($level->view_inoutlog){
                return $next($request);
            }
            else{
                return redirect()->back()->with(['msg' => "You don't have permission to access this page", 'class' => 'error']);
            }
        }
        // if($request->route()->getName() == 'inout.report'){
        //     $forumsetting = ForumSetting::all()->first();
        //     // $user = User::select('shift')->get();
        //     if(empty($forumsetting)){
        //         return redirect()->back()->with(['msg' => 'You must create settings at In-Out Settings', 'class' => 'error']);
        //     }
        //     // foreach ($user as $user){
        //     //     if(empty($user->GetShift)){
        //     //         return redirect()->back()->with(['msg' => 'You must assign shift to every user to view this page', 'class' => 'error']);
        //     //     }
        //     // }
        //     if($level->permission_view_log){
        //         return $next($request);
        //     }
        //     else{
        //         return redirect()->back()->with(['msg' => "You don't have permission to access this page", 'class' => 'error']);
        //     }
        // }
        if($request->route()->getName() == 'inout.setting'){
            if($level->view_forumsetting){
                return $next($request);
            }
            else{
                return redirect()->back()->with(['msg' => "You don't have permission to access this page", 'class' => 'error']);
            }
        }

        if($request->route()->getName() == 'inout.update'){
            if($level->edit_forumsetting){
                return $next($request);
            }
            else{
                return redirect()->back()->with(['msg' => "You need permission to perform this action", 'class' => 'error']);
            }
        }


        //Performance
        if($request->route()->getName() == 'perf.index'){
            if($level->permission_view_performance){
                return $next($request);
            }
            else{
                return redirect()->back()->with(['msg' => "You don't have permission to access this pag", 'class' => 'error']);
            }
        }
        if($request->route()->getName() == 'perf.create'
        || $request->route()->getName() == 'perf.store'){
            if($level->permission_edit_performance){
                return $next($request);
            }
            else{
                return redirect()->back()->with(['msg' => "You need permission to perform this action", 'class' => 'error']);
            }
        }


        //Whitelist IP
        if($request->route()->getName() == 'wl.index'){
            if($level->view_whitelist){
                return $next($request);
            }
            else{
                return redirect()->back()->with(['msg' => "You don't have permission to access this page", 'class' => 'error']);
            }
        }
        if($request->route()->getName() == 'wl.store'
        || $request->route()->getName() == 'wl.destroy'){
            if($level->edit_whitelist){
                return $next($request);
            }
            else{
                return redirect()->back()->with(['msg' => 'You need permission to perform this action', 'class' => 'error']);
            }
        }

        //Overtime
        if($request->route()->getName() == 'ot.index'){
            if($level->view_overtime){
                return $next($request);
            }
            else{
                return redirect()->back()->with(['msg' => "You don't have permission to access this page", 'class' => 'error']);
            }
        }
        if($request->route()->getName() == 'ot.approve'
        || $request->route()->getName() == 'ot.reject'
        || $request->route()->getName() == 'ot.update'
        || $request->route()->getName() == 'ot.approveall'){
            if($level->edit_overtime){
                return $next($request);
            }
            else{
                return redirect()->back()->with(['msg' => 'You need permission to perform this action', 'class' => 'error']);
            }
        }

        if($request->route()->getName() == 'ot.temporary'
        || $request->route()->getName() == 'ot.temporaryupdate'){
            return $next($request);
        }

        //Notif Details
        if($request->route()->getName() == 'ot.notice'){
            return $next($request);
        }
    }
}
