@extends('layouts.app')
@section('title')
    Edit Posisi
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            @include('layouts.notif')

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 style="float:left;">Edit Level</h5>
                        <a href="{{ route('level.index') }}" class="btn btn-sm btn-primary" style="float: right">Back</a>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('level.update', $level->id) }}" method="POST" enctype="multipart/form-data"
                            autocomplete="off">
                            @csrf
                            @method('PUT')
                            <div class="row py-2">
                                <div class="col-6">
                                    <label style="font-weight: bold" for="">Level</label>
                                    @include('layouts.error', ['name' => 'name'])
                                    <input type="text" class="form-control" name="name" placeholder="Enter a name"
                                        value="{{ $level->name }}">
                                </div>
                                <div class="col-6 pt-4">
                                    <button class="btn btn-primary col-md-2" type="submit">Update</button>
                                </div>
                            </div>
                            <br>
                            <table class="table text-center table-bordered table-striped" id="SellSummariesTable">
                                <thead>
                                    <tr>
                                        <th>Menu</th>
                                        <th>View</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Profile</td>
                                        <td><input type="checkbox" name="view_home"
                                                @if ($level->permission_view_home) checked @endif onClick="HomeU(this)">
                                        </td>
                                        <td><input type="checkbox" name="edit_home"
                                                @if ($level->permission_edit_home) checked @endif onClick="Home(this)">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Data User</td>
                                        <td><input type="checkbox" name="view_user"
                                                @if ($level->permission_view_user) checked @endif onClick="UserU(this)">
                                        </td>
                                        <td><input type="checkbox" name="edit_user"
                                                @if ($level->permission_edit_user) checked @endif onClick="User(this)">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Data Cuti</td>
                                        <td><input type="checkbox" name="view_usercuti"
                                                @if ($level->view_usercuti) checked @endif onClick="UserCutiU(this)">
                                        </td>
                                        <td><input type="checkbox" name="edit_usercuti"
                                                @if ($level->edit_usercuti) checked @endif onClick="UserCuti(this)">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Level Control</td>
                                        <td><input type="checkbox" name="view_user_tingkatan"
                                                @if ($level->permission_view_user_tingkatan) checked @endif onClick="LevelU(this)">
                                        </td>
                                        <td><input type="checkbox" name="edit_user_tingkatan"
                                                @if ($level->permission_edit_user_tingkatan) checked @endif onClick="Level(this)">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Data Divisi</td>
                                        <td><input type="checkbox" name="view_divisi"
                                                @if ($level->permission_view_divisi) checked @endif onClick="DivisiU(this)">
                                        </td>
                                        <td><input type="checkbox" name="edit_divisi"
                                                @if ($level->permission_edit_divisi) checked @endif onClick="Divisi(this)">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Log</td>
                                        <td><input type="checkbox" name="view_log"
                                                @if ($level->permission_view_log) checked @endif onClick="LogU(this)">
                                        </td>
                                        <td><input type="checkbox" name="edit_log"
                                                @if ($level->permission_edit_log) checked @endif onClick="Log(this)">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Setting In-Out</td>
                                        <td><input type="checkbox" name="view_forumsetting"
                                                @if ($level->view_forumsetting) checked @endif onClick="SettingU(this)">
                                        </td>
                                        <td><input type="checkbox" name="edit_forumsetting"
                                                @if ($level->edit_forumsetting) checked @endif onClick="Setting(this)">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>IP Control</td>
                                        <td><input type="checkbox" name="view_whitelist"
                                                @if ($level->view_whitelist) checked @endif
                                                onClick="WhitelistU(this)"></td>
                                        <td><input type="checkbox" name="edit_whitelist"
                                                @if ($level->edit_whitelist) checked @endif
                                                onClick="Whitelist(this)"></td>
                                    </tr>
                                    <tr>
                                        <td>In-Out Log</td>
                                        <td><input type="checkbox" name="view_inout"
                                                @if ($level->view_inoutlog) checked @endif onClick="InoutU(this)">
                                        </td>
                                        <td><input type="checkbox" name="edit_inout"
                                                @if ($level->edit_inoutlog) checked @endif onClick="Inout(this)">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Data Performance</td>
                                        <td><input type="checkbox" name="view_performance"
                                                @if ($level->permission_view_performance) checked @endif
                                                onClick="PerformanceU(this)"></td>
                                        <td><input type="checkbox" name="edit_performance"
                                                @if ($level->permission_edit_performance) checked @endif
                                                onClick="Performance(this)"></td>
                                    </tr>
                                    <tr>
                                        <td>Data OverTime</td>
                                        <td><input type="checkbox" name="view_overtime"
                                                @if ($level->view_overtime) checked @endif
                                                onClick="OvertimeU(this)"></td>
                                        <td><input type="checkbox" name="edit_overtime"
                                                @if ($level->edit_overtime) checked @endif
                                                onClick="Overtime(this)"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function Home(source) {
            checkboxes = document.getElementsByName('view_home');
            if (source.checked) {
                checkboxes[0].checked = source.checked;
            }
        }

        function HomeU(source) {
            checkboxes = document.getElementsByName('edit_home');
            if (source.checked == false) {
                checkboxes[0].checked = false;
            }
        }

        function User(source) {
            checkboxes = document.getElementsByName('view_user');
            if (source.checked) {
                checkboxes[0].checked = source.checked;
            }
        }

        function UserU(source) {
            checkboxes = document.getElementsByName('edit_user');
            if (source.checked == false) {
                checkboxes[0].checked = false;
            }
        }

        function UserCuti(source) {
            checkboxes = document.getElementsByName('view_usercuti');
            if (source.checked) {
                checkboxes[0].checked = source.checked;
            }
        }

        function UserCutiU(source) {
            checkboxes = document.getElementsByName('edit_usercuti');
            if (source.checked == false) {
                checkboxes[0].checked = false;
            }
        }

        function Level(source) {
            checkboxes = document.getElementsByName('view_user_tingkatan');
            if (source.checked) {
                checkboxes[0].checked = source.checked;
            }
        }

        function LevelU(source) {
            checkboxes = document.getElementsByName('edit_user_tingkatan');
            if (source.checked == false) {
                checkboxes[0].checked = false;
            }
        }

        function Divisi(source) {
            checkboxes = document.getElementsByName('view_divisi');
            if (source.checked) {
                checkboxes[0].checked = source.checked;
            }
        }

        function DivisiU(source) {
            checkboxes = document.getElementsByName('edit_divisi');
            if (source.checked == false) {
                checkboxes[0].checked = false;
            }
        }

        function Log(source) {
            checkboxes = document.getElementsByName('view_log');
            if (source.checked) {
                checkboxes[0].checked = source.checked;
            }
        }

        function LogU(source) {
            checkboxes = document.getElementsByName('edit_log');
            if (source.checked == false) {
                checkboxes[0].checked = false;
            }
        }

        function Setting(source) {
            checkboxes = document.getElementsByName('view_forumsetting');
            if (source.checked) {
                checkboxes[0].checked = source.checked;
            }
        }

        function SettingU(source) {
            checkboxes = document.getElementsByName('edit_forumsetting');
            if (source.checked == false) {
                checkboxes[0].checked = false;
            }
        }

        function Whitelist(source) {
            checkboxes = document.getElementsByName('view_whitelist');
            if (source.checked) {
                checkboxes[0].checked = source.checked;
            }
        }

        function WhitelistU(source) {
            checkboxes = document.getElementsByName('edit_whitelist');
            if (source.checked == false) {
                checkboxes[0].checked = false;
            }
        }

        function Inout(source) {
            checkboxes = document.getElementsByName('view_inout');
            if (source.checked) {
                checkboxes[0].checked = source.checked;
            }
        }

        function InoutU(source) {
            checkboxes = document.getElementsByName('edit_inout');
            if (source.checked == false) {
                checkboxes[0].checked = false;
            }
        }

        function Performance(source) {
            checkboxes = document.getElementsByName('view_performance');
            if (source.checked) {
                checkboxes[0].checked = source.checked;
            }
        }

        function PerformanceU(source) {
            checkboxes = document.getElementsByName('edit_performance');
            if (source.checked == false) {
                checkboxes[0].checked = false;
            }
        }

        function Overtime(source) {
            checkboxes = document.getElementsByName('view_overtime');
            if (source.checked) {
                checkboxes[0].checked = source.checked;
            }
        }

        function OvertimeU(source) {
            checkboxes = document.getElementsByName('edit_overtime');
            if (source.checked == false) {
                checkboxes[0].checked = false;
            }
        }
    </script>
@endsection
