@extends('layouts.app')
@section('title')
    Create Level
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            @include('layouts.notif')

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 style="float:left;">Add Level</h5>
                        <a href="{{ route('level.index') }}" class="btn btn-sm btn-primary" style="float: right">Back</a>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('level.store') }}" method="POST" enctype="multipart/form-data"
                            autocomplete="off">
                            @csrf
                            <div class="row py-2">
                                <div class="col-6">
                                    <label for="">Level</label>
                                    <input type="text" class="form-control" name="name" placeholder="Enter a name"
                                        value="{{ old('name') }}">
                                    @include('layouts.error', ['name' => 'name'])
                                </div>
                                <div class="col-6 pt-4">
                                    <button class="btn btn-primary col-md-2" type="submit">Submit</button>
                                </div>
                            </div>
                            <br>
                            <table class="table text-center table-bordered table-striped" id="SellSummariesTable">
                                <thead>
                                    <tr>
                                        <th>Menu</th>
                                        <th>View</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="font-weight:bold">Check All</td>
                                        <td><input type="checkbox" class="viewchkall" onClick="view(this)" /></td>
                                        <td><input type="checkbox" class="editchkall" onClick="edit(this)" /></td>
                                    </tr>
                                    <tr>
                                        <td>Profile</td>
                                        <td><input class="view" type="checkbox" name="view_home"
                                                onClick="HomeU(this)"></td>
                                        <td><input class="edit" type="checkbox" name="edit_home"
                                                onClick="Home(this)"></td>
                                    </tr>
                                    <tr>
                                        <td>Data User</td>
                                        <td><input class="view" type="checkbox" name="view_user"
                                                onClick="UserU(this)"></td>
                                        <td><input class="edit" type="checkbox" name="edit_user"
                                                onClick="User(this)"></td>
                                    </tr>
                                    <tr>
                                        <td>Data Cuti</td>
                                        <td><input class="view" type="checkbox" name="view_usercuti"
                                                onClick="UserCutiU(this)"></td>
                                        <td><input class="edit" type="checkbox" name="edit_usercuti"
                                                onClick="UserCuti(this)"></td>
                                    </tr>
                                    <tr>
                                        <td>Level Control</td>
                                        <td><input class="view" type="checkbox" name="view_user_tingkatan"
                                                onClick="LevelU(this)"></td>
                                        <td><input class="edit" type="checkbox" name="edit_user_tingkatan"
                                                onClick="Level(this)"></td>
                                    </tr>
                                    <tr>
                                        <td>Data Divisi</td>
                                        <td><input class="view" type="checkbox" name="view_divisi"
                                                onClick="DivisiU(this)"></td>
                                        <td><input class="edit" type="checkbox" name="edit_divisi"
                                                onClick="Divisi(this)"></td>
                                    </tr>
                                    <tr>
                                        <td>Log</td>
                                        <td><input class="view" type="checkbox" name="view_log"
                                                onClick="LogU(this)"></td>
                                        <td><input class="edit" type="checkbox" name="edit_log"
                                                onClick="Log(this)"></td>
                                    </tr>
                                    <tr>
                                        <td>Setting In-Out</td>
                                        <td><input class="view" type="checkbox" name="view_forumsetting"
                                                onClick="SettingU(this)"></td>
                                        <td><input class="edit" type="checkbox" name="edit_forumsetting"
                                                onClick="Setting(this)"></td>
                                    </tr>
                                    <tr>
                                        <td>IP Control</td>
                                        <td><input class="view" type="checkbox" name="view_whitelist"
                                                onClick="WhitelistU(this)"></td>
                                        <td><input class="edit" type="checkbox" name="edit_whitelist"
                                                onClick="Whitelist(this)"></td>
                                    </tr>
                                    <tr>
                                        <td>In-Out Log</td>
                                        <td><input class="view" type="checkbox" name="view_inout"
                                                onClick="InoutU(this)"></td>
                                        <td><input class="edit" type="checkbox" name="edit_inout"
                                                onClick="Inout(this)"></td>
                                    </tr>
                                    <tr>
                                        <td>Data Performance</td>
                                        <td><input class="view" type="checkbox" name="view_performance"
                                                onClick="PerformanceU(this)"></td>
                                        <td><input class="edit" type="checkbox" name="edit_performance"
                                                onClick="Performance(this)"></td>
                                    </tr>
                                    <tr>
                                        <td>Data OverTime</td>
                                        <td><input class="view" type="checkbox" name="view_overtime"
                                                onClick="OvertimeU(this)"></td>
                                        <td><input class="edit" type="checkbox" name="edit_overtime"
                                                onClick="Overtime(this)"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function view(source) {
            checkboxes = document.getElementsByClassName('view');
            checkboxes1 = document.getElementsByClassName('edit');
            checkboxes2 = document.getElementsByClassName('editchkall');
            for (var i = 0, n = checkboxes.length; i < n; i++) {
                if (source.checked == false) {
                    checkboxes1[i].checked = false;
                    checkboxes2[0].checked = false;
                }
                checkboxes[i].checked = source.checked;
            }
        }

        function edit(source) {
            checkboxes = document.getElementsByClassName('edit');
            checkboxes1 = document.getElementsByClassName('view');
            checkboxes2 = document.getElementsByClassName('viewchkall');
            for (var i = 0, n = checkboxes.length; i < n; i++) {
                if (source.checked) {
                    checkboxes1[i].checked = source.checked;
                    checkboxes2[0].checked = source.checked;
                }
                checkboxes[i].checked = source.checked;
            }
        }

        function Home(source) {
            checkboxes = document.getElementsByName('view_home');
            if (source.checked) {
                checkboxes[0].checked = source.checked;
            }
        }

        function HomeU(source) {
            checkboxes = document.getElementsByName('edit_home');
            if (source.checked == false) {
                checkboxes[0].checked = false;
            }
        }

        function User(source) {
            checkboxes = document.getElementsByName('view_user');
            if (source.checked) {
                checkboxes[0].checked = source.checked;
            }
        }

        function UserU(source) {
            checkboxes = document.getElementsByName('edit_user');
            if (source.checked == false) {
                checkboxes[0].checked = false;
            }
        }

        function UserCuti(source) {
            checkboxes = document.getElementsByName('view_usercuti');
            if (source.checked) {
                checkboxes[0].checked = source.checked;
            }
        }

        function UserCutiU(source) {
            checkboxes = document.getElementsByName('edit_usercuti');
            if (source.checked == false) {
                checkboxes[0].checked = false;
            }
        }

        function Level(source) {
            checkboxes = document.getElementsByName('view_user_tingkatan');
            if (source.checked) {
                checkboxes[0].checked = source.checked;
            }
        }

        function LevelU(source) {
            checkboxes = document.getElementsByName('edit_user_tingkatan');
            if (source.checked == false) {
                checkboxes[0].checked = false;
            }
        }

        function Divisi(source) {
            checkboxes = document.getElementsByName('view_divisi');
            if (source.checked) {
                checkboxes[0].checked = source.checked;
            }
        }

        function DivisiU(source) {
            checkboxes = document.getElementsByName('edit_divisi');
            if (source.checked == false) {
                checkboxes[0].checked = false;
            }
        }

        function Log(source) {
            checkboxes = document.getElementsByName('view_log');
            if (source.checked) {
                checkboxes[0].checked = source.checked;
            }
        }

        function LogU(source) {
            checkboxes = document.getElementsByName('edit_log');
            if (source.checked == false) {
                checkboxes[0].checked = false;
            }
        }

        function Setting(source) {
            checkboxes = document.getElementsByName('view_forumsetting');
            if (source.checked) {
                checkboxes[0].checked = source.checked;
            }
        }

        function SettingU(source) {
            checkboxes = document.getElementsByName('edit_forumsetting');
            if (source.checked == false) {
                checkboxes[0].checked = false;
            }
        }

        function Whitelist(source) {
            checkboxes = document.getElementsByName('view_whitelist');
            if (source.checked) {
                checkboxes[0].checked = source.checked;
            }
        }

        function WhitelistU(source) {
            checkboxes = document.getElementsByName('edit_whitelist');
            if (source.checked == false) {
                checkboxes[0].checked = false;
            }
        }

        function Inout(source) {
            checkboxes = document.getElementsByName('view_inout');
            if (source.checked) {
                checkboxes[0].checked = source.checked;
            }
        }

        function InoutU(source) {
            checkboxes = document.getElementsByName('edit_inout');
            if (source.checked == false) {
                checkboxes[0].checked = false;
            }
        }

        function Performance(source) {
            checkboxes = document.getElementsByName('view_performance');
            if (source.checked) {
                checkboxes[0].checked = source.checked;
            }
        }

        function PerformanceU(source) {
            checkboxes = document.getElementsByName('edit_performance');
            if (source.checked == false) {
                checkboxes[0].checked = false;
            }
        }

        function Overtime(source) {
            checkboxes = document.getElementsByName('view_overtime');
            if (source.checked) {
                checkboxes[0].checked = source.checked;
            }
        }

        function OvertimeU(source) {
            checkboxes = document.getElementsByName('edit_overtime');
            if (source.checked == false) {
                checkboxes[0].checked = false;
            }
        }
    </script>
@endsection
