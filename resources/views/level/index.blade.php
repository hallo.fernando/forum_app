@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
@endsection
@section('title')
    Level
@endsection
@section('content')
    <div class="">
        <div class="row justify-content-center">

            @include('layouts.notif')

            <div class="col-md-12 mt-2">
                <div class="card">
                    <div class="card-header">
                        Level Control
                    </div>
                    <div class="card-body">

                        {{-- filter:start --}}
                        {{-- <form class="row g-3" method="GET" autocomplete="off">
                            <div class="col-md-4 pt-2">
                                <input name="Sname" value="{{ request('Sname') }}" type="search" id="Sname"
                                    class="form-control" placeholder="Name">
                            </div>


                            <div class="row pl-3 pt-2">
                                <div class="col-md">
                                    <button type="submit" class="btn btn-primary mb-3">Search</button>
                                    <button type="submit" class="btn btn-danger mb-3 reset-btn">Reset</button>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <hr>
                            </div>
                        </form> --}}
                        {{-- filter:end --}}

                        <a href="{{ route('level.create') }}" class="btn btn-sm btn-primary" style="">Add Level</a>
                        <br>
                        <br>

                        <table class="table table-responsive-xs text-center table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th style="background-color:white; vertical-align:middle" rowspan='2'>Level</th>
                                    <th style="background-color:white" colspan='2'>Control</th>
                                </tr>
                                <tr>
                                    <th style="background-color:white" class="col-2">Edit</th>
                                    <th style="background-color:white" class="col-2">Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($level as $level)
                                    <tr>
                                        <td> {{ $level->name }} </td>
                                        <td> <a href="{{ route('level.edit', $level->id) }}">
                                                <span class="fa fa-edit btn btn-primary"></span>
                                            </a>
                                        </td>
                                        <td>
                                            <button type="button" class="fa fa-trash btn btn-danger" data-toggle="modal"
                                                data-target="#delete-{{ $level->id }}">
                                            </button>

                                            <div class="modal fade" id="delete-{{ $level->id }}" tabindex="-1"
                                                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">
                                                                Delete Level Control</h5>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="alert alert-danger alert-dismissible fade show"
                                                                role="alert">
                                                                You are about to <strong>DELETE</strong> Level
                                                                [{{ $level->name }}].
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <a href="{{ route('level.destroy', $level->id) }}"
                                                                class="btn btn-danger">Delete</a>
                                                            {{-- <a href="#" class="btn btn-danger">Delete</a> --}}
                                                            <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#posisitable').DataTable({
                searching: false,
                pageLength: 100
            });
        });
        $('.reset-btn').on('click', function() {
            $('#Sname').val(null).trigger("change");
            $('#Sdivisi').val(null).trigger("change");
        });
    </script>
@endpush
