@extends('layouts.app')
@section('css')
    <style>
        /* CheckBox */
        .check-text input {
            display: none;
        }

        .check-text input~span {
            color: #333;
            cursor: pointer;
            padding: 6px;
            border-radius: 2px;
            font-weight: 200;
        }

        .check-text input~.checked {
            display: none;
        }

        .check-text input:checked~.checked {
            display: inline-block;
        }

        .check-text input:checked~.unchecked {
            display: none;
        }
    </style>
@endsection
@section('title')
    Edit User
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @php
                use Carbon\Carbon;
            @endphp
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 style="float:left;">Edit Data User</h5>
                        <a href="{{ route('user.index') }}" class="btn btn-sm btn-primary" style="float: right">Back</a>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('user.update', $user->id) }}" method="POST" enctype="multipart/form-data"
                            autocomplete="off">
                            @csrf
                            @method('PUT')
                            <h2 style="margin-top:0; text-align:center"><strong>Edit</strong></h2>
                            <hr>
                            <div class="row py-2">
                                <div class="col-6">
                                    <label style="font-weight: bold" for="">Name</label>
                                    @include('layouts.error', ['name' => 'name'])
                                    <input type="text" class="form-control" name="name" placeholder="Enter a name"
                                        value="{{ $user->name }}" required>
                                    <label style="color: forestgreen;font-size: 10px" for="">*Minimal 4 karakter
                                        (huruf)</label>
                                </div>
                                <div class="col-6">
                                    <label style="font-weight: bold" for="">Email (Docs Staff)</label>
                                    @include('layouts.error', ['name' => 'email'])
                                    <input type="email" class="form-control" name="email" placeholder="Enter a email"
                                        value="{{ $user->email }}" required>
                                    <label style="color: forestgreen;font-size: 10px" for="">*Contoh:
                                        example@example.com</label>
                                </div>
                            </div>

                            <div class="row py-2">
                                <div class="col-6">
                                    <label style="font-weight: bold" for="">Bank</label>
                                    <select name="bank" class="form-control formatRek" id="bank" required>
                                        <option value="" disabled>Select Bank</option>
                                        <option data-length=".{12,}" value="BCA"
                                            {{ $user->bank == 'BCA' ? 'selected' : '' }}>BCA</option>
                                        <option data-length=".{11,}" value="BCA Syariah"
                                            {{ $user->bank == 'BCA Syariah' ? 'selected' : '' }}>BCA Syariah</option>
                                        <option data-length=".{12,}" value="BNI"
                                            {{ $user->bank == 'BNI' ? 'selected' : '' }}>BNI</option>
                                        <option data-length=".{19,}" value="BRI"
                                            {{ $user->bank == 'BRI' ? 'selected' : '' }}>BRI</option>
                                        <option data-length=".{12,}" value="BSI"
                                            {{ $user->bank == 'BSI' ? 'selected' : '' }}>BSI</option>
                                        <option data-length=".{19,}" value="BTN"
                                            {{ $user->bank == 'BTN' ? 'selected' : '' }}>BTN</option>
                                        <option data-length=".{15,}" value="CIMB Niaga"
                                            {{ $user->bank == 'CIMB Niaga' ? 'selected' : '' }}>CIMB Niaga</option>
                                        <option data-length=".{12,}" value="Danamon"
                                            {{ $user->bank == 'Danamon' ? 'selected' : '' }}>Danamon</option>
                                        <option data-length=".{16,}" value="Mandiri"
                                            {{ $user->bank == 'Mandiri' ? 'selected' : '' }}>Mandiri</option>
                                        <option data-length=".{12,}" value="Maybank"
                                            {{ $user->bank == 'Maybank' ? 'selected' : '' }}>Maybank</option>
                                        <option data-length=".{13,}" value="Mestika"
                                            {{ $user->bank == 'Mestika' ? 'selected' : '' }}>Mestika</option>
                                        <option data-length=".{15,}" value="OCBC NISP"
                                            {{ $user->bank == 'OCBC NISP' ? 'selected' : '' }}>OCBC NISP</option>
                                        <option data-length=".{11,}" value="Panin Bank"
                                            {{ $user->bank == 'Panin Bank' ? 'selected' : '' }}>Panin Bank</option>
                                        <option data-length=".{14,}" value="Union Bank"
                                            {{ $user->bank == 'Union Bank' ? 'selected' : '' }}>Union Bank</option>
                                    </select>
                                    <label style="color: forestgreen;font-size: 10px" for="">*Wajib Pilih</label>
                                </div>
                                <div class="col-6">
                                    <label style="font-weight: bold" for="">Office</label>
                                    <select name="office" class="form-control" required>
                                        <option value="" disabled selected>Select Office</option>
                                        <option value="CT" {{ $user->office == 'CT' ? 'selected' : null }}>CT</option>
                                        <option value="GV" {{ $user->office == 'GV' ? 'selected' : null }}>GV</option>
                                    </select>
                                    <label style="color: forestgreen;font-size: 10px" for="">*Wajib Pilih</label>
                                </div>
                            </div>

                            <div class="row py-2">
                                <div class="col-6">
                                    <label style="font-weight: bold" for="">Rekening Number</label>
                                    @if ($user->bank == 'BCA' || $user->bank == 'BNI' || $user->bank == 'BSI' || $user->bank == 'Maybank')
                                        <input id="rekening" type="text" class="form-control" pattern=".{12,}"
                                            name="nomor_rekening" placeholder="xxx-xxx-xxxx"
                                            value="{{ $user->nomor_rekening }}" required>
                                    @elseif ($user->bank == 'BCA Syariah' || $user->bank == 'Panin Bank')
                                        <input id="rekening" type="text" class="form-control" pattern=".{11,}"
                                            name="nomor_rekening" placeholder="xxxx-xxxxxx"
                                            value="{{ $user->nomor_rekening }}" required>
                                    @elseif ($user->bank == 'Danamon')
                                        <input id="rekening" type="text" class="form-control" pattern=".{12,}"
                                            name="nomor_rekening" placeholder="xxxx-xxxx-xx"
                                            value="{{ $user->nomor_rekening }}" required>
                                    @elseif ($user->bank == 'Mestika')
                                        <input id="rekening" type="text" class="form-control" pattern=".{13,}"
                                            name="nomor_rekening" placeholder="xx-xxx-xxxxxx"
                                            value="{{ $user->nomor_rekening }}" required>
                                    @elseif ($user->bank == 'OCBC NISP')
                                        <input id="rekening" type="text" class="form-control" pattern=".{15,}"
                                            name="nomor_rekening" placeholder="xxx-xx-xxxx-xxx"
                                            value="{{ $user->nomor_rekening }}" required>
                                    @elseif ($user->bank == 'Union Bank')
                                        <input id="rekening" type="text" class="form-control" pattern=".{14,}"
                                            name="nomor_rekening" placeholder="xxxx-xxxx-xxxx"
                                            value="{{ $user->nomor_rekening }}" required>
                                    @elseif ($user->bank == 'CIMB Niaga')
                                        <input id="rekening" type="text" class="form-control" pattern=".{15,17}"
                                            name="nomor_rekening" placeholder="xxx-xx-xxx-xxxx / xxx-xx-xxx-xxxxxx"
                                            value="{{ $user->nomor_rekening }}" required>
                                    @elseif ($user->bank == 'Mandiri')
                                        <input id="rekening" type="text" class="form-control" pattern=".{16,}"
                                            name="nomor_rekening" placeholder="xxx-xx-xxxxxxx-x"
                                            value="{{ $user->nomor_rekening }}" required>
                                    @elseif ($user->bank == 'BRI')
                                        <input id="rekening" type="text" class="form-control" pattern=".{19,}"
                                            name="nomor_rekening" placeholder="xxxx-xx-xxxxxx-xx-x"
                                            value="{{ $user->nomor_rekening }}" required>
                                    @elseif ($user->bank == 'BTN')
                                        <input id="rekening" type="text" class="form-control" pattern=".{19,}"
                                            name="nomor_rekening" placeholder="xxxx-xxxx-xxxx-xxxx"
                                            value="{{ $user->nomor_rekening }}" required>
                                    @else
                                        <input id="rekening" type="text" class="form-control" pattern=".{12,}"
                                            name="nomor_rekening" placeholder="Enter your bank account"
                                            value="{{ $user->nomor_rekening }}" required>
                                    @endif
                                    <label style="color: forestgreen;font-size: 10px" for="">*Hanya Angka
                                        saja</label>
                                </div>
                                <div class="col-6 divisi-select">
                                    <label style="font-weight: bold" for="">Divisi</label>
                                    <select name="divisi_id" class="form-control" required>
                                        <option value="" disabled selected>Select Divisi</option>
                                        @forelse ($divisi as $divisi)
                                            @php
                                                if ($divisi->id == $user->divisi) {
                                                    $select = 'selected';
                                                } else {
                                                    $select = '';
                                                }
                                                echo "<option $select data-posisi='$divisi->id' value='$divisi->id'>" . $divisi['name'] . '</option>';
                                            @endphp
                                        @empty
                                            <option value="" selected>No Data</option>
                                        @endforelse
                                    </select>
                                    <label style="color: forestgreen;font-size: 10px" for="">*Wajib Pilih</label>
                                </div>
                            </div>

                            <div class="row py-2">
                                <div class="col-6">
                                    <label style="font-weight: bold" for="">Status Work</label>
                                    <select name="status_work" class="form-control" required>
                                        <option value="" disabled>Select Status</option>
                                        <option value="0" {{ $user->status_work == 0 ? 'selected' : null }}>
                                            Nonaktif
                                        </option>
                                        <option value="1" {{ $user->status_work == 1 ? 'selected' : null }}>Aktif
                                        </option>
                                        <option value="2" {{ $user->status_work == 2 ? 'selected' : null }}>Cuti
                                        </option>
                                        <option value="3" {{ $user->status_work == 3 ? 'selected' : null }}>Resign
                                        </option>
                                    </select>
                                    <label style="color: forestgreen;font-size: 10px" for="">*Wajib Pilih</label>
                                </div>
                                <div class="col-6">
                                    <label style="font-weight: bold" for="">Posisi</label>
                                    <select name="level_id" id="level_id" class="form-control" required>
                                        <option value="" disabled>Select Posisi</option>
                                        @forelse ($level as $level)
                                            @php
                                                if ($level->id == $user->status_level) {
                                                    $select = 'selected';
                                                } else {
                                                    $select = '';
                                                }
                                                echo "<option $select value='$level->id'>" . $level['name'] . '</option>';
                                            @endphp
                                        @empty
                                            <option value="" selected>No Data</option>
                                        @endforelse
                                    </select>
                                    <label style="color: forestgreen;font-size: 10px" for="">*Wajib Pilih</label>
                                </div>
                            </div>
                            <div class="row py-2">
                                <div class="col-4">
                                    <label style="font-weight: bold" for="">Join Date</label>
                                    <input type="date" class="form-control" name="tanggal_gabung"
                                        value="{{ $user->tanggal_gabung }}" required>
                                    <label style="color: forestgreen;font-size: 10px" for="">*Date</label>
                                </div>
                                <div class="col-4">
                                    <label style="font-weight: bold" for="">Cuti</label>
                                    <input type="text" class="form-control" placeholder="Enter Cuti/Year"
                                        name="cuti" value="{{ $user->cuti }}" required>
                                    <label style="color: forestgreen;font-size: 10px" for="">*Hanya Angka
                                        Saja</label>
                                </div>
                                <div class="col-4">
                                    <label style="font-weight: bold" for="">Reset Password</label>
                                    <div class="input-group ">
                                        <input type="password" class="form-control" placeholder="Enter a new password"
                                            name="password" id="password" value="">
                                        <span style="align-self: center" class="px-2">
                                            <label class="check-text">
                                                <input type="checkbox" id="checkbox">
                                                <span class="fa fa-eye checked"></span>
                                                <span class="fa fa-eye-slash unchecked"></span>
                                            </label>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row py-2">
                                <div class="col-4">
                                    <label style="font-weight: bold" for="">Salary</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">Rp.</span>
                                        <input type="text" class="form-control currency_format" min="0"
                                            name="gaji" placeholder="Enter Salary" value="{{ $user->gaji }}"
                                            required>
                                    </div>
                                    <label style="color: forestgreen;font-size: 10px" for="">*Hanya Angka
                                        saja</label>
                                </div>
                                <div class="col-4">
                                    <label style="font-weight: bold" for="">UM/Daily</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">Rp.</span>
                                        <input type="text" class="form-control currency_format" min="0"
                                            name="um_harian" placeholder="Enter UM/Harian"
                                            value="{{ $user->um_harian }}" required>
                                    </div>
                                    <label style="color: forestgreen;font-size: 10px" for="">*Hanya Angka
                                        saja</label>
                                </div>
                                <div class="col-4">
                                    <label style="font-weight: bold" for="">Subsidi</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">Rp.</span>
                                        <input type="text" class="form-control currency_format" min="0"
                                            name="subsidi_lainnya" placeholder="Enter Subsidi"
                                            value="{{ $user->subsidi_lainnya }}" required>
                                    </div>
                                    <label style="color: forestgreen;font-size: 10px" for="">*Hanya Angka
                                        saja</label>
                                </div>
                            </div>

                            <div class="row p-3">
                                <button class="btn btn-primary col-md-2" type="submit">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cleave.js/1.6.0/cleave.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#checkbox').on('change', function() {
                $('#password').attr('type', $('#checkbox').prop('checked') == true ? "text" : "password");
            });

            $('.currency_format').toArray().forEach(function(field) {
                new Cleave(field, {
                    numeral: true,
                    numeralThousandsGroupStyle: 'thousand'
                })
            });

            $('.formatRek').on('change', function() {
                const length = $('#bank option:selected').data('length');

                $('[name=nomor_rekening]').val('');
                $('[name=nomor_rekening]').attr('pattern', length);
                if (document.getElementById('bank').value == "BCA" ||
                    document.getElementById('bank').value == "BNI" ||
                    document.getElementById('bank').value == "BSI" ||
                    document.getElementById('bank').value == "Maybank") {
                    $('[name=nomor_rekening]').attr('placeholder', 'xxx-xxx-xxxx');
                }
                if (document.getElementById('bank').value == "BCA Syariah" ||
                    document.getElementById('bank').value == "Panin Bank") {
                    $('[name=nomor_rekening]').attr('placeholder', 'xxxx-xxxxxx');
                }
                if (document.getElementById('bank').value == "Danamon") {
                    $('[name=nomor_rekening]').attr('placeholder', 'xxxx-xxxx-xx / xxxx-xxxx-xxxx');
                }
                if (document.getElementById('bank').value == "Mestika") {
                    $('[name=nomor_rekening]').attr('placeholder', 'xx-xxx-xxxxxx');
                }
                if (document.getElementById('bank').value == "OCBC NISP") {
                    $('[name=nomor_rekening]').attr('placeholder', 'xxx-xx-xxxx-xxx');
                }
                if (document.getElementById('bank').value == "Union Bank") {
                    $('[name=nomor_rekening]').attr('placeholder', 'xxxx-xxxx-xxxx');
                }
                if (document.getElementById('bank').value == "CIMB Niaga") {
                    $('[name=nomor_rekening]').attr('placeholder', 'xxx-xx-xxx-xxxx / xxx-xx-xxx-xxxxxx');
                }
                if (document.getElementById('bank').value == "Mandiri") {
                    $('[name=nomor_rekening]').attr('placeholder', 'xxx-xx-xxxxxxx-x');
                }
                if (document.getElementById('bank').value == "BRI") {
                    $('[name=nomor_rekening]').attr('placeholder', 'xxxx-xx-xxxxxx-xx-x');
                }
                if (document.getElementById('bank').value == "BTN") {
                    $('[name=nomor_rekening]').attr('placeholder', 'xxxx-xxxx-xxxx-xxxx');
                }
            });

            function bca_format(value) {
                var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');
                if (v.length > 3)
                    v = [v.slice(0, 3), '-', v.slice(3)].join('');
                if (v.length > 7)
                    v = [v.slice(0, 7), '-', v.slice(7)].join('');
                if (v.length > 12)
                    v = v.slice(0, 12);
                return v;
            }

            function panin_format(value) {
                var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');
                if (v.length > 4)
                    v = [v.slice(0, 4), '-', v.slice(4)].join('');
                if (v.length > 11)
                    v = v.slice(0, 11);
                return v;
            }

            function danamon_format(value) {
                var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');
                if (v.length > 4)
                    v = [v.slice(0, 4), '-', v.slice(4)].join('');
                if (v.length > 9)
                    v = [v.slice(0, 9), '-', v.slice(9)].join('');
                if (v.length > 14)
                    v = v.slice(0, 14);
                return v;
            }

            function mestika_format(value) {
                var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');
                if (v.length > 2)
                    v = [v.slice(0, 2), '-', v.slice(2)].join('');
                if (v.length > 6)
                    v = [v.slice(0, 6), '-', v.slice(6)].join('');
                if (v.length > 13)
                    v = v.slice(0, 13);
                return v;
            }

            function ocbc_format(value) {
                var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');
                if (v.length > 3)
                    v = [v.slice(0, 3), '-', v.slice(3)].join('');
                if (v.length > 6)
                    v = [v.slice(0, 6), '-', v.slice(6)].join('');
                if (v.length > 11)
                    v = [v.slice(0, 11), '-', v.slice(11)].join('');
                if (v.length > 15)
                    v = v.slice(0, 15);
                return v;
            }

            function union_format(value) {
                var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');
                if (v.length > 4)
                    v = [v.slice(0, 4), '-', v.slice(4)].join('');
                if (v.length > 9)
                    v = [v.slice(0, 9), '-', v.slice(9)].join('');
                if (v.length > 14)
                    v = v.slice(0, 14);
                return v;
            }

            function cimb_format(value) {
                var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');
                if (v.length > 3)
                    v = [v.slice(0, 3), '-', v.slice(3)].join('');
                if (v.length > 6)
                    v = [v.slice(0, 6), '-', v.slice(6)].join('');
                if (v.length > 10)
                    v = [v.slice(0, 10), '-', v.slice(10)].join('');
                if (v.length > 17)
                    v = v.slice(0, 17);
                return v;
            }

            function mandiri_format(value) {
                var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');
                if (v.length > 3)
                    v = [v.slice(0, 3), '-', v.slice(3)].join('');
                if (v.length > 6)
                    v = [v.slice(0, 6), '-', v.slice(6)].join('');
                if (v.length > 14)
                    v = [v.slice(0, 14), '-', v.slice(14)].join('');
                if (v.length > 16)
                    v = v.slice(0, 16);
                return v;
            }

            function bri_format(value) {
                var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');
                if (v.length > 4)
                    v = [v.slice(0, 4), '-', v.slice(4)].join('');
                if (v.length > 7)
                    v = [v.slice(0, 7), '-', v.slice(7)].join('');
                if (v.length > 14)
                    v = [v.slice(0, 14), '-', v.slice(14)].join('');
                if (v.length > 17)
                    v = [v.slice(0, 17), '-', v.slice(17)].join('');
                if (v.length > 19)
                    v = v.slice(0, 19);
                return v;
            }

            function btn_format(value) {
                var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');
                if (v.length > 4)
                    v = [v.slice(0, 4), '-', v.slice(4)].join('');
                if (v.length > 9)
                    v = [v.slice(0, 9), '-', v.slice(9)].join('');
                if (v.length > 14)
                    v = [v.slice(0, 14), '-', v.slice(14)].join('');
                if (v.length > 19)
                    v = v.slice(0, 19);
                return v;
            }

            onload = function() {
                document.getElementById('rekening').oninput = function() {
                    if (document.getElementById('bank').value == "BCA" ||
                        document.getElementById('bank').value == "BNI" ||
                        document.getElementById('bank').value == "BSI" ||
                        document.getElementById('bank').value == "Maybank") {
                        this.value = bca_format(this.value)
                    }
                    if (document.getElementById('bank').value == "BCA Syariah" ||
                        document.getElementById('bank').value == "Panin Bank") {
                        this.value = panin_format(this.value)
                    }
                    if (document.getElementById('bank').value == "Danamon") {
                        this.value = danamon_format(this.value)
                    }
                    if (document.getElementById('bank').value == "Mestika") {
                        this.value = mestika_format(this.value)
                    }
                    if (document.getElementById('bank').value == "OCBC NISP") {
                        this.value = ocbc_format(this.value)
                    }
                    if (document.getElementById('bank').value == "CIMB Niaga") {
                        this.value = cimb_format(this.value)
                    }
                    if (document.getElementById('bank').value == "Union Bank") {
                        this.value = union_format(this.value)
                    }
                    if (document.getElementById('bank').value == "Mandiri") {
                        this.value = mandiri_format(this.value)
                    }
                    if (document.getElementById('bank').value == "BRI") {
                        this.value = bri_format(this.value)
                    }
                    if (document.getElementById('bank').value == "BTN") {
                        this.value = btn_format(this.value)
                    }
                }
            }
        });
    </script>
@endsection
