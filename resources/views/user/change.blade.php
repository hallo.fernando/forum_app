@extends('layouts.app')
@section('css')
    <style>
        /* CheckBox */
        .check-text input {
            display: none;
        }

        .check-text input~span {
            color: #333;
            cursor: pointer;
            padding: 6px;
            border-radius: 2px;
            font-weight: 200;
        }

        .check-text input~.checked {
            display: none;
        }

        .check-text input:checked~.checked {
            display: inline-block;
        }

        .check-text input:checked~.unchecked {
            display: none;
        }
    </style>
@endsection
@section('title')
    Change Password
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Change Password
                    </div>
                    <div class="card-body">
                        <form action="{{ route('user.update', Auth::user()->id) }}" method="POST" autocomplete="off">
                            @csrf
                            @method('PUT')

                            <input type="text" name="changepassword" value="1" hidden>

                            <div class="row py-2">
                                <div class="col-md-6">
                                    <label for="">Password</label>
                                    <div class="input-group">
                                        <input type="password" id="password" class="form-control"
                                            placeholder="Enter a password" name="password" required>
                                        <span style="align-self: center" class="px-2">
                                            <label class="check-text">
                                                <input type="checkbox" class="checkpass">
                                                <span class="fa fa-eye checked"></span>
                                                <span class="fa fa-eye-slash unchecked"></span>
                                            </label>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row py-2">
                                <div class="col-md-6">
                                    <label for="">Confirm Password</label>
                                    <div class="input-group">
                                        <input type="password" id="conf_password" class="form-control"
                                            placeholder="Enter confirmation password" name="conf_password" required>
                                        <span style="align-self: center" class="px-2">
                                            <label class="check-text">
                                                <input type="checkbox" class="checkconfirmpass">
                                                <span class="fa fa-eye checked"></span>
                                                <span class="fa fa-eye-slash unchecked"></span>
                                            </label>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            
                            @include('layouts.error', ['name' => 'password'])

                            <div class="row p-3">
                                <button class="btn btn-primary col-md-2" type="submit">Change</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.checkpass').on('change', function() {
                $('#password').attr('type', $('.checkpass').prop('checked') == true ? "text" : "password");
            });

            $('.checkconfirmpass').on('change', function() {
                $('#conf_password').attr('type', $('.checkconfirmpass').prop('checked') == true ? "text" :
                    "password");
            });
        });
    </script>
@endsection
