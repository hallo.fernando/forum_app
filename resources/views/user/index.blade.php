@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <style>
        .calendarspan {
            float: right;
            margin-right: 15px;
            margin-top: -25px;
            position: relative;
        }

        label {
            font-weight: bold;
        }

        @media (max-width:1500px) {
            .table-responsive-xl {
                display: block;
                width: 100%;
                overflow-x: auto;
                -webkit-overflow-scrolling: touch
            }

            .table-responsive-xl>.table-bordered {
                border: 0
            }
        }
    </style>
@endsection
@section('title')
    Data User
@endsection
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12 mt-2">
            <div class="card">
                <div class="card-header">
                    Data User
                </div>
                <div class="card-body">
                    @php
                        use Carbon\Carbon;
                    @endphp

                    {{-- filter:start --}}
                    <form class="row g-3" method="GET" autocomplete="off">

                        <div class="col-md-12">
                            <h3 style="text-align:center; font-weight:bold">Join Date</h3>
                        </div>

                        <div class="col-md-3">
                            <label>Name</label>
                            <input name="Sname" value="{{ request('Sname') }}" type="search" id="Sname"
                                class="form-control" placeholder="Full Name">
                        </div>

                        <div class="col-md-3">
                            <label>Date Range</label>
                            <input type="text" class="form-control" placeholder="mm/dd/yyyy - mm/dd/yyyy" id="Sdaterange"
                                name="daterange" value="{{ request('daterange') }}" />
                            <span class="fa fa-calendar-o calendarspan"></span>
                        </div>

                        {{-- Select Divisi Query --}}
                        <div class="col-xl-2 col-md-3">
                            <label>Divisi</label>
                            <select name="Sdivisi" id="Sdivisi" class="form-control">
                                <option value="" disabled selected>Select Divisi</option>
                                @forelse ($divisi as $divisi)
                                    @php
                                        if ($divisi->name == request('Sdivisi')) {
                                            $select = 'selected';
                                        } else {
                                            $select = '';
                                        }
                                        echo "<option $select value='$divisi->name'>" . $divisi['name'] . '</option>';
                                    @endphp
                                @empty
                                    <option value="" selected>No Data</option>
                                @endforelse
                            </select>
                        </div>
                        {{-- Select Status Query --}}
                        <div class="col-xl-2 col-md-3">
                            <label>Status Work</label>
                            <select name="Sstatus" id="Sstatus" class="form-control">
                                <option value="" disabled selected>Select Status</option>
                                <option value="0" {{ request('Sstatus') == '0' ? 'selected' : null }}>Nonaktif
                                </option>
                                <option value="1" {{ request('Sstatus') == 1 ? 'selected' : null }}>Aktif</option>
                                <option value="2" {{ request('Sstatus') == 2 ? 'selected' : null }}>Cuti</option>
                                <option value="3" {{ request('Sstatus') == 3 ? 'selected' : null }}>Resign</option>
                            </select>
                        </div>

                        <div class="col-xl-2 col-lg-6 col-md-6 col-sm-6">
                            <button style="margin-top:1.7rem" type="submit" class="btn btn-primary mb-3">Search</button>
                            <button style="margin-top:1.7rem" type="submit"
                                class="btn btn-danger mb-3 reset-btn">Reset</button>
                        </div>
                    </form>
                    {{-- filter:end --}}

                    <table class="table-responsive-xl text-center table-bordered table-striped" id="usertable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Username</th>
                                <th>Divisi</th>
                                <th>Cuti</th>
                                <th>Posisi</th>
                                <th>Office</th>
                                <th>Status Work</th>
                                <th>Join Date</th>
                                <th>Resign Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $i = 1;
                            @endphp
                            @foreach ($users as $user)
                                <tr>
                                    <td> {{ $i++ }} </td>
                                    <td> {{ $user->name }} </td>
                                    <td> {{ $user->email }} </td>
                                    <td> {{ $user->username }} </td>

                                    @php
                                        $uDName = [];
                                        $uDTime = [];
                                        $uDType = [];
                                        
                                        foreach ($user->GetDetails as $detail) {
                                            $levelName = $detail->name;
                                            array_push($uDName, $levelName);
                                        
                                            $levelDate = $detail->datetime;
                                            array_push($uDTime, $levelDate);
                                        
                                            $levelType = $detail->type;
                                            array_push($uDType, $levelType);
                                        }
                                        
                                        $cUD = count($uDType);
                                    @endphp

                                    <td>
                                        @if ((in_array(1, $uDType) && in_array(2, $uDType)) || in_array(1, $uDType))
                                            <a href="#"
                                                style="color:black;text-decoration:underline;font-style:italic"
                                                data-toggle="modal" data-target="#show-{{ $user->id }}-divisi">
                                                {{ Str::ucfirst($user->GetDivisi->name) }}
                                            </a>
                                        @elseif (in_array(2, $uDType) || empty($cUD) || $cUD == 1)
                                            {{ Str::ucfirst($user->GetDivisi->name) }}
                                        @endif

                                        <div class="modal fade" id="show-{{ $user->id }}-divisi" tabindex="-1"
                                            role="dialog">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Detail Divisi</h5>
                                                    </div>
                                                    <div class="modal-body">
                                                        <table class="table">
                                                            <tr>
                                                                <th>Date</th>
                                                                <th>Divisi</th>
                                                            </tr>
                                                            @for ($x = 1; $x <= $cUD; $x++)
                                                                @if ($uDType[$x - 1] == 1)
                                                                    <tr>
                                                                        <td>{{ Carbon::parse($uDTime[$x - 1])->isoFormat('DD-MMMM-Y') }}
                                                                        </td>
                                                                        <td>{{ Str::ucfirst($uDName[$x - 1]) }}
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                            @endfor
                                                        </table>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>

                                    <td> {{ $user->cuti }} Hari </td>

                                    <td>
                                        @if ((in_array(1, $uDType) && in_array(2, $uDType)) || in_array(2, $uDType))
                                            <a href="#"
                                                style="color:black;text-decoration:underline;font-style:italic"
                                                data-toggle="modal" data-target="#show-{{ $user->id }}-posisi">
                                                {{ Str::ucfirst($user->GetLevel->name) }}
                                            </a>
                                        @elseif (in_array(1, $uDType) || empty($cUD) || $cUD == 1)
                                            {{ Str::ucfirst($user->GetLevel->name) }}
                                        @endif

                                        <div class="modal fade" id="show-{{ $user->id }}-posisi" tabindex="-1"
                                            role="dialog">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Detail Posisi</h5>
                                                    </div>
                                                    <div class="modal-body">
                                                        <table class="table">
                                                            <tr>
                                                                <th>Date</th>
                                                                <th>Posisi</th>
                                                            </tr>
                                                            @for ($x = 1; $x <= $cUD; $x++)
                                                                @if ($uDType[$x - 1] == 2)
                                                                    <tr>
                                                                        <td>{{ Carbon::parse($uDTime[$x - 1])->isoFormat('DD-MMMM-Y') }}
                                                                        </td>
                                                                        <td>{{ Str::ucfirst($uDName[$x - 1]) }}
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                            @endfor
                                                        </table>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>

                                    <td> {{ $user->office }} </td>

                                    @if ($user->status_work == 0)
                                        <td> Nonaktif </td>
                                    @else
                                        @if ($user->status_work == 1)
                                            <td> Aktif </td>
                                        @endif
                                        @if ($user->status_work == 2)
                                            <td> Cuti </td>
                                        @endif
                                        @if ($user->status_work == 3)
                                            <td> Resign </td>
                                        @endif
                                    @endif

                                    <td>{{ Carbon::parse($user->tanggal_gabung)->isoFormat('DD-MMMM-Y') }}</td>

                                    @if (empty($user->tanggal_resign))
                                        <td>-</td>
                                    @else
                                        <td>{{ Carbon::parse($user->tanggal_resign)->isoFormat('DD-MMMM-Y') }}</td>
                                    @endif

                                    <td>
                                        <a href="{{ route('user.edit', $user->id) }}"
                                            class="btn btn-primary btn-sm btn-block">Edit</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    {{-- Datatables --}}
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.12/sorting/datetime-moment.js"></script>

    {{-- Daterange --}}
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script>
        $(document).ready(function() {
            $.fn.dataTable.moment('DD-MMMM-Y');

            $('#usertable').DataTable({
                searching: false,
                pageLength: 100
            });

            $('.reset-btn').on('click', function() {
                $('#Sdaterange').val(null).trigger("change");
                $('#Sname').val(null).trigger("change");
                $('#Sdivisi').val(null).trigger("change");
                $('#Sstatus').val(null).trigger("change");
            });

            $(function() {
                $('input[name="daterange"]').daterangepicker({
                    autoUpdateInput: false,
                    ranges: {
                        '1 Month': [moment().subtract(0, 'month').startOf('month'), moment()],
                        '3 Month': [moment().subtract(3, 'month').startOf('month'), moment()],
                        '6 Month': [moment().subtract(6, 'month').startOf('month'), moment()],
                        '12 Month': [moment().subtract(12, 'month').startOf('month'), moment()]
                    }
                });

                $('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
                    $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate
                        .format('MM/DD/YYYY'));
                });

                $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
                    $(this).val('');
                });
            });
        });
    </script>
@endpush
