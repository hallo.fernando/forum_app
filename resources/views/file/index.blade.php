@extends('layouts.app')
@section('title')
    File Upload
@endsection
@section('content')
    <div class="">
        <div class="row justify-content-center">

            @include('layouts.notif')

            <div class="col-md-12 mt-2">
                <div class="card">
                    <div class="card-header">
                        File Upload
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <form method="POST" enctype="multipart/form-data" id="upload-file"
                                    action="{{ route('file.store') }}">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="path">Select Path</label>
                                                <select name="path" class="form-control" id="path">
                                                    <option value="test">TEST</option>
                                                    <option value="prod">PROD</option>
                                                </select>
                                                <br>
                                                <div class="custom-file">
                                                    <input type="file" name="file" class="custom-file-input"
                                                        id="customFile" required>
                                                    <label class="custom-file-label" for="customFile">Choose
                                                        file...</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-primary" id="submit">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-6">
                                @foreach ($files as $file)
                                    @if ($file != '.gitignore')
                                        <li>{{ $file }}</li>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        // Add the following code if you want the name of the file appear on select
        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });
    </script>
@endpush
