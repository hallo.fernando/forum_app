@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
@endsection
@section('title')
    Whitelist IP
@endsection
@section('content')
    <div class="">
        <div class="row justify-content-center">

            @include('layouts.notif')

            <div class="col-md-12 mt-2">
                <div class="card">
                    <div class="card-header">
                        Whitelist
                    </div>
                    <div class="card-body">

                        <form action="{{ route('wl.store') }}" method="POST" enctype="multipart/form-data"
                            autocomplete="off">
                            @csrf
                            <div class="row py-2">
                                <div class="col-6">
                                    <label style="font-weight: bold" for="">Add Whitelist IP</label>
                                    @include('layouts.error', ['name' => 'ips'])
                                    <input type="text" class="form-control" name="ips" placeholder="Enter an IP" required
                                        value="{{ old('ips') }}">
                                </div>
                                <div class="col-6 pt-4" style="margin-top:3px">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            </div>
                        </form>

                        <table class="table-responsive-sm text-center table-bordered table-striped" id="divisitable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>IP</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach ($whitelist as $wl)
                                    <tr>
                                        <td> {{ $i++ }} </td>
                                        <td> {{ str_replace('/32', '', $wl->ips) }} </td>
                                        <td> <a href="{{ route('wl.destroy', $wl->id) }}"
                                                class="btn btn-danger btn-sm btn-block">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#divisitable').DataTable({
                searching: false,
                pageLength: 100
            });
        });
    </script>
@endpush
