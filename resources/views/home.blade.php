@extends('layouts.app')
@section('css')
    <style>
        .btn-centered {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100px;
        }

        .card-green {
            background: #1ab394;
            /* background: linear-gradient(to right,#1ab394,#07f3c4); */
        }

        .card-red {
            /* background: linear-gradient(to right, #dc3545, #ff5f6f); */
            background: #ec4758;
        }

        /* CheckBox */
        .check-text input {
            display: none;
        }

        .check-text input~span {
            color: #fff;
            cursor: pointer;
            padding: 6px;
            border-radius: 2px;
            font-weight: 200;
        }

        .check-text input~.checked {
            display: none;
        }

        .check-text input:checked~.checked {
            display: inline-block;
        }

        .check-text input:checked~.unchecked {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">

            @include('layouts.notif')

            @php
                use Carbon\Carbon;
            @endphp

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="text-center">
                            <h1>Hello, {{ Auth::user()->name }}!</h1>
                            <h4>This is your profile page.</h4>
                        </div>
                        <hr>

                        {{-- <div class="btn-centered btn-profile">
                            <button id="profileshow" class="btn btn-primary">Show Information</button>
                        </div>

                        <div class="row profile" style="display:none">
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-header text-white card-green">
                                        Join Date
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <h3 style="color:#01a9ac">
                                                    {{ Carbon::parse(Auth::user()->tanggal_gabung)->isoFormat('DD-MMMM-Y') }}
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-header text-white card-green">
                                        Sisa Cuti
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <h3 type="button"
                                                    style="color:#01a9ac;text-decoration:underline;font-style:italic"
                                                    data-toggle="modal" data-target="#show-data-cuti">
                                                    {{ Auth::user()->sisa_cuti }} Hari
                                                </h3>
                                                <div class="modal fade" id="show-data-cuti">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">
                                                                    Detail Cuti</h5>
                                                            </div>
                                                            <div class="modal-body">
                                                                <table
                                                                    class="table table-responsive-lg text-center table-bordered table-striped">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Date</th>
                                                                            <th>Return</th>
                                                                            <th>Days</th>
                                                                            <th>Status</th>
                                                                            <th>Edit Status</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        @foreach ($usercuti as $cuti)
                                                                            <tr>
                                                                                <td>{{ Carbon::parse($cuti->from)->format('d-M-Y') }}
                                                                                </td>
                                                                                <td>{{ Carbon::parse($cuti->till)->format('d-M-Y') }}
                                                                                </td>
                                                                                <td>{{ $cuti->leaveDay }} Hari</td>
                                                                                <td>
                                                                                    @if (empty($cuti->approvedBy))
                                                                                        <div style="padding:5px; border:1px solid; border-radius:30px"
                                                                                            class="btn-warning">
                                                                                            Waiting...</div>
                                                                                    @elseif($cuti->isApproved)
                                                                                        <div style="padding:5px; border:1px solid; border-radius:30px"
                                                                                            class="btn-primary">Approved
                                                                                        </div>
                                                                                        <span style="font-size:10px">by
                                                                                            [{{ $cuti->approvedBy }}]</span>
                                                                                    @else
                                                                                        <div tabindex="0" type="button"
                                                                                            class="btn-danger"
                                                                                            style="padding:5px; border:1px solid; border-radius:30px"
                                                                                            data-toggle="popover"
                                                                                            data-trigger="focus"
                                                                                            title="Reject Reason"
                                                                                            data-content="{{ $cuti->reason }}">
                                                                                            Rejected
                                                                                        </div>
                                                                                        <span style="font-size:10px">by
                                                                                            [{{ $cuti->approvedBy }}]</span>
                                                                                    @endif
                                                                                </td>
                                                                                <td>
                                                                                    @if ($cuti->isEdited)
                                                                                        <div tabindex="0" type="button"
                                                                                            class="btn-warning"
                                                                                            style="color:black; padding:5px; border:1px solid; border-radius:30px"
                                                                                            data-toggle="popover"
                                                                                            data-trigger="focus"
                                                                                            title="Edit Reason"
                                                                                            data-content="{{ $cuti->editReason }}">
                                                                                            Edited
                                                                                        </div>
                                                                                        <span style="font-size:10px">by
                                                                                            [{{ $cuti->approvedBy }}]</span>
                                                                                    @else
                                                                                        -
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="card">
                                    <div class="card-header text-white card-green">
                                        Kode Meeting
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <h3 style="color:#01a9ac">
                                                    [Generate Code]
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="row profile mt-3" style="display:none">
                            <div class="col-md-4">
                                <div class="card card-green">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-8">
                                                <div style="color:#fff">Salary</div>
                                                <h3 id="salary" style="color:#fff">
                                                    Rp. {{ preg_replace('/[0-9]/', '*', Auth::user()->gaji) }}
                                                </h3>
                                            </div>
                                            <div class="col-4 mt-3">
                                                <label class="check-text" style="float:right">
                                                    <input type="checkbox" class="checksalary">
                                                    <span class="fa fa-eye checked"></span>
                                                    <span class="fa fa-eye-slash unchecked"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="card card-green">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-8">
                                                <div style="color:#fff">UM/Daily</div>
                                                <h3 id="umharian" style="color:#fff">
                                                    Rp. {{ preg_replace('/[0-9]/', '*', Auth::user()->um_harian) }}
                                                </h3>
                                            </div>
                                            <div class="col-4 mt-3">
                                                <label class="check-text" style="float:right">
                                                    <input type="checkbox" class="checkum">
                                                    <span class="fa fa-eye checked"></span>
                                                    <span class="fa fa-eye-slash unchecked"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="card card-green">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-8">
                                                <div style="color:#fff">Subsidi</div>
                                                <h3 id="subsidi" style="color:#fff">
                                                    Rp. {{ preg_replace('/[0-9]/', '*', Auth::user()->subsidi_lainnya) }}
                                                </h3>
                                            </div>
                                            <div class="col-4 mt-3">
                                                <label class="check-text" style="float:right">
                                                    <input type="checkbox" class="checksubsidi">
                                                    <span class="fa fa-eye checked"></span>
                                                    <span class="fa fa-eye-slash unchecked"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row profile mt-3" style="display:none">
                            <div class="col-md-2">
                            </div>

                            @foreach ($user as $user)
                                @php
                                    $uDName = [];
                                    $uDTime = [];
                                    $uDType = [];
                                    
                                    $lastDivTime = [];
                                    $lastPosTime = [];
                                    
                                    foreach ($user->GetDetails as $detail) {
                                        if ($detail->type == 1) {
                                            array_push($lastDivTime, $detail->datetime);
                                        }
                                        if ($detail->type == 2) {
                                            array_push($lastPosTime, $detail->datetime);
                                        }
                                    
                                        $levelName = $detail->name;
                                        array_push($uDName, $levelName);
                                    
                                        $levelDate = $detail->datetime;
                                        array_push($uDTime, $levelDate);
                                    
                                        $levelType = $detail->type;
                                        array_push($uDType, $levelType);
                                    }
                                    
                                    $cUD = count($uDType);
                                @endphp
                                <div class="col-md-4">
                                    <div class="card">
                                        <div class="card-header text-white card-green">
                                            Mutasi
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    @if ((in_array(1, $uDType) && in_array(2, $uDType)) || in_array(1, $uDType))
                                                        <span style="color:#01a9ac; font-size:10px">
                                                            {{ Carbon::parse(end($lastDivTime))->isoFormat('DD-MMMM-Y') }}
                                                        </span>
                                                        <h3 href="#"
                                                            style="cursor:pointer; color:#01a9ac;text-decoration:underline;font-style:italic"
                                                            data-toggle="modal"
                                                            data-target="#show-{{ $user->id }}-divisi">
                                                            {{ Str::ucfirst($user->GetDivisi->name) }}
                                                        </h3>
                                                    @elseif (in_array(2, $uDType) || empty($cUD) || $cUD == 1)
                                                        <span style="color:#01a9ac; font-size:10px">
                                                            {{ Carbon::parse($user->tanggal_gabung)->isoFormat('DD-MMMM-Y') }}
                                                        </span>
                                                        <h3 style="color:#01a9ac">
                                                            {{ Str::ucfirst($user->GetDivisi->name) }}
                                                        </h3>
                                                    @endif
                                                </div>

                                                <div class="modal fade" id="show-{{ $user->id }}-divisi">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">
                                                                    Detail Divisi</h5>
                                                            </div>
                                                            <div class="modal-body">
                                                                <table
                                                                    class="table table-striped table-bordered text-center">
                                                                    <tr>
                                                                        <th class="col-md-5">Date</th>
                                                                        <th>Divisi</th>
                                                                    </tr>
                                                                    @for ($x = 1; $x <= $cUD; $x++)
                                                                        @if ($uDType[$x - 1] == 1)
                                                                            <tr>
                                                                                <td>{{ Carbon::parse($uDTime[$x - 1])->isoFormat('DD-MMMM-Y') }}
                                                                                </td>
                                                                                <td>{{ Str::ucfirst($uDName[$x - 1]) }}
                                                                                </td>
                                                                            </tr>
                                                                        @endif
                                                                    @endfor
                                                                </table>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="card">
                                        <div class="card-header text-white card-green">
                                            Promosi
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    @if ((in_array(1, $uDType) && in_array(2, $uDType)) || in_array(2, $uDType))
                                                        <span style="color:#01a9ac; font-size:10px">
                                                            {{ Carbon::parse(end($lastPosTime))->isoFormat('DD-MMMM-Y') }}
                                                        </span>
                                                        <h3 href="#"
                                                            style="cursor:pointer; color:#01a9ac;text-decoration:underline;font-style:italic"
                                                            data-toggle="modal"
                                                            data-target="#show-{{ $user->id }}-posisi">
                                                            {{ Str::ucfirst($user->GetLevel->name) }}
                                                        </h3>
                                                    @elseif (in_array(1, $uDType) || empty($cUD) || $cUD == 1)
                                                        <span style="color:#01a9ac; font-size:10px">
                                                            {{ Carbon::parse($user->tanggal_gabung)->isoFormat('DD-MMMM-Y') }}
                                                        </span>
                                                        <h3 style="color:#01a9ac">
                                                            {{ Str::ucfirst($user->GetLevel->name) }}
                                                        </h3>
                                                    @endif
                                                </div>

                                                <div class="modal fade" id="show-{{ $user->id }}-posisi">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">
                                                                    Detail Posisi</h5>
                                                            </div>
                                                            <div class="modal-body">
                                                                <table
                                                                    class="table table-striped table-bordered text-center">
                                                                    <tr>
                                                                        <th class="col-md-5">Date</th>
                                                                        <th>Posisi</th>
                                                                    </tr>
                                                                    @for ($x = 1; $x <= $cUD; $x++)
                                                                        @if ($uDType[$x - 1] == 2)
                                                                            <tr>
                                                                                <td>{{ Carbon::parse($uDTime[$x - 1])->isoFormat('DD-MMMM-Y') }}
                                                                                </td>
                                                                                <td>{{ Str::ucfirst($uDName[$x - 1]) }}
                                                                                </td>
                                                                            </tr>
                                                                        @endif
                                                                    @endfor
                                                                </table>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            <div class="col-md-2">
                            </div>
                        </div>


                        <hr>

                        <div class="btn-centered btn-inout">
                            <button id="inoutshow" class="btn btn-danger">Show Details</button>
                        </div>


                        <div class="row inout" style="display:none">
                            <div class="col-md-3">
                                <div class="card card-red">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-8">
                                                <div style="color:#fff">Break (T)</div>
                                                <h3 style="color:#fff">0 x</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="card card-red">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-8">
                                                <div style="color:#fff">Break (W)</div>
                                                <h3 style="color:#fff">0 x</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="card card-red">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-8">
                                                <div style="color:#fff">Eat Time</div>
                                                <h3 style="color:#fff">0 x</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="card card-red">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-8">
                                                <div style="color:#fff">Absen</div>
                                                <h3 style="color:#fff">0 x</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                            </div>

                            <div class="col-md-4 mt-3">
                                <div class="card card-red">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div style="color:#fff">Overtime</div>
                                                @if (count($overtime) >= 1)
                                                    <h3 type="button" style="color:#fff; font-style:italic"
                                                        data-toggle="modal" data-target="#show-data-overtime">
                                                        {{ count($overtime) }} x
                                                        <span style="color:rgb(226, 226, 226); font-size:10px;">(click for detail) </span>
                                                    </h3>
                                                @else
                                                    <h3 style="color:#fff">-</h3>
                                                @endif
                                                <div class="modal fade" id="show-data-overtime">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">
                                                                    Detail Overtime</h5>
                                                            </div>
                                                            <div class="modal-body">
                                                                <table
                                                                    class="table table-responsive-lg text-center table-bordered table-striped">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>OT Date</th>
                                                                            <th>Status</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        @foreach ($overtime as $ot)
                                                                            <tr>
                                                                                <td>{{ $ot->d1 }}</td>
                                                                                </td>
                                                                                <td>
                                                                                    @if (empty($ot->actionBy) || $ot->isStaffAcc === null || $ot->isLeaderAcc === null)
                                                                                        <div style="padding:5px; border:1px solid; border-radius:30px"
                                                                                            class="btn-warning">
                                                                                            Waiting...</div>
                                                                                    @elseif($ot->isStaffAcc == $ot->isLeaderAcc)
                                                                                        @if ($ot->isStaffAcc == 1)
                                                                                            <div href="#"
                                                                                                class="btn-primary"
                                                                                                style="padding:5px; border:1px solid; border-radius:30px">
                                                                                                PAID OT
                                                                                            </div>
                                                                                        @else
                                                                                            <div href="#"
                                                                                                class="btn-danger"
                                                                                                style="padding:5px; border:1px solid; border-radius:30px">
                                                                                                UNPAID OT
                                                                                            </div>
                                                                                        @endif
                                                                                    @else
                                                                                        <div style="padding:5px; border:1px solid; border-radius:30px"
                                                                                            class="btn-dark">
                                                                                            NOT MATCH
                                                                                        </div>
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                            </div>
                        </div>

                        <hr>


                        <!-- Tabs -->
                        <ul class="nav nav-tabs" id="mainTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="appreciation-tab" data-toggle="tab" href="#appreciation"
                                    role="tab">
                                    Apresiasi
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="wrongdoings-tab" data-toggle="tab" href="#wrongdoings"
                                    role="tab">
                                    Kesalahan
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content main-tab" id="mainTabContent">
                            <div class="tab-pane fade show active" id="appreciation" role="tabpanel">
                                <table class="table table-bordered table-striped text-center">
                                    <tr>
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Type</th>
                                        <th>Description</th>
                                        <th>DateTime</th>
                                    </tr>
                                    @forelse ($appreciation as $correct)
                                        <tr>
                                            @if (empty($correct->auth_id))
                                                <td>-</td>
                                            @else
                                                <td>{{ $correct->GetAuth->name }}</td>
                                            @endif
                                            @if (empty($correct->user_id))
                                                <td>-</td>
                                            @else
                                                <td>{{ $correct->GetUser->name }}</td>
                                            @endif
                                            <td>
                                                <div style="padding:5px; border: 1px solid; border-radius:30px;"
                                                    class="btn-primary">Apresiasi</div>
                                            </td>
                                            <td>{{ $correct->description }}</td>
                                            <td>{{ $correct->datetime }}</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td>No Data</td>
                                        </tr>
                                    @endforelse
                                </table>
                            </div>
                            <div class="tab-pane fade" id="wrongdoings" role="tabpanel">
                                <table class="table table-bordered table-striped text-center">
                                    <tr>
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Type</th>
                                        <th>Description</th>
                                        <th>DateTime</th>
                                    </tr>
                                    @forelse ($wrongdoings as $wrong)
                                        <tr>
                                            @if (empty($wrong->auth_id))
                                                <td>-</td>
                                            @else
                                                <td>{{ $wrong->GetAuth->name }}</td>
                                            @endif
                                            @if (empty($wrong->user_id))
                                                <td>-</td>
                                            @else
                                                <td>{{ $wrong->GetUser->name }}</td>
                                            @endif
                                            <td>
                                                <div style="padding:5px; border: 1px solid; border-radius:30px;"
                                                    class="btn-danger">Kesalahan</div>
                                            </td>
                                            <td>{{ $wrong->description }}</td>
                                            <td>{{ $wrong->datetime }}</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="5">No Data</td>
                                        </tr>
                                    @endforelse
                                </table>
                            </div>
                        </div>
                        <!-- End of Tabs --> --}}





                        <div class="alert alert-warning">This page under contruction</div>

















                        {{-- <div class="row">
                            <div class="col-md-6 py-2">
                                <div class="row">
                                    <div class="col-md-3">
                                        Join Date
                                    </div>
                                    <div class="col-md-9">
                                        {{ Carbon::parse(Auth::user()->tanggal_gabung)->isoFormat('DD-MMMM-Y') }}
                                    </div>

                                    <div class="col-md-3">
                                        Sisa Cuti
                                    </div>

                                    <div class="col-md-9">
                                        <a href="#" style="color:black;text-decoration:underline;font-style:italic"
                                            data-toggle="modal" data-target="#show-data-cuti">
                                            {{ Auth::user()->sisa_cuti }} Hari
                                        </a>
                                        <div class="modal fade" id="show-data-cuti">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">
                                                            Detail Cuti</h5>
                                                    </div>
                                                    <div class="modal-body">
                                                        <table
                                                            class="table table-responsive-lg text-center table-bordered table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Date</th>
                                                                    <th>Return</th>
                                                                    <th>Days</th>
                                                                    <th>Status</th>
                                                                    <th>Edit Status</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($usercuti as $cuti)
                                                                    <tr>
                                                                        <td>{{ Carbon::parse($cuti->from)->format('d-M-Y') }}
                                                                        </td>
                                                                        <td>{{ Carbon::parse($cuti->till)->format('d-M-Y') }}
                                                                        </td>
                                                                        <td>{{ $cuti->leaveDay }} Hari</td>
                                                                        <td>
                                                                            @if (empty($cuti->approvedBy))
                                                                                <div style="padding:5px; border:1px solid; border-radius:30px"
                                                                                    class="btn-warning">Waiting...</div>
                                                                            @elseif ($cuti->isApproved)
                                                                                <div style="padding:5px; border:1px solid; border-radius:30px"
                                                                                    class="btn-primary">Approved</div>
                                                                            @else
                                                                                <div tabindex="0" type="button"
                                                                                    class="btn-danger"
                                                                                    style="padding:5px; border:1px solid; border-radius:30px"
                                                                                    data-toggle="popover"
                                                                                    data-trigger="focus"
                                                                                    title="Reject Reason"
                                                                                    data-content="{{ $cuti->reason }}">
                                                                                    Rejected
                                                                                </div>
                                                                            @endif
                                                                        </td>
                                                                        <td>
                                                                            @if ($cuti->isEdited)
                                                                                <div tabindex="0" type="button"
                                                                                    class="btn-warning"
                                                                                    style="color:black; padding:5px; border:1px solid; border-radius:30px"
                                                                                    data-toggle="popover"
                                                                                    data-trigger="focus" title="Edit Reason"
                                                                                    data-content="{{ $cuti->editReason }}">
                                                                                    Edited
                                                                                </div>
                                                                            @else
                                                                                -
                                                                            @endif
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        Salary
                                    </div>
                                    <div class="col-md-9">
                                        Rp. {{ preg_replace('/[0-9]/', '*', Auth::user()->gaji) }}
                                    </div>

                                    <div class="col-md-3">
                                        UM/Daily
                                    </div>
                                    <div class="col-md-9">
                                        Rp. {{ preg_replace('/[0-9]/', '*', Auth::user()->um_harian) }}
                                    </div>

                                    <div class="col-md-3">
                                        Subsidi
                                    </div>
                                    <div class="col-md-9">
                                        Rp. {{ preg_replace('/[0-9]/', '*', Auth::user()->subsidi_lainnya) }}
                                    </div>

                                    <div class="col-md-3">
                                        Kode Meeting
                                    </div>
                                    <div class="col-md-9">
                                        (Generate Code)
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 py-2">
                                <div class="row">
                                    <div class="col-md-3">
                                        Break (T)
                                    </div>
                                    <div class="col-md-9">
                                        -
                                    </div>

                                    <div class="col-md-3">
                                        Break (W)
                                    </div>
                                    <div class="col-md-9">
                                        -
                                    </div>

                                    <div class="col-md-3">
                                        Eat Time
                                    </div>
                                    <div class="col-md-9">
                                        -
                                    </div>

                                    <div class="col-md-3">
                                        Over Time
                                    </div>
                                    <div class="col-md-9">
                                        @if (count($overtime) >= 1)
                                            <a href="#" style="color:black;text-decoration:underline;font-style:italic"
                                                data-toggle="modal" data-target="#show-data-overtime">
                                                Overtime
                                            </a>
                                        @else
                                            -
                                        @endif
                                        <div class="modal fade" id="show-data-overtime">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">
                                                            Detail Overtime</h5>
                                                    </div>
                                                    <div class="modal-body">
                                                        <table
                                                            class="table table-responsive-lg text-center table-bordered table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Check In</th>
                                                                    <th>Check Out</th>
                                                                    <th>Total</th>
                                                                    <th>Status</th>
                                                                    <th>Edit Status</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($overtime as $ot)
                                                                    @php
                                                                        $chkin = Carbon::createFromFormat('Y-m-d H:i:s', $ot->d1);
                                                                        if (empty($ot->d2)) {
                                                                            $chkout = $chkin;
                                                                        } else {
                                                                            $chkout = Carbon::createFromFormat('Y-m-d H:i:s', $ot->d2);
                                                                        }
                                                                        $total_in_out = $chkin->diffInSeconds($chkout);
                                                                    @endphp
                                                                    <tr>
                                                                        <td>{{ $ot->d1 }}</td>
                                                                        <td>
                                                                            @if (empty($ot->d2))
                                                                                -
                                                                            @else
                                                                                {{ $ot->d2 }}
                                                                            @endif
                                                                        </td>
                                                                        <td>{{ Carbon::parse($total_in_out)->format('H:i:00') }}
                                                                        </td>
                                                                        <td>
                                                                            @if (empty($ot->actionBy) || $ot->isStaffAcc === null || $ot->isLeaderAcc === null)
                                                                                <div style="padding:5px; border:1px solid; border-radius:30px"
                                                                                    class="btn-warning">Waiting...</div>
                                                                            @elseif ($ot->isStaffAcc == $ot->isLeaderAcc)
                                                                                @if ($ot->isStaffAcc == 1)
                                                                                    <div href="#" class="btn-primary"
                                                                                        style="padding:5px; border:1px solid; border-radius:30px">
                                                                                        PAID OT
                                                                                    </div>
                                                                                @else
                                                                                    <div href="#" class="btn-danger"
                                                                                        style="padding:5px; border:1px solid; border-radius:30px">
                                                                                        UNPAID OT
                                                                                    </div>
                                                                                @endif
                                                                            @else
                                                                                <div style="padding:5px; border:1px solid; border-radius:30px"
                                                                                    class="btn-dark">
                                                                                    NOT MATCH
                                                                                </div>
                                                                            @endif
                                                                        </td>
                                                                        <td>
                                                                            @if ($ot->isEdited)
                                                                                <div tabindex="0" type="button"
                                                                                    class="btn-warning"
                                                                                    style="color:black; padding:5px; border:1px solid; border-radius:30px"
                                                                                    data-toggle="popover"
                                                                                    data-trigger="focus" title="Edit Reason"
                                                                                    data-placement="left"
                                                                                    data-content="{{ $ot->reason }}">
                                                                                    Edited
                                                                                </div>
                                                                                <span style="font-size:10px">
                                                                                    [by {{ $ot->editedBy }}]
                                                                                </span>
                                                                            @else
                                                                                -
                                                                            @endif
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        Absen
                                    </div>
                                    <div class="col-md-9">
                                        -
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 py-2">
                                <div class="row">
                                    <div class="col-md-3">
                                        Kesalahan
                                    </div>
                                    <div class="col-md-9">

                                    </div>

                                    <div class="col-md-3">
                                        Apresiasi
                                    </div>
                                    <div class="col-md-9">

                                    </div>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function() {
            $("#profileshow").click(function() {
                $(".profile").show(1000);
                $("#profileshow").hide(500);
                $(".btn-profile").hide();
            });
            $("#inoutshow").click(function() {
                $(".inout").show(1000);
                $("#inoutshow").hide(500);
                $(".btn-inout").hide();
            });
        });

        $(document).ready(function() {
            $('.checksalary').on('change', function() {
                if ($(this).is(':checked')) {
                    $('#salary').html("Rp. {{ number_format(Auth::user()->gaji) }}");
                } else {
                    $('#salary').html("Rp. {{ preg_replace('/[0-9]/', '*', Auth::user()->gaji) }}");
                }
            });
            $('.checkum').on('change', function() {
                if ($(this).is(':checked')) {
                    $('#umharian').html("Rp. {{ number_format(Auth::user()->um_harian) }}");
                } else {
                    $('#umharian').html(
                        "Rp. {{ preg_replace('/[0-9]/', '*', Auth::user()->um_harian) }}");
                }
            });
            $('.checksubsidi').on('change', function() {
                if ($(this).is(':checked')) {
                    $('#subsidi').html("Rp. {{ number_format(Auth::user()->subsidi_lainnya) }}");
                } else {
                    $('#subsidi').html(
                        "Rp. {{ preg_replace('/[0-9]/', '*', Auth::user()->subsidi_lainnya) }}");
                }
            });
        });
    </script>
@endpush
