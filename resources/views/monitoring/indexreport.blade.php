@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
@endsection
@section('title')
    Log
@endsection
@section('content')
    <div class="">
        <div class="row justify-content-center">

            @include('layouts.notif')

            <div class="col-md-12 mt-2">
                <div class="card">
                    <div class="card-header">
                        In-Out Report
                    </div>
                    <div class="card-body">

                        {{-- filter:start --}}
                        <form class="row g-3" method="GET" autocomplete="off">
                            @php
                                use Carbon\Carbon;
                            @endphp

                            {{-- Date Range --}}
                            <div class="col-md-6">
                                <label>Dari Tanggal</label>
                                <input type="date" id="Sstart" class="form-control" name="start_date"
                                    value="@if(request('start_date')){{ request('start_date') }}@else{{ Carbon::now()->toDateString() }}@endif">
                            </div>
                            <div class="col-md-6">
                                <label>Sampai Tanggal</label>
                                <input type="date" id="Send" class="form-control" name="end_date"
                                    value="@if(request('end_date')){{ request('end_date') }}@else{{ Carbon::now()->toDateString() }}@endif">
                            </div>

                            {{-- Select Type Query --}}
                            {{-- <div class="col-md-6 pt-2">
                                <select name="type" id="Stype" class="form-control">
                                    <option value="" selected>Select Type</option>
                                    <option value="1" {{ request('type') == 1 ? 'selected' : null }}>Login</option>
                                    <option value="2" {{ request('type') == 2 ? 'selected' : null }}>Logout</option>
                                    <option value="3" {{ request('type') == 3 ? 'selected' : null }}>Break</option>
                                    <option value="4" {{ request('type') == 4 ? 'selected' : null }}>Stop Break</option>
                                    <option value="5" {{ request('type') == 5 ? 'selected' : null }}>Lainnya</option>
                                    <option value="6" {{ request('type') == 6 ? 'selected' : null }}>Stop Lainnya</option>
                                </select>
                            </div> --}}

                            <div class="col-md-4 pt-2">
                                <input name="Sname" value="{{ request('Sname') }}" type="search" id="Sname"
                                    class="form-control" placeholder="User...">
                            </div>

                            <div class="row pl-3 pt-2">
                                <div class="col-md">
                                    <button type="submit" class="btn btn-primary mb-3">Search</button>
                                    <button type="submit" class="btn btn-danger mb-3 reset-btn">Reset</button>
                                </div>
                            </div>

                        </form>
                        {{-- filter:end --}}

                        <table id="logtable" class="table-responsive-lg text-center table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>User</th>
                                    <th>Description</th>
                                    <th>Time (A)</th>
                                    <th>Time (B)</th>
                                    <th>Total Time</th>
                                    {{-- <th>Shift</th> --}}
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                @endphp

                                @foreach ($forumlog as $forumlog)
                                    @if ($forumlog->type == 1)
                                        @if (Carbon::parse($forumlog->datetime)->format('H:i:s') > $forumlog->GetUser->GetShift->check_in)
                                            <tr>
                                                <td> {{ $i++ }} </td>
                                                <td> {{ $forumlog->GetUser->name }} </td>
                                                <td> {{ $forumlog->description }} </td>
                                                <td> {{ $forumlog->datetime }}</td>
                                                <td>-</td>
                                                {{-- @php
                                                    $start_check_in = Carbon::createFromFormat('H:i:s', $forumsetting->check_in);
                                                    $user_check_in = Carbon::parse($forumlog->datetime)->format('H:i:s');
                                                @endphp

                                                <td>{{$result = $start_check_in->diffInMinutes($user_check_in)}}</td> --}}
                                                <td>-</td>
                                                {{-- <td>{{$forumlog->GetUser->GetShift->name}}</td> --}}
                                                <td>Late</td>
                                            </tr>
                                        @endif
                                    @endif

                                    @if ($forumlog->type == 2)
                                        @if (Carbon::parse($forumlog->datetime)->format('H:i:s') < $forumlog->GetUser->GetShift->check_out)
                                            <tr>
                                                <td> {{ $i++ }} </td>
                                                <td> {{ $forumlog->GetUser->name }} </td>
                                                <td> {{ $forumlog->description }} </td>
                                                <td>-</td>
                                                <td>{{ $forumlog->datetime }}</td>
                                                <td>-</td>
                                                {{-- <td>{{$forumlog->GetUser->GetShift->name}}</td> --}}
                                                <td>Home Early</td>
                                            </tr>
                                        @endif

                                        @if (Carbon::parse($forumlog->datetime)->format('H:i:s') > $forumlog->GetUser->GetShift->check_out)
                                            <tr>
                                                <td> {{ $i++ }} </td>
                                                <td> {{ $forumlog->GetUser->name }} </td>
                                                <td> {{ $forumlog->description }} </td>
                                                <td>-</td>
                                                <td>{{ $forumlog->datetime }}</td>
                                                <td>-</td>
                                                {{-- <td>{{$forumlog->GetUser->GetShift->name}}</td> --}}
                                                <td>Overtime</td>
                                            </tr>
                                        @endif
                                    @endif

                                    @if ($forumlog->type == 3)
                                        @php
                                            $getCount = \App\Models\ForumLog::where('auth_id', $forumlog->GetUser->id)
                                                ->whereBetween('datetime', [Carbon::parse($forumlog->datetime)->format('Y-m-d 00:00:00'), Carbon::parse($forumlog->datetime)->format('Y-m-d 23:59:59')])
                                                ->where('type', 3)
                                                ->get();
                                        @endphp
                                        @if (count($getCount) > $forumlog->GetUser->GetShift->break)
                                            <tr>
                                                <td> {{ $i++ }} </td>
                                                <td> {{ $forumlog->GetUser->name }} </td>
                                                <td> {{ $forumlog->description }} </td>
                                                <td> {{ $forumlog->datetime }}</td>
                                                <td>-</td>
                                                <td>-</td>
                                                {{-- <td>{{$forumlog->GetUser->GetShift->name}}</td> --}}
                                                <td>
                                                    Total Break (T): {{ count($getCount) . 'x' }} 
                                                    (Max Break:{{ $forumlog->GetUser->GetShift->break }})
                                                </td>
                                            </tr>
                                        @endif
                                    @endif

                                    @if ($forumlog->type == 4)
                                        @php
                                            $start_time = \App\Models\ForumLog::where('auth_id', $forumlog->GetUser->id)
                                                ->where('type', 3)
                                                ->whereBetween('datetime', [Carbon::parse($forumlog->datetime)->format('Y-m-d 00:00:00'), Carbon::parse($forumlog->datetime)->format('Y-m-d 23:59:59')])
                                                ->get();
                                            
                                            $end_time = \App\Models\ForumLog::where('auth_id', $forumlog->GetUser->id)
                                                ->where('type', 4)
                                                ->whereBetween('datetime', [Carbon::parse($forumlog->datetime)->format('Y-m-d 00:00:00'), Carbon::parse($forumlog->datetime)->format('Y-m-d 23:59:59')])
                                                ->get();
                                            
                                            $start_date = [];
                                            $end_date = [];
                                            $result = [];
                                            $counter = 0;
                                            
                                            foreach ($start_time as $start) {
                                                $starts = Carbon::createFromFormat('Y-m-d H:i:s', $start->datetime);
                                                array_push($start_date, $starts);
                                            }
                                            foreach ($end_time as $end) {
                                                $ends = Carbon::createFromFormat('Y-m-d H:i:s', $end->datetime);
                                                array_push($end_date, $ends);
                                            }
                                            
                                            $arrlen = count($start_date);
                                            
                                            for ($x = 1; $x <= $arrlen; $x++) {
                                                $result[$x - 1] = $start_date[$x - 1]->diffInMinutes($end_date[$x - 1]);
                                            }
                                            
                                            for ($x = 1; $x <= $arrlen; $x++) {
                                                if ($result[$x - 1] > $forumlog->GetUser->GetShift->break_duration) {
                                                    $counter++;
                                                }
                                            }
                                            
                                        @endphp
                                        @for ($x = 1; $x <= $arrlen; $x++)
                                            @if ($result[$x - 1] > $forumlog->GetUser->GetShift->break_duration)
                                                <tr>
                                                    <td> {{ $i++ }} </td>
                                                    <td> {{ $forumlog->GetUser->name }} </td>
                                                    <td> {{ $forumlog->description }} </td>
                                                    <td> {{ $start_date[$x - 1] }}</td>
                                                    <td> {{ $end_date[$x - 1] }}</td>
                                                    <td>-</td>
                                                    {{-- <td>{{$forumlog->GetUser->GetShift->name}}</td> --}}
                                                    <td>
                                                        {{-- @for ($x = 1; $x <= $arrlen; $x++) --}}
                                                        @if ($result[$x - 1] > $forumlog->GetUser->GetShift->break_duration)
                                                            Exceed break (T) time: {{ $result[$x - 1] }} minute 
                                                            (max: {{$forumlog->GetUser->GetShift->break_duration}} minute)
                                                            <br />
                                                        @endif
                                                        {{-- @endfor --}}
                                                    </td>
                                                </tr>
                                            @endif
                                        @endfor
                                    @endif



                                    {{-- Break W --}}
                                    @if ($forumlog->type == 10)
                                        @php
                                            $getCount = \App\Models\ForumLog::where('auth_id', $forumlog->GetUser->id)
                                                ->whereBetween('datetime', [Carbon::parse($forumlog->datetime)->format('Y-m-d 00:00:00'), Carbon::parse($forumlog->datetime)->format('Y-m-d 23:59:59')])
                                                ->where('type', 10)
                                                ->get();
                                        @endphp
                                        @if (count($getCount) > $forumlog->GetUser->GetShift->break)
                                            <tr>
                                                <td> {{ $i++ }} </td>
                                                <td> {{ $forumlog->GetUser->name }} </td>
                                                <td> {{ $forumlog->description }} </td>
                                                <td> {{ $forumlog->datetime }}</td>
                                                <td>-</td>
                                                <td>-</td>
                                                {{-- <td>{{$forumlog->GetUser->GetShift->name}}</td> --}}
                                                <td>
                                                    Total Break (W): {{ count($getCount) . 'x' }} 
                                                    (Max Break:{{ $forumlog->GetUser->GetShift->break }})
                                                </td>
                                            </tr>
                                        @endif
                                    @endif

                                    @if ($forumlog->type == 11)
                                        @php
                                            $start_time = \App\Models\ForumLog::where('auth_id', $forumlog->GetUser->id)
                                                ->where('type', 10)
                                                ->whereBetween('datetime', [Carbon::parse($forumlog->datetime)->format('Y-m-d 00:00:00'), Carbon::parse($forumlog->datetime)->format('Y-m-d 23:59:59')])
                                                ->get();
                                            
                                            $end_time = \App\Models\ForumLog::where('auth_id', $forumlog->GetUser->id)
                                                ->where('type', 11)
                                                ->whereBetween('datetime', [Carbon::parse($forumlog->datetime)->format('Y-m-d 00:00:00'), Carbon::parse($forumlog->datetime)->format('Y-m-d 23:59:59')])
                                                ->get();
                                            
                                            $start_date = [];
                                            $end_date = [];
                                            $result = [];
                                            $counter = 0;
                                            
                                            foreach ($start_time as $start) {
                                                $starts = Carbon::createFromFormat('Y-m-d H:i:s', $start->datetime);
                                                array_push($start_date, $starts);
                                            }
                                            foreach ($end_time as $end) {
                                                $ends = Carbon::createFromFormat('Y-m-d H:i:s', $end->datetime);
                                                array_push($end_date, $ends);
                                            }
                                            
                                            $arrlen = count($start_date);
                                            
                                            for ($x = 1; $x <= $arrlen; $x++) {
                                                $result[$x - 1] = $start_date[$x - 1]->diffInMinutes($end_date[$x - 1]);
                                            }
                                            
                                            for ($x = 1; $x <= $arrlen; $x++) {
                                                if ($result[$x - 1] > $forumlog->GetUser->GetShift->break_duration) {
                                                    $counter++;
                                                }
                                            }
                                            
                                        @endphp
                                        @for ($x = 1; $x <= $arrlen; $x++)
                                            @if ($result[$x - 1] > $forumlog->GetUser->GetShift->break_duration)
                                                <tr>
                                                    <td> {{ $i++ }} </td>
                                                    <td> {{ $forumlog->GetUser->name }} </td>
                                                    <td> {{ $forumlog->description }} </td>
                                                    <td> {{ $start_date[$x - 1] }}</td>
                                                    <td> {{ $end_date[$x - 1] }}</td>
                                                    <td>-</td>
                                                    {{-- <td>{{$forumlog->GetUser->GetShift->name}}</td> --}}
                                                    <td>
                                                        {{-- @for ($x = 1; $x <= $arrlen; $x++) --}}
                                                        @if ($result[$x - 1] > $forumlog->GetUser->GetShift->break_duration)
                                                            Exceed break (W) time: {{ $result[$x - 1] }} minute 
                                                            (max: {{$forumlog->GetUser->GetShift->break_duration}} minute)
                                                            <br />
                                                        @endif
                                                        {{-- @endfor --}}
                                                    </td>
                                                </tr>
                                            @endif
                                        @endfor
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#logtable').DataTable({
                searching: false,
                pageLength: 100
            });
        });
        $('.reset-btn').on('click', function() {
            $('#Sstart').val(null).trigger("change");
            $('#Send').val(null).trigger("change");
            $('#Sname').val(null).trigger("change");
            $('#Stype').val(null).trigger("change");
        });
    </script>
@endpush
