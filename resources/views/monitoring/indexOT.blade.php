@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
    <style>
        .calendarspan {
            float: right;
            margin-right: 15px;
            margin-top: -25px;
            position: relative;
        }

        label {
            font-weight: bold;
        }

        .notification {
            z-index: 1;
            float: right;
            /* top: 30px; */
            right: 10px;
            background-color: #1c84c6;
            border-color: #1c84c6;
            color: white;
            text-decoration: none;
            padding: 7px 15px;
            position: relative;
            display: inline-block;
            border-radius: 5px;
        }

        td,
        th {
            vertical-align: middle !important;
        }

        thead input {
            width: 100%;
            padding: 3px;
            box-sizing: border-box;
        }

        #requestTable_filter {
            display: none;
        }

        .badge {
            position: absolute;
            top: -10px;
            right: -10px;
            padding: 5px 8px;
            color: white;
        }

        @media (max-width:640px) {
            .notification {
                top: 15px;
            }
        }

        @media (max-width:1260px) {
            .table-responsive-xl {
                display: block;
                width: 100%;
                overflow-x: auto;
                -webkit-overflow-scrolling: touch
            }

            .table-responsive-xl>.table-bordered {
                border: 0
            }
        }
    </style>
@endsection
@section('title')
    Data OverTime Request
@endsection
@section('content')
    <div class="">
        <div class="row justify-content-center">

            @include('layouts.notif')

            <div class="col-md-12 mt-2">
                <div class="card">
                    <div class="card-header">
                        Data OverTime Request
                    </div>
                    <div class="card-body">
                        @php
                            use Carbon\Carbon;
                        @endphp

                        {{-- filter:start --}}
                        <form class="row g-3" method="GET" autocomplete="off">
                            {{-- Date Range --}}
                            <div class="col-md-3">
                                <label>Dari Tanggal</label>
                                <input type="date" id="Sstart" class="form-control" name="start_date"
                                    value="@if(request('start_date')){{ request('start_date') }}@else{{ Carbon::now()->toDateString() }}@endif">
                            </div>
                            <div class="col-md-3">
                                <label>Sampai Tanggal</label>
                                <input type="date" id="Send" class="form-control" name="end_date"
                                    value="@if(request('end_date')){{ request('end_date') }}@else{{ Carbon::now()->toDateString() }}@endif">
                            </div>

                            <div class="col-md-3">
                                <label>Name</label>
                                <input name="Sname" value="{{ request('Sname') }}" type="search" id="Sname"
                                    class="form-control" placeholder="Full Name">
                            </div>

                            {{-- Select Divisi Query --}}
                            <div class="col-xl-3 col-md-3">
                                <label>Divisi</label>
                                <select name="Sdivisi" id="Sdivisi" class="form-control">
                                    <option value="" disabled selected>Select Divisi</option>
                                    @forelse ($divisi as $divisi)
                                        @php
                                            if ($divisi->name == request('Sdivisi')) {
                                                $select = 'selected';
                                            } else {
                                                $select = '';
                                            }
                                            echo "<option $select value='$divisi->name'>" . $divisi['name'] . '</option>';
                                        @endphp
                                    @empty
                                        <option value="" selected>No Data</option>
                                    @endforelse
                                </select>
                            </div>

                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3">
                                <button style="margin-top:1rem" type="submit" class="btn btn-primary mb-3">Search</button>
                                <button style="margin-top:1rem" type="submit"
                                    class="btn btn-danger mb-3 reset-btn">Reset</button>
                            </div>
                        </form>
                        {{-- filter:end --}}

                        @if (Auth::user()->getLevel->edit_overtime)
                            <button style="top:185px; position:absolute; margin-right:20px" type="button"
                                class="notification" data-toggle="modal" data-target="#request-ot">
                                @php
                                    $count = 0;
                                    foreach ($overtime as $ot) {
                                        // if (!$ot->isOTApproved && empty($ot->actionBy)) {
                                        if (empty($ot->actionBy)) {
                                            $count++;
                                        }
                                    }
                                @endphp
                                <i class="fa fa-info"></i>
                                @if ($count == 0)
                                    <span class="badge" id="notification"
                                        style="background-color: gray">{{ $count }}</span>
                                @else
                                    <span class="badge" id="notification"
                                        style="background-color: red">{{ $count }}</span>
                                @endif
                            </button>
                        @endif

                        <div class="modal fade" id="request-ot" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-xl" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">
                                            Request OverTime</h5>
                                    </div>
                                    <div class="modal-body">
                                        {{-- <a href="{{ route('ot.approveall') }}"
                                            class="btn btn-primary mb-2" style="background-color:green; float:right">
                                            Approve All
                                        </a> --}}
                                        <form action="{{ route('ot.approveall') }}" method="POST"
                                            enctype="multipart/form-data" autocomplete="off">
                                            @csrf
                                            <input type="checkbox" class="mt-2" id="checkAll"> All
                                            <input style="float:right" class="btn btn-success mb-2" type="submit"
                                                name="submit" value="Approve Checked" />
                                            <table style="width:100%"
                                                class="table table-responsive-lg table-striped table-bordered text-center"
                                                id="requestTable">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>No</th>
                                                        <th class="col-2">Name</th>
                                                        <th class="col-2">Divisi</th>
                                                        <th>Posisi</th>
                                                        <th>Check-In</th>
                                                        <th>Check-Out</th>
                                                        <th>Total</th>
                                                        <th class="col-2">Status</th>
                                                    </tr>
                                                    <tr class="filterhead">
                                                        <th>#</th>
                                                        <th>No</th>
                                                        <th class="text_search">Name</th>
                                                        <th class="select_search">Divisi</th>
                                                        <th>Posisi</th>
                                                        <th>Check-In</th>
                                                        <th>Check-Out</th>
                                                        <th>Total</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php
                                                        $noReq = 1;
                                                    @endphp
                                                    @foreach ($overtime as $ot)
                                                        @php
                                                            $chkin = Carbon::createFromFormat('Y-m-d H:i:s', $ot->d1);
                                                            if (empty($ot->d2)) {
                                                                $chkout = $chkin;
                                                            } else {
                                                                $chkout = Carbon::createFromFormat('Y-m-d H:i:s', $ot->d2);
                                                            }
                                                            $total_in_out = $chkin->diffInSeconds($chkout);
                                                        @endphp
                                                        {{-- @if (!$ot->isOTApproved && empty($ot->actionBy)) --}}
                                                        @if (empty($ot->actionBy))
                                                            <tr>
                                                                <td><input name="id[]" type="checkbox" id="checkItem"
                                                                        value="{{ $ot->id }}">
                                                                </td>
                                                                <td>{{ $noReq++ }}</td>
                                                                <td>{{ $ot->n1 }}</td>
                                                                <td>{{ $ot->n2 }}</td>
                                                                <td>{{ $ot->n3 }}</td>
                                                                <td>{{ $ot->d1 }}</td>
                                                                <td>
                                                                    @if (empty($ot->d2))
                                                                        -
                                                                    @else
                                                                        {{ $ot->d2 }}
                                                                    @endif
                                                                </td>
                                                                <td>{{ Carbon::parse($total_in_out)->format('H:i:00') }}
                                                                </td>
                                                                <td>
                                                                    <a href="{{ route('ot.approve', $ot->id) }}"
                                                                        class="btn btn-primary">
                                                                        Approve
                                                                    </a>
                                                                    <a href="{{ route('ot.reject', $ot->id) }}"
                                                                        class="btn btn-danger">
                                                                        Reject
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                </tbody>

                                            </table>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <table class="table table-responsive-lg text-center table-bordered table-striped" id="OTtable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th class="col-md-2">Name</th>
                                    <th>Divisi</th>
                                    <th>Posisi</th>
                                    <th class="col-md-1">Check In Time</th>
                                    <th class="col-md-1">Check Out Time</th>
                                    <th class="col-md-1">Total In-Out</th>
                                    <th>Status</th>
                                    <th>Edit Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach ($overtime as $ot)
                                    @php
                                        $chkin = Carbon::createFromFormat('Y-m-d H:i:s', $ot->d1);
                                        if (empty($ot->d2)) {
                                            $chkout = $chkin;
                                        } else {
                                            $chkout = Carbon::createFromFormat('Y-m-d H:i:s', $ot->d2);
                                        }
                                        $total_in_out = $chkin->diffInSeconds($chkout);
                                    @endphp
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $ot->n1 }}</td>
                                        <td>{{ $ot->n2 }}</td>
                                        <td>{{ $ot->n3 }}</td>
                                        <td>{{ $ot->d1 }}</td>
                                        <td>
                                            @if (empty($ot->d2))
                                                -
                                            @else
                                                {{ $ot->d2 }}
                                            @endif
                                        </td>
                                        <td>{{ Carbon::parse($total_in_out)->format('H:i:00') }}</td>
                                        <td>
                                            @if (empty($ot->actionBy) || $ot->isStaffAcc === null || $ot->isLeaderAcc === null)
                                                <div style="padding:5px; border:1px solid; border-radius:30px"
                                                    class="btn-warning">Waiting...</div>
                                            @elseif ($ot->isStaffAcc == $ot->isLeaderAcc)
                                                @if ($ot->isStaffAcc == 1)
                                                    <div href="#" class="btn-primary"
                                                        style="padding:5px; border:1px solid; border-radius:30px">
                                                        PAID OT
                                                    </div>
                                                    <span style="font-size:10px">[by {{ $ot->actionBy }}]</span>
                                                @else
                                                    <div href="#" class="btn-danger"
                                                        style="padding:5px; border:1px solid; border-radius:30px">
                                                        UNPAID OT
                                                    </div>
                                                    {{-- <span style="font-size:10px">[by {{ $ot->actionBy }}]</span> --}}
                                                @endif
                                            @else
                                                <div style="padding:5px; border:1px solid; border-radius:30px"
                                                    class="btn-dark">
                                                    NOT MATCH
                                                </div>
                                                {{-- <span style="font-size:10px">[by {{ $ot->actionBy }}]</span> --}}
                                            @endif
                                        </td>
                                        <td>
                                            @if ($ot->isEdited)
                                                <div tabindex="0" type="button" class="btn-warning"
                                                    style="color:black; padding:5px; border:1px solid; border-radius:30px"
                                                    data-toggle="popover" data-trigger="focus" title="Edit Reason" data-placement="left"
                                                    data-content="{{ $ot->reason }}">
                                                    Edited
                                                </div>
                                                <span style="font-size:10px">
                                                    [by {{ $ot->editedBy }}]
                                                </span>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>
                                            @if ($ot->isStaffAcc !== null && $ot->isLeaderAcc !== null)
                                                <div href="#" style="cursor:pointer;" data-toggle="modal"
                                                    data-target="#show-{{ $ot->id }}-edit">
                                                    <span class="fa fa-edit btn btn-success"></span>
                                                </div>
                                                <div class="modal fade" id="show-{{ $ot->id }}-edit"
                                                    tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                    aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">
                                                                    Edit Overtime</h5>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="{{ route('ot.update', $ot->id) }}"
                                                                    method="POST">
                                                                    @csrf
                                                                    @method('PUT')
                                                                    <label for="">Status</label>
                                                                    <select name="isOTPaid"
                                                                        class="form-control OTApprove text-center"
                                                                        id="OTApprove{{ $ot->id }}" required>
                                                                        <option style="background: #ED5565; color:#fff"
                                                                            value="0"
                                                                            {{ $ot->isLeaderAcc == '0' ? 'selected' : null }}>
                                                                            Unpaid OT</option>
                                                                        <option style="background: #1ab394; color:#fff"
                                                                            value="1"
                                                                            {{ $ot->isLeaderAcc == '1' ? 'selected' : null }}>
                                                                            Paid OT</option>
                                                                    </select>
                                                                    <br>
                                                                    <label for="">Edit Reason</label>
                                                                    <textarea class="form-control" name="reason" rows="5" required>{{ $ot->reason }}</textarea>
                                                                    <div class="row p-3">
                                                                        <button class="btn btn-success mt-2"
                                                                            type="submit">Update</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                -
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#OTtable').DataTable({
                searching: false,
                pageLength: 100,
            });
            $('#requestTable thead tr.filterhead th').each(function() {
                var title = $(this).text();
                if ($(this)[0].className === "text_search") {
                    $(this).html('<input class="form-control" type="text" placeholder="' + title + '" />');
                } else {
                    $(this).html('-');
                }
            });

            $('#requestTable').DataTable({
                initComplete: function() {
                    this.api().columns().every(function() {
                        var column = this;

                        if ($(column.header()).hasClass('select_search')) {

                            var select = $(
                                    '<select class="form-control"><option value=""></option></select>'
                                    )
                                .appendTo($(column.header()).empty())
                                .on('change', function() {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );
                                    column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                                });
                            column.data().unique().sort().each(function(d, j) {
                                select.append('<option value="' + d + '">' + d +
                                    '</option>');
                            });

                        } else {

                            var that = this;
                            $('input', this.header()).on('keyup change', function() {
                                that.search(this.value).draw();
                            });

                        }

                    });
                },
                paging: false,
                ordering: false,
                entries: false,
                info: false,
                language: {
                    emptyTable: "No request at the moment"
                }
            });
        });

        $('.reset-btn').on('click', function() {
            $('#Sstart').val(null).trigger("change");
            $('#Send').val(null).trigger("change");
            $('#Sname').val(null).trigger("change");
            $('#Sdivisi').val(null).trigger("change");
        });

        $("#checkAll").click(function() {
            $('input:checkbox').not(this).prop('checked', this.checked);
        });

        $(document).ready(function() {
            @foreach ($overtime as $ot)
                var current = $('#OTApprove{{ $ot->id }}').val();
                if (current == '0') {
                    $('#OTApprove{{ $ot->id }}').css('background', '#ED5565').css('color', '#fff');
                } else if (current == '1') {
                    $('#OTApprove{{ $ot->id }}').css('background', '#1ab394').css('color', '#fff');
                }
                $('.OTApprove').change(function() {
                    var current = $('#OTApprove{{ $ot->id }}').val();
                    if (current == '0') {
                        $('#OTApprove{{ $ot->id }}').css('background', '#ED5565').css('color',
                            '#fff');
                    } else if (current == '1') {
                        $('#OTApprove{{ $ot->id }}').css('background', '#1ab394').css('color',
                            '#fff');
                    }
                });
            @endforeach
        });

        @if (Session::has('msg') && Session::get('modal_open') == 1)
            $(function() {
                $('#request-ot').modal('show');
            });
        @endif
    </script>
@endpush
