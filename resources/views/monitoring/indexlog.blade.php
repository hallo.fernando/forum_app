@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">

    <style>
        label {
            font-weight: bold;
        }

        td,
        th {
            vertical-align: middle !important;
        }

        div.d-none.flex-sm-fill.d-sm-flex.align-items-sm-center.justify-content-sm-between{
            margin-top:15px;
        }

        @media (max-width:1300px) {
            .table-responsive-xl {
                display: block;
                width: 100%;
                overflow-x: auto;
                -webkit-overflow-scrolling: touch
            }

            .table-responsive-xl>.table-bordered {
                border: 0
            }
        }
    </style>
@endsection
@section('title')
    In-Out Log
@endsection
@section('content')
    <div class="">
        <div class="row justify-content-center">

            @include('layouts.notif')

            <div class="col-md-12 mt-2">
                <div class="card">
                    <div class="card-header">
                        In-Out Log
                    </div>
                    <div class="card-body">

                        {{-- filter:start --}}
                        <form class="row g-3" method="GET" autocomplete="off">
                            @php
                                use Carbon\Carbon;
                            @endphp

                            {{-- Date Range --}}
                            <div class="col-md-3">
                                <label>Dari Tanggal</label>
                                <input type="date" id="Sstart" class="form-control" name="start_date"
                                    value="@if(request('start_date')){{ request('start_date') }}@else{{ Carbon::now()->toDateString() }}@endif">
                            </div>
                            <div class="col-md-3">
                                <label>Sampai Tanggal</label>
                                <input type="date" id="Send" class="form-control" name="end_date"
                                    value="@if(request('end_date')){{ request('end_date') }}@else{{ Carbon::now()->toDateString() }}@endif">
                            </div>
                            {{-- End of Date Range --}}

                            <div class="col-md-2">
                                <label>Name</label>
                                <input name="Sname" value="{{ request('Sname') }}" type="search" id="Sname"
                                    class="form-control" placeholder="User...">
                            </div>

                            {{-- Select Divisi --}}
                            <div class="col-xl-2 col-md-2">
                                <label>Divisi</label>
                                <select name="Sdivisi" id="Sdivisi" class="form-control">
                                    <option value="" disabled selected>Select Divisi</option>
                                    @forelse ($divisi as $divisi)
                                        @php
                                            if ($divisi->name == request('Sdivisi')) {
                                                $select = 'selected';
                                            } else {
                                                $select = '';
                                            }
                                            echo "<option $select value='$divisi->name'>" . $divisi['name'] . '</option>';
                                        @endphp
                                    @empty
                                        <option value="" selected>No Data</option>
                                    @endforelse
                                </select>
                            </div>
                            {{-- End of Select Divisi --}}

                            <div class="row pl-3">
                                <div class="col-md">
                                    <button style="margin-top:1.7rem" type="submit"
                                        class="btn btn-primary mb-3">Search</button>
                                    <button style="margin-top:1.7rem" type="submit"
                                        class="btn btn-danger mb-3 reset-btn">Reset</button>
                                </div>
                            </div>

                        </form>
                        {{-- filter:end --}}

                        <table id="logtable" class="table table-responsive-xl text-center table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Divisi</th>
                                    <th class="text-center">Posisi</th>
                                    <th class="text-center">Check-In</th>
                                    <th class="text-center">Check-Out</th>
                                    <th class="text-center">Total In/Out</th>
                                    <th class="text-center">Break (T)</th>
                                    <th class="text-center">Break (W)</th>
                                    <th class="text-center">Eating Time</th>
                                    <th class="text-center">Meeting</th>
                                </tr>
                            </thead>
                            <tbody>
                                @include('monitoring.logdata')
                            </tbody>
                        </table>
                        {{ $chk->onEachSide(1)->withQueryString()->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#logtable').DataTable({
                searching: false,
                pageLength: 100,
                paging: false,
                entries: false,
                info: false,
            });
        });
        $('.reset-btn').on('click', function() {
            $('#Sstart').val(null).trigger("change");
            $('#Send').val(null).trigger("change");
            $('#Sname').val(null).trigger("change");
            $('#Sdivisi').val(null).trigger("change");
        });
    </script>
@endpush
