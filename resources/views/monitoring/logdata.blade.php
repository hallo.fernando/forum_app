@php
use Carbon\Carbon;
@endphp

@foreach ($chk as $key => $log)
    @php
        $inout = DB::table('collectlog')
            ->join('forumlog as t1', 't1.id', '=', 'collectlog.start')
            ->join('forumlog as t2', 't2.id', '=', 'collectlog.end')
            ->select('t1.datetime as d1', 't2.datetime as d2', DB::raw('timestampdiff(second, t1.datetime, t2.datetime) diff'))
            ->where('t1.auth_id', '=', $log->auth_id)
            ->whereDate('t1.datetime', Carbon::parse($log->d1)->format('Y-m-d'))
            ->where('t1.type', 1)
            ->get();
        
        $cInOut = count($inout);
        
        $total = 0;
        foreach ($inout as $diff) {
            $total += $diff->diff;
        }
        
        $totalM = intval($total / 60);
    @endphp
    <tr>
        <td>{{ $chk->firstItem() + $key }}</td>
        <td>{{ $log->n1 }}</td>
        <td>{{ $log->n2 }}</td>
        <td>{{ $log->n3 }}</td>
        <td>{{ $log->d1 }}</td>
        <td>
            @if (empty($log->d2))
                -
            @else
                {{ $log->d2 }}
            @endif
        </td>

        <td>
            @php
                if (empty($log->d2) || $totalM < $log->workHours) {
                    $color = 'color:red';
                    $title = 'Total in-out less than the specified criteria';
                } elseif ($log->isOTApproved) {
                    $color = 'color:blue';
                    $title = 'Overtime';
                } else {
                    $color = '';
                    $title = '';
                }
            @endphp
            @if ($log->d2 && $cInOut > 1)
                <div style="cursor:pointer; {{ $color }}; text-decoration:underline; font-style:italic"
                    data-toggle="modal" title="{{ $title }}"
                    data-target="#show-{{ $total }}{{ $log->start }}-inout">
                    {{ Carbon::parse($total)->format('H:i:00') }}
                </div>
            @else
                <div style="{{ $color }}" title="{{ $title }}">
                    @if (empty($log->d2))
                        {{ Carbon::parse(0)->format('H:i:00') }}
                    @else
                        {{ Carbon::parse($total)->format('H:i:00') }}
                    @endif
                </div>
            @endif

            @if ($log->isStaffAcc == 1)
                <span style="font-size:10px; color:black">
                    [OT {{ $log->n1 }}]
                </span>
            @endif

            @if ($log->d2 && $cInOut > 1)
                <div class="modal fade" id="show-{{ $total }}{{ $log->start }}-inout" tabindex="-1"
                    role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">
                                    Detail In-Out
                                </h5>
                            </div>
                            <div class="modal-body">
                                <table class="table">
                                    <tr>
                                        <th colspan="2">Date & Time</th>
                                        <th>Total In-Out</th>
                                    </tr>
                                    @foreach ($inout as $data)
                                        <tr>
                                            <td>{{ $data->d1 }}</td>
                                            <td>{{ $data->d2 }}</td>
                                            <td>{{ Carbon::parse($data->diff)->format('H:i:s') }}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </td>

        @php
            $BrkT = DB::table('collectlog')
                ->join('forumlog as t1', 't1.id', '=', 'collectlog.start')
                ->leftJoin('forumlog as t2', 't2.id', '=', 'collectlog.end')
                ->select('t1.datetime as d1', 't2.datetime as d2', DB::raw('timestampdiff(second, t1.datetime, t2.datetime) diff'))
                ->where('t1.auth_id', '=', $log->auth_id)
                ->where('t1.type', '=', 3);
            
            $BrkW = DB::table('collectlog')
                ->join('forumlog as t1', 't1.id', '=', 'collectlog.start')
                ->leftJoin('forumlog as t2', 't2.id', '=', 'collectlog.end')
                ->select('t1.datetime as d1', 't2.datetime as d2', DB::raw('timestampdiff(second, t1.datetime, t2.datetime) diff'))
                ->where('t1.auth_id', '=', $log->auth_id)
                ->where('t1.type', '=', 10);
            
            $BrkET = DB::table('collectlog')
                ->join('forumlog as t1', 't1.id', '=', 'collectlog.start')
                ->leftJoin('forumlog as t2', 't2.id', '=', 'collectlog.end')
                ->select('t1.datetime as d1', 't2.datetime as d2', DB::raw('timestampdiff(second, t1.datetime, t2.datetime) diff'))
                ->where('t1.auth_id', '=', $log->auth_id)
                ->where('t1.type', '=', 8);
            
            if (empty($log->d2)) {
                $BrkT = $BrkT->whereBetween('t1.datetime', [$log->d1, Carbon::createFromFormat('Y-m-d H:i:s', $log->d1)->addHours(10)])->get();
                $BrkW = $BrkW->whereBetween('t1.datetime', [$log->d1, Carbon::createFromFormat('Y-m-d H:i:s', $log->d1)->addHours(10)])->get();
                $BrkET = $BrkET->whereBetween('t1.datetime', [$log->d1, Carbon::createFromFormat('Y-m-d H:i:s', $log->d1)->addHours(10)])->get();
            } else {
                $BrkT = $BrkT->whereBetween('t1.datetime', [$log->d1, $log->d2])->get();
                $BrkW = $BrkW->whereBetween('t1.datetime', [$log->d1, $log->d2])->get();
                $BrkET = $BrkET->whereBetween('t1.datetime', [$log->d1, $log->d2])->get();
            }
            
            $cBT = count($BrkT);
            $cBW = count($BrkW);
            $cET = count($BrkET);
            
            $totalBT = 0;
            $totalBW = 0;
            $totalET = 0;
            
            foreach ($BrkT as $BT) {
                $totalBT += $BT->diff;
            }
            foreach ($BrkW as $BW) {
                $totalBW += $BW->diff;
            }
            foreach ($BrkET as $ET) {
                $totalET += $ET->diff;
            }
            
            $totalBTM = intval($totalBT / 60);
            $totalBWM = intval($totalBW / 60);
            $totalETM = intval($totalET / 60);
            
            if ($log->isOTApproved) {
                $vTotalBT = $totalBT - $log->oBreakT * 60;
            } else {
                $vTotalBT = $totalBT - $log->breakT * 60;
            }
            
            if ($log->isOTApproved) {
                $vTotalBW = $totalBW - $log->oBreakW * 60;
            } else {
                $vTotalBW = $totalBW - $log->breakW * 60;
            }
            
            if ($log->isOTApproved) {
                $vTotalET = $totalET - $log->oEatTime * 60;
            } else {
                $vTotalET = $totalET - $log->eatTime * 60;
            }
        @endphp

        <td>
            @php
                if ($log->isOTApproved) {
                    if ($totalBTM > $log->oBreakT) {
                        $color = 'color:red';
                    } else {
                        $color = 'color:black';
                    }
                } elseif ($totalBTM > $log->breakT) {
                    $color = 'color:red';
                } else {
                    $color = 'color:black';
                }
            @endphp
            @if (empty($totalBT) && empty($BTout))
                -
            @elseif (empty($totalBT))
                <a href="#" style="color:black;" data-toggle="modal"
                    data-target="#show-{{ $totalBT }}{{ $log->start }}-breakT">
                    ...
                </a>
            @else
                <a href="#" style="{{ $color }}; text-decoration:underline; font-style:italic"
                    data-toggle="modal" data-target="#show-{{ $totalBT }}{{ $log->start }}-breakT">
                    @if (($log->isOTApproved && $totalBTM > $log->oBreakT) || ($log->isOTApproved == 0 && $totalBTM > $log->breakT))
                        {{ Carbon::parse($vTotalBT)->format('H:i:00') }}
                    @else
                        00:00:00
                    @endif
                </a>
            @endif

            @if ($cBT >= 1)
                <div class="modal fade" id="show-{{ $totalBT }}{{ $log->start }}-breakT" tabindex="-1"
                    role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">
                                    @if ($log->isOTApproved)
                                        Detail OT Break (T)
                                    @else
                                        Detail Break (T)
                                    @endif
                                </h5>
                            </div>
                            <div class="modal-body">
                                <table class="table">
                                    <tr>
                                        <th colspan="2">Date & Time</th>
                                        @if ($log->isOTApproved)
                                            <th>Total OT Break (T)</th>
                                        @else
                                            <th>Total Break (T)</th>
                                        @endif
                                    </tr>
                                    @foreach ($BrkT as $BT)
                                        <tr>
                                            <td>{{ $BT->d1 }}</td>
                                            @if (empty($BT->d2))
                                                <td>-</td>
                                            @else
                                                <td>{{ $BT->d2 }}</td>
                                            @endif
                                            @if (empty($BT->diff))
                                                <td>{{ Carbon::parse(0)->format('H:i:s') }}</td>
                                            @else
                                                <td>{{ Carbon::parse($BT->diff)->format('H:i:s') }}</td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </td>

        <td>
            @php
                if ($log->isOTApproved) {
                    if ($totalBWM > $log->oBreakW) {
                        $color = 'color:red';
                    } else {
                        $color = 'color:black';
                    }
                } elseif ($totalBWM > $log->breakW) {
                    $color = 'color:red';
                } else {
                    $color = 'color:black';
                }
            @endphp
            @if (empty($totalBW) && empty($BWout))
                -
            @elseif (empty($totalBW))
                <a href="#" style="color:black;" data-toggle="modal"
                    data-target="#show-{{ $totalBW }}{{ $log->start }}-breakW">
                    ...
                </a>
            @else
                <a href="#" style="{{ $color }}; text-decoration:underline; font-style:italic"
                    data-toggle="modal" data-target="#show-{{ $totalBW }}{{ $log->start }}-breakW">
                    @if (($log->isOTApproved && $totalBWM > $log->oBreakW) || ($log->isOTApproved == 0 && $totalBWM > $log->breakW))
                        {{ Carbon::parse($vTotalBW)->format('H:i:00') }}
                    @else
                        00:00:00
                    @endif
                </a>
            @endif

            @if ($cBW >= 1)
                <div class="modal fade" id="show-{{ $totalBW }}{{ $log->start }}-breakW" tabindex="-1"
                    role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">
                                    @if ($log->isOTApproved)
                                        Detail OT Break (W)
                                    @else
                                        Detail Break (W)
                                    @endif
                                </h5>
                            </div>
                            <div class="modal-body">
                                <table class="table">
                                    <tr>
                                        <th colspan="2">Date & Time</th>
                                        @if ($log->isOTApproved)
                                            <th>Total OT Break (W)</th>
                                        @else
                                            <th>Total Break (W)</th>
                                        @endif
                                    </tr>
                                    @foreach ($BrkW as $BW)
                                        <tr>
                                            <td>{{ $BW->d1 }}</td>
                                            @if (empty($BW->d2))
                                                <td>-</td>
                                            @else
                                                <td>{{ $BW->d2 }}</td>
                                            @endif
                                            @if (empty($BW->diff))
                                                <td>{{ Carbon::parse(0)->format('H:i:s') }}</td>
                                            @else
                                                <td>{{ Carbon::parse($BW->diff)->format('H:i:s') }}</td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </td>


        <td>
            @php
                if ($log->isOTApproved) {
                    if ($totalETM > $log->oEatTime) {
                        $color = 'color:red';
                    } else {
                        $color = 'color:black';
                    }
                } elseif ($totalETM > $log->eatTime) {
                    $color = 'color:red';
                } else {
                    $color = 'color:black';
                }
            @endphp
            @if (empty($totalET) && empty($ETout))
                -
            @elseif (empty($totalET))
                <a href="#" style="color:black;" data-toggle="modal"
                    data-target="#show-{{ $totalET }}{{ $log->start }}-eat">
                    ...
                </a>
            @else
                <a href="#" style="{{ $color }}; text-decoration:underline; font-style:italic"
                    data-toggle="modal" data-target="#show-{{ $totalET }}{{ $log->start }}-eat">
                    @if (($log->isOTApproved && $totalETM > $log->oEatTime) || ($log->isOTApproved == 0 && $totalETM > $log->eatTime))
                        {{ Carbon::parse($vTotalET)->format('H:i:00') }}
                    @else
                        00:00:00
                    @endif
                </a>
            @endif

            @if ($cET >= 1)
                <div class="modal fade" id="show-{{ $totalET }}{{ $log->start }}-eat" tabindex="-1"
                    role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">
                                    @if ($log->isOTApproved)
                                        Detail OT Eat Time
                                    @else
                                        Detail Eat Time
                                    @endif
                                </h5>
                            </div>
                            <div class="modal-body">
                                <table class="table">
                                    <tr>
                                        <th colspan="2">Date & Time</th>
                                        @if ($log->isOTApproved)
                                            <th>Total OT Eat Time</th>
                                        @else
                                            <th>Total Eat Time</th>
                                        @endif
                                    </tr>
                                    @foreach ($BrkET as $ET)
                                        <tr>
                                            <td>{{ $ET->d1 }}</td>
                                            @if (empty($ET->d2))
                                                <td>-</td>
                                            @else
                                                <td>{{ $ET->d2 }}</td>
                                            @endif
                                            @if (empty($ET->diff))
                                                <td>{{ Carbon::parse(0)->format('H:i:s') }}</td>
                                            @else
                                                <td>{{ Carbon::parse($ET->diff)->format('H:i:s') }}</td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </td>
        <td>-</td>
    </tr>
@endforeach
