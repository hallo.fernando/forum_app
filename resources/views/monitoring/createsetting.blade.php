@extends('layouts.app')
@section('title')
    Create Log Setting
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @include('layouts.notif')
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 style="float:left">
                            Create Log Setting
                        </h5>
                        <a href="{{ route('inout.setting') }}" class="btn btn-sm btn-primary" style="float: right">Back</a>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('inout.store') }}" method="POST" enctype="multipart/form-data"
                            autocomplete="off">
                            @csrf
                            <h2 style="margin-top:0; text-align:center"><strong>Create</strong></h2>
                            <hr>
                            <div class="row py-2">
                                <div class="col-4">
                                </div>
                                <div class="col-4" style="text-align: center;">
                                    <label style="font-weight: bold" for="">Shift Name</label>
                                    <input type="text" class="form-control" name="name" value=""
                                        placeholder="Shift Name..." style="text-align: center;">
                                    @include('layouts.error', ['name' => 'name'])
                                    {{-- <label style="color: forestgreen;font-size:10px" for="">Minimal 4 karakter, huruf saja</label> --}}
                                </div>
                                <div class="col-4">
                                </div>
                            </div>

                            <div class="row py-2">
                                <div class="col-6">
                                    <label style="font-weight: bold" for="">Check In</label>
                                    @include('layouts.error', ['name' => 'check_in'])
                                    <input type="time" class="form-control" name="check_in" value="">
                                </div>
                                <div class="col-6">
                                    <label style="font-weight: bold" for="">Check Out</label>
                                    @include('layouts.error', ['name' => 'check_out'])
                                    <input type="time" class="form-control" name="check_out" value="">
                                </div>
                            </div>

                            <div class="row py-2">
                                <div class="col-6">
                                    <label style="font-weight: bold" for="">Max Break</label>
                                    @include('layouts.error', ['name' => 'break'])
                                    <input type="number" class="form-control" name="break" placeholder="Enter Max Break"
                                        value="">
                                </div>
                                <div class="col-4">
                                    <label style="font-weight: bold" for="">Break Duration</label>
                                    @include('layouts.error', ['name' => 'break_duration'])
                                    <div class="row">
                                        <div class="col-md-10">
                                            <input type="number" min="1" max="60" class="form-control"
                                                name="break_duration" placeholder="Break Duration" value="">
                                        </div>
                                        <div>
                                            <label style="font-weight: bold; margin-top:8px" for="">Minute</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row p-3">
                                <button class="btn btn-primary col-md-2" type="submit">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
