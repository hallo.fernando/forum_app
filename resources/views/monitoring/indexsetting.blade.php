@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
@endsection
@section('title')
    In-Out Setting
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">

            @include('layouts.notif')

            <div class="col-md-12 mt-2">
                <div class="card" style="margin-bottom:20px">
                    <div class="card-header" style="border-bottom:none;">
                        <h3 style="text-align:center">Setting In-Out</h3>
                    </div>
                </div>

                @php
                    use Carbon\Carbon;
                @endphp

                <form action="{{ route('inout.update') }}" method="POST" enctype="multipart/form-data" autocomplete="off">
                    @csrf
                    <div class="row">
                        @foreach ($forumsetting as $setting)
                            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="card" style="margin-bottom:25px; box-shadow:2px 2px 3px #1ab394;">
                                    <div class="card-header">
                                        <h5 style="float:left; margin-top:8px">
                                            {{ $setting->divName }}
                                        </h5>

                                        <div href="#" style="cursor:pointer; float:right; border-radius:100%; font-weight:900" data-toggle="modal"
                                            data-target="#show-{{ $setting->divisi }}-details" class="btn btn-primary">
                                            ?
                                        </div>

                                        <div class="modal fade" id="show-{{ $setting->divisi }}-details" tabindex="-1"
                                            role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-xl" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">
                                                            [{{ $setting->divName }}] - Forum Setting History
                                                        </h5>
                                                    </div>
                                                    <div class="modal-body">
                                                        <table
                                                            class="table table-responsive-lg table-striped table-bordered text-center">
                                                            <thead>
                                                                <tr>
                                                                    <th>No</th>
                                                                    <th>Break (T)</th>
                                                                    <th>Break (W)</th>
                                                                    <th>Eat Time</th>
                                                                    <th>Work Hours</th>
                                                                    <th>OT - Break (T)</th>
                                                                    <th>OT - Break (W)</th>
                                                                    <th>OT - Eat Time</th>
                                                                    <th>OverTime</th>
                                                                    <th>Created At</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @php
                                                                    $i = 1;
                                                                @endphp
                                                                @foreach ($sDetails as $detail)
                                                                    @if ($detail->divisi == $setting->divisi)
                                                                        <tr>
                                                                            <td>{{ $i++ }}</td>
                                                                            <td>{{ $detail->breakT }} Min</td>
                                                                            <td>{{ $detail->breakW }} Min</td>
                                                                            <td>{{ $detail->eatTime }} Min</td>
                                                                            <td>{{ $detail->workHours / 60 }} Hours</td>
                                                                            <td>{{ $detail->oBreakT ?? 0 }} Min</td>
                                                                            <td>{{ $detail->oBreakW ?? 0 }} Min</td>
                                                                            <td>{{ $detail->oEatTime ?? 0 }} Min</td>
                                                                            <td>{{ $detail->overTime / 60 }} Hours</td>
                                                                            <td>{{ Carbon::parse($detail->updated_at)->isoFormat('DD MMMM Y') }}
                                                                            </td>
                                                                        </tr>
                                                                    @endif
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <input type="text" value="{{ $setting->divName }}" name="name{{ $setting->id }}"
                                            hidden>
                                        <div class="row py-2">
                                            <div class="col-12">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-5 col-5">
                                                        <label style="font-weight: bold; margin-top:7px" for="">Break
                                                            (T)
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4 col-sm-5 col-5">
                                                        <input type="number" min="0" class="form-control"
                                                            value="{{ $setting->breakT }}"
                                                            name="breakT{{ $setting->id }}" required>
                                                    </div>
                                                    <div>
                                                        <label style="font-weight: bold; margin-top:7px" for="">Min</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row py-2">
                                            <div class="col-12">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-5 col-5">
                                                        <label style="font-weight: bold; margin-top:7px" for="">Break
                                                            (W)</label>
                                                    </div>
                                                    <div class="col-md-4 col-sm-5 col-5">
                                                        <input type="number" min="0" class="form-control"
                                                            value="{{ $setting->breakW }}"
                                                            name="breakW{{ $setting->id }}" required>
                                                    </div>
                                                    <div>
                                                        <label style="font-weight: bold; margin-top:7px" for="">Min</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row py-2">
                                            <div class="col-12">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-5 col-5">
                                                        <label style="font-weight: bold; margin-top:7px" for="">Eat
                                                            Time</label>
                                                    </div>
                                                    <div class="col-md-4 col-sm-5 col-5">
                                                        <input type="number" min="0" class="form-control"
                                                            value="{{ $setting->eatTime }}"
                                                            name="eatTime{{ $setting->id }}" required>
                                                    </div>
                                                    <div>
                                                        <label style="font-weight: bold; margin-top:7px" for="">Min</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row py-2">
                                            <div class="col-12">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-5 col-5">
                                                        <label style="font-weight: bold; margin-top:7px" for="">Work
                                                            Hours</label>
                                                    </div>
                                                    <div class="col-md-4 col-sm-5 col-5">
                                                        <input type="number" min="0" class="form-control"
                                                            value="{{ $setting->workHours / 60 }}"
                                                            name="workHours{{ $setting->id }}" required>
                                                    </div>
                                                    <div>
                                                        <label style="font-weight: bold; margin-top:7px" for="">Hour</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row py-2">
                                            <div class="col-12">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-5 col-5">
                                                        <label style="font-weight: bold; margin-top:7px" for="">OT - Break
                                                            (T)
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4 col-sm-5 col-5">
                                                        <input type="number" min="0" class="form-control"
                                                            value="{{ $setting->oBreakT ?? 0 }}"
                                                            name="oBreakT{{ $setting->id }}" required>
                                                    </div>
                                                    <div>
                                                        <label style="font-weight: bold; margin-top:7px" for="">Min</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row py-2">
                                            <div class="col-12">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-5 col-5">
                                                        <label style="font-weight: bold; margin-top:7px" for="">OT - Break
                                                            (W)
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4 col-sm-5 col-5">
                                                        <input type="number" min="0" class="form-control"
                                                            value="{{ $setting->oBreakW ?? 0 }}"
                                                            name="oBreakW{{ $setting->id }}" required>
                                                    </div>
                                                    <div>
                                                        <label style="font-weight: bold; margin-top:7px" for="">Min</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row py-2">
                                            <div class="col-12">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-5 col-5">
                                                        <label style="font-weight: bold; margin-top:7px" for="">
                                                            OT - Eat Time
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4 col-sm-5 col-5">
                                                        <input type="number" min="0" class="form-control"
                                                            value="{{ $setting->oEatTime ?? 0 }}"
                                                            name="oEatTime{{ $setting->id }}" required>
                                                    </div>
                                                    <div>
                                                        <label style="font-weight: bold; margin-top:7px" for="">Min</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row py-2">
                                            <div class="col-12">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-5 col-5">
                                                        <label style="font-weight: bold; margin-top:7px" for="">OverTime</label>
                                                    </div>
                                                    <div class="col-md-4 col-sm-5 col-5">
                                                        <input type="number" min="0" class="form-control"
                                                            value="{{ $setting->overTime / 60 }}"
                                                            name="overTime{{ $setting->id }}" required>
                                                    </div>
                                                    <div>
                                                        <label style="font-weight: bold; margin-top:7px" for="">Hour</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="row pl-3">
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script></script>
@endpush
