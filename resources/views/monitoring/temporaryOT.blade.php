@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
    <style>
        .calendarspan {
            float: right;
            margin-right: 15px;
            margin-top: -25px;
            position: relative;
        }

        label {
            font-weight: bold;
        }

        .notification {
            z-index: 1;
            float: right;
            /* top: 30px; */
            right: 10px;
            background-color: #1c84c6;
            border-color: #1c84c6;
            color: white;
            text-decoration: none;
            padding: 7px 15px;
            position: relative;
            display: inline-block;
            border-radius: 5px;
        }

        td,
        th {
            vertical-align: middle !important;
        }

        .badge {
            position: absolute;
            top: -10px;
            right: -10px;
            padding: 5px 8px;
            color: white;
        }

        @media (max-width:640px) {
            .notification {
                top: 15px;
            }
        }

        @media (max-width:1260px) {
            .table-responsive-xl {
                display: block;
                width: 100%;
                overflow-x: auto;
                -webkit-overflow-scrolling: touch
            }

            .table-responsive-xl>.table-bordered {
                border: 0
            }
        }
    </style>
@endsection
@section('title')
    Data OverTime Request
@endsection
@section('content')
    <div class="">
        <div class="row justify-content-center">

            @include('layouts.notif')

            <div class="col-md-12 mt-2">
                <div class="card">
                    <div class="card-header">
                        Data OverTime Request
                    </div>
                    <div class="card-body">
                        @php
                            use Carbon\Carbon;
                        @endphp

                        <table class="table table-responsive-lg text-center table-bordered table-striped" id="OTtable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th class="col-md-2">Name</th>
                                    <th>Divisi</th>
                                    <th>Posisi</th>
                                    <th class="col-md-1">Check In Time</th>
                                    <th class="col-md-1">Check Out Time</th>
                                    <th>isApproved</th>
                                    <th>isStaffAcc</th>
                                    <th>isLeaderAcc</th>
                                    <th>Action By</th>
                                    <th>Status</th>
                                    <th>Edit Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach ($overtime as $ot)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $ot->n1 }}</td>
                                        <td>{{ $ot->n2 }}</td>
                                        <td>{{ $ot->n3 }}</td>
                                        <td>{{ $ot->d1 }}</td>
                                        <td>
                                            @if (empty($ot->d2))
                                                -
                                            @else
                                                {{ $ot->d2 }}
                                            @endif
                                        </td>
                                        <td>{{ $ot->isOTApproved }}</td>
                                        <td>{{ $ot->isStaffAcc }}</td>
                                        <td>{{ $ot->isLeaderAcc }}</td>
                                        <td>{{ $ot->actionBy }}</td>
                                        <td>
                                            @if (empty($ot->actionBy) || $ot->isStaffAcc === null || $ot->isLeaderAcc === null)
                                                <div style="padding:5px; border:1px solid; border-radius:30px"
                                                    class="btn-warning">Waiting...</div>
                                            @elseif ($ot->isStaffAcc == $ot->isLeaderAcc)
                                                @if ($ot->isStaffAcc == 1)
                                                    <div href="#" class="btn-primary"
                                                        style="padding:5px; border:1px solid; border-radius:30px">
                                                        PAID OT
                                                    </div>
                                                    <span style="font-size:10px">[by {{ $ot->actionBy }}]</span>
                                                @else
                                                    <div href="#" class="btn-danger"
                                                        style="padding:5px; border:1px solid; border-radius:30px">
                                                        UNPAID OT
                                                    </div>
                                                @endif
                                            @else
                                                <div style="padding:5px; border:1px solid; border-radius:30px"
                                                    class="btn-dark">
                                                    NOT MATCH
                                                </div>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($ot->isEdited)
                                                <div href="#"
                                                    style="cursor:pointer; padding:5px; border:1px solid; color:black; border-radius:30px"
                                                    data-toggle="modal" data-target="#show-{{ $ot->id }}-editReason"
                                                    class="btn-warning">
                                                    Edited
                                                </div>
                                                <span style="font-size:10px">
                                                    [by {{ $ot->editedBy }}]
                                                </span>
                                                <div class="modal fade" id="show-{{ $ot->id }}-editReason"
                                                    tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                    aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">
                                                                    Edit Reason</h5>
                                                            </div>
                                                            <div class="modal-body">
                                                                "{{ $ot->reason }}"
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>
                                            <div href="#" style="cursor:pointer;" data-toggle="modal"
                                                data-target="#show-{{ $ot->id }}-edit">
                                                <span class="fa fa-edit btn btn-success"></span>
                                            </div>
                                            <div class="modal fade" id="show-{{ $ot->id }}-edit" tabindex="-1"
                                                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">
                                                                Edit Overtime</h5>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="{{ route('ot.temporaryupdate', $ot->id) }}"
                                                                method="POST">
                                                                @csrf
                                                                @method('PUT')
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <label for="">Leader</label>
                                                                        <input class="form-control" type="number"
                                                                            value="{{ $ot->isLeaderAcc }}"
                                                                            name="LeaderAcc">
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label for="">Staff</label>
                                                                        <input class="form-control" type="number"
                                                                            value="{{ $ot->isStaffAcc }}" name="StaffAcc">
                                                                    </div>
                                                                </div>
                                                                <div class="row p-3">
                                                                    <button class="btn btn-success mt-2"
                                                                        type="submit">Update</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#OTtable').DataTable({
                searching: false,
                pageLength: 100,
            });
            $('#requestTable').DataTable({
                searching: false,
                paging: false,
                ordering: false,
                entries: false,
                info: false,
                language: {
                    emptyTable: "No request at the moment"
                }
            });
        });

        $(document).ready(function() {
            @foreach ($overtime as $ot)
                var current = $('#OTApprove{{ $ot->id }}').val();
                if (current == '0') {
                    $('#OTApprove{{ $ot->id }}').css('background', '#ED5565').css('color', '#fff');
                } else if (current == '1') {
                    $('#OTApprove{{ $ot->id }}').css('background', '#1ab394').css('color', '#fff');
                }
                $('.OTApprove').change(function() {
                    var current = $('#OTApprove{{ $ot->id }}').val();
                    if (current == '0') {
                        $('#OTApprove{{ $ot->id }}').css('background', '#ED5565').css('color',
                            '#fff');
                    } else if (current == '1') {
                        $('#OTApprove{{ $ot->id }}').css('background', '#1ab394').css('color',
                            '#fff');
                    }
                });
            @endforeach
        });

        @if (Session::has('msg') && Session::get('modal_open') == 1)
            $(function() {
                $('#request-ot').modal('show');
            });
        @endif
    </script>
@endpush
