@extends('layouts.app')
@section('title')
    Edit Log Setting
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @include('layouts.notif')
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 style="float:left;">Edit Log Setting</h5>
                        <a href="{{ route('inout.setting') }}" class="btn btn-sm btn-primary" style="float: right">Back</a>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('inout.update', $forumsetting->id) }}" method="POST"
                            enctype="multipart/form-data" autocomplete="off">
                            @csrf
                            @method('PUT')
                            <h2 style="margin-top:0; text-align:center"><strong>Edit</strong></h2>
                            <hr>
                            <div class="row py-2">
                                <div class="col-4">
                                </div>
                                <div class="col-4" style="text-align: center;">
                                    <label style="font-weight: bold" for="">Division Name</label>
                                    <input type="text" class="form-control" name="name"
                                        value="{{ $forumsetting->name }}" style="text-align: center;" readonly>
                                    @include('layouts.error', ['name' => 'name'])
                                    {{-- <label style="color: forestgreen;font-size:10px" for="">Minimal 4 karakter, huruf saja</label> --}}
                                </div>
                                <div class="col-4">
                                </div>
                            </div>

                            <div class="row py-2">
                                <div class="col-3">
                                    <label style="font-weight: bold" for="">Break (T)</label>
                                    @include('layouts.error', ['name' => 'breakT'])
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="number" class="form-control" name="breakT"
                                                placeholder="Break (T)" value="{{ $forumsetting->breakT }}" required>
                                        </div>
                                        <div>
                                            <label style="font-weight: bold; margin-top:8px" for="">Minute</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <label style="font-weight: bold" for="">Break (W)</label>
                                    @include('layouts.error', ['name' => 'breakW'])
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="number" class="form-control" name="breakW"
                                                placeholder="Break (W)" value="{{ $forumsetting->breakW }}" required>
                                        </div>
                                        <div>
                                            <label style="font-weight: bold; margin-top:8px" for="">Minute</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <label style="font-weight: bold" for="">Eat Time</label>
                                    @include('layouts.error', ['name' => 'eatTime'])
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="number" class="form-control" name="eatTime"
                                                placeholder="Eat Time" value="{{ $forumsetting->eatTime }}" required>
                                        </div>
                                        <div>
                                            <label style="font-weight: bold; margin-top:8px" for="">Minute</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <label style="font-weight: bold" for="">Work Hours</label>
                                    @include('layouts.error', ['name' => 'workHours'])
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="number" class="form-control" name="workHours"
                                                placeholder="Work Hours" value="{{ $forumsetting->workHours / 60 }}"
                                                required>
                                        </div>
                                        <div>
                                            <label style="font-weight: bold; margin-top:8px" for="">Hours</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row p-3">
                                <button class="btn btn-primary col-md-2" type="submit">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
