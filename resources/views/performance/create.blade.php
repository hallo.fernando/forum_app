@extends('layouts.app')
@section('title')
    Input Performance
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            @include('layouts.notif')

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 style="float:left">Input Performance</h5>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('perf.store') }}" method="POST" enctype="multipart/form-data"
                            autocomplete="off">
                            @csrf
                            <h2 style="margin-top:0; text-align:center"><strong>Input Kesalahan/Apresiasi</strong></h2>
                            <hr>
                            <div class="row py-2">
                                <input type="text" class="form-control" name="auth" value="{{ Auth::user()->id }}"
                                    hidden>
                                <div class="col-6">
                                    <label style="font-weight: bold" for="">Name</label>
                                    @include('layouts.error', ['name' => 'user'])
                                    <select name="user" class="form-control getuserdetail" id="userdetail" required>
                                        <option value="" disabled selected>Select User</option>
                                        @foreach ($user as $user)
                                            <option value="{{ $user->id }}" data-divisi="{{ $user->GetDivisi->name }}"
                                                data-posisi="{{ $user->GetLevel->name }}">
                                                {{ $user->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6">
                                    <label style="font-weight: bold" for="">Divisi</label>
                                    <input class="form-control" name="divisiName" type="text" readonly>
                                </div>
                            </div>
                            <div class="row py-2">
                                <div class="col-6">
                                    <label style="font-weight: bold" for="">Type</label>
                                    @include('layouts.error', ['name' => 'type'])
                                    <select name="type" class="form-control" required>
                                        <option value="" disabled selected>Select Type</option>
                                        <option value="1">Apresiasi</option>
                                        <option value="2">Kesalahan</option>
                                    </select>
                                </div>
                                <div class="col-6">
                                    <label style="font-weight: bold" for="">Posisi</label>
                                    <input class="form-control" name="posisiName" type="text" readonly>
                                </div>
                            </div>
                            <div class="row py-2">
                                <div class="col-12">
                                    <label style="font-weight: bold" for="">Description</label>
                                    @include('layouts.error', ['name' => 'description'])
                                    <textarea class="form-control" name="description" rows="3" placeholder="Enter a Description" required>{{ old('description') }}</textarea>
                                </div>
                            </div>

                            <div class="row p-3">
                                <button class="btn btn-primary col-md-2" type="submit">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $('.getuserdetail').on('change', function() {
            const divisi = $('#userdetail option:selected').data('divisi');
            const posisi = $('#userdetail option:selected').data('posisi');

            $('[name=divisiName]').val(divisi);
            $('[name=posisiName]').val(posisi);
        });
    </script>
@endpush
