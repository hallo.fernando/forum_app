@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
@endsection
@section('title')
    Performance
@endsection
@section('content')
    <div class="">
        <div class="row justify-content-center">

            @include('layouts.notif')

            <div class="col-md-12 mt-2">
                <div class="card">
                    <div class="card-header">
                        Performance Report
                    </div>
                    <div class="card-body">
                        
                        {{-- filter:start --}}
                        <form class="row g-3" method="GET" autocomplete="off">
                            @php
                                use Carbon\Carbon;
                            @endphp

                            {{-- Date Range --}}
                            <div class="col-md-3">
                                <label>Dari Tanggal</label>
                                <input type="date" id="Sstart" class="form-control" name="start_date"
                                    value="@if(request('start_date')){{ request('start_date') }}@else{{ Carbon::now()->toDateString() }}@endif">
                            </div>
                            <div class="col-md-3">
                                <label>Sampai Tanggal</label>
                                <input type="date" id="Send" class="form-control" name="end_date"
                                    value="@if(request('end_date')){{ request('end_date') }}@else{{ Carbon::now()->toDateString() }}@endif">
                            </div>

                            <div class="col-md-6">
                                <label>Description</label>
                                <input name="Sdesc" value="{{ request('Sdesc') }}" type="text" id="Sdesc"
                                    class="form-control" placeholder="Description...">
                            </div>

                            <div class="col-md-3 pt-2">
                                <input name="Sauth" value="{{ request('Sauth') }}" type="search" id="Sauth"
                                    class="form-control" placeholder="From...">
                            </div>
                            <div class="col-md-3 pt-2">
                                <input name="Suser" value="{{ request('Suser') }}" type="search" id="Suser"
                                    class="form-control" placeholder="To...">
                            </div>

                            {{-- Select Type Query --}}
                            <div class="col-md-4 pt-2">
                                <select name="type" id="Stype" class="form-control">
                                    <option value="" disabled selected>Select Type</option>
                                    <option value="1" {{ request('type') == 1 ? 'selected' : null }}>Apresiasi</option>
                                    <option value="2" {{ request('type') == 2 ? 'selected' : null }}>Kesalahan</option>
                                </select>
                            </div>

                            <div class="row pl-3 pt-2">
                                <div class="col-md">
                                    <button type="submit" class="btn btn-primary mb-3">Search</button>
                                    <button type="submit" class="btn btn-danger mb-3 reset-btn">Reset</button>
                                </div>
                            </div>

                        </form>
                        {{-- filter:end --}}

                        <table id="logtable" class="table-responsive-lg text-center table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th class="">No</th>
                                    <th class="">From</th>
                                    <th class="">To</th>
                                    <th class="col-5">Description</th>
                                    <th class="col-1">Type</th>
                                    <th class="col-2">DateTime</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                @endphp

                                @foreach ($performance as $performance)
                                    <tr>
                                        <td> {{ $i++ }} </td>
                                        <td> 
                                            @if (empty($performance->auth_id))
                                            -
                                            @else
                                            {{ $performance->GetAuth->name }} 
                                            <br/>
                                            <span style="font-size:10px; color:gray">
                                                {{ $performance->GetAuth->GetDivisi->name }} - {{ $performance->GetAuth->GetLevel->name }} 
                                            </span>
                                            @endif
                                        </td>
                                        <td> 
                                            @if (empty($performance->user_id))
                                            -
                                            @else
                                            {{ $performance->GetUser->name }} 
                                            <br/>
                                            <span style="font-size:10px; color:gray">
                                                {{ $performance->GetUser->GetDivisi->name }} - {{ $performance->GetUser->GetLevel->name }} 
                                            </span>
                                            @endif
                                        </td>
                                        <td> {{ $performance->description }} </td>
                                        @if ($performance->type == 1)
                                            <td> <div style="padding:5px; border: 1px solid; border-radius:30px;" class="btn-primary">Apresiasi</div> </td>
                                        @elseif ($performance->type == 2)
                                            <td> <div style="padding:5px; border: 1px solid; border-radius:30px;" class="btn-danger">Kesalahan</div> </td>
                                        @endif
                                        <td>
                                            @php
                                                $perfDate = Carbon::createFromFormat('Y-m-d H:i:s', $performance->datetime);
                                            @endphp
                                            {{ $performance->datetime }} 
                                            <br>
                                            <span style="font-size: 10px; color:gray">{{ $perfDate->diffForHumans() }} </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#logtable').DataTable({
                searching: false,
                pageLength: 100
            });
        });
        $('.reset-btn').on('click', function() {
            $('#Sstart').val(null).trigger("change");
            $('#Send').val(null).trigger("change");
            $('#Sdesc').val(null).trigger("change");
            $('#Sauth').val(null).trigger("change");
            $('#Suser').val(null).trigger("change");
            $('#Stype').val(null).trigger("change");
        });
    </script>
@endpush
