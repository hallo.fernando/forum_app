@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
@endsection
@section('title')
    Divisi
@endsection
@section('content')
    <div class="">
        <div class="row justify-content-center">

            @include('layouts.notif')

            <div class="col-md-12 mt-2">
                <div class="card">
                    <div class="card-header">
                        Data Divisi
                    </div>
                    <div class="card-body">
                        {{-- filter:start --}}
                        <form class="row g-3" method="GET" autocomplete="off">

                            <div class="col-md-4 pt-2">
                                <input name="Sdivisi" value="{{ request('Sdivisi') }}" type="search" id="Sdivisi"
                                    class="form-control" placeholder="Enter a Divisi Name...">
                            </div>
                            

                            <div class="row pl-3 pt-2">
                                <div class="col-md">
                                    <button type="submit" class="btn btn-primary mb-3">Search</button>
                                    <button type="submit" class="btn btn-danger mb-3 reset-btn">Reset</button>
                                </div>
                            </div>

                        </form>
                        {{-- filter:end --}}
                        <table class="table-responsive-lg text-center table-bordered table-striped" id="divisitable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Divisi Name</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th>Created By</th>
                                    <th>Updated By</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach ($divisi as $divisi)
                                    <tr>
                                        <td> {{ $i++ }} </td>
                                        <td> {{ $divisi->name }} </td>
                                        <td> {{ $divisi->created_at }} </td>
                                        <td> {{ $divisi->updated_at }} </td>
                                        <td> 
                                            @if (empty($divisi->userCreate))
                                            -
                                            @else
                                            {{ $divisi->userCreate->name }} 
                                            @endif
                                        </td>
                                        <td>
                                            @if (empty($divisi->userUpdate))
                                            -
                                            @else
                                            {{ $divisi->userUpdate->name }} 
                                            @endif
                                        </td>
                                        <td> <a href="{{ route('divisi.edit', $divisi->id) }}"
                                                class="btn btn-primary btn-sm btn-block">Edit</a>
                                            {{-- <a href="/divisi/{{ $divisi->id }}/destroy"
                                                class="btn btn-danger btn-sm btn-block">Delete</a> --}}
                                        </td>

                                        {{-- <td> {{ return '<a href="/divisi/' . $divisi->id . '/edit"
                                        class="btn btn-primary btn-sm btn-block">Edit</a>
                                        <a href="/divisi/' . $divisi->id . '/destroy"
                                        class="btn btn-danger btn-sm btn-block">Delete</a>';}}</td> --}}
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#divisitable').DataTable({
                searching: false,
                pageLength: 100
            });
        });

        $('.reset-btn').on('click', function() {
            $('#Sdivisi').val(null).trigger("change");
        });
    </script>
@endpush
