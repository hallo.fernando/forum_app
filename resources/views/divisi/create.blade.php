@extends('layouts.app')
@section('title')
    Create Divisi
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            @include('layouts.notif')

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 style="float:left;">Create Divisi</h5>
                        {{-- <a href="{{ route('divisi.index') }}" class="btn btn-sm btn-primary" style="float: right">Back</a> --}}
                    </div>
                    <div class="card-body">
                        <form action="{{ route('divisi.store') }}" method="POST" enctype="multipart/form-data" autocomplete="off">
                            @csrf
                            <h2 style="margin-top:0; text-align:center"><strong>Pendaftaran Divisi</strong></h2>
                            <hr>
                            <div class="row py-2">
                                <div class="col-6">
                                    <label style="font-weight: bold" for="">Divisi Name</label>
                                    @include('layouts.error', ['name' => 'name'])
                                    <input type="text" class="form-control" name="name" placeholder="Enter a divisi"
                                        value="{{ old('name') }}">
                                    <label style="color: forestgreen; font-size:10px" for="">*Minimal 4 karakter, huruf
                                        saja</label>
                                </div>
                            </div>
                            <label style="font-weight: bold" for="">Posisi Divisi (Tidak Harus Di Isi)</label>
                            <div class="table-responsive">
                                <table class="table table-bordered col-6" id="dynamic_field">
                                    <tr>
                                        <td><input type="text" name="posisi[]" placeholder="" class="form-control" /></td>
                                        <td><button type="button" name="add" id="add" class="btn btn-primary btn-block">Add
                                                More</button></td>
                                    </tr>
                                </table>
                            </div>

                            <div class="row p-3">
                                <button class="btn btn-primary col-md-2" type="submit">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var postURL = "<?php echo url('addmore'); ?>";
            var i = 1;

            $('#add').click(function() {
                i++;
                $('#dynamic_field').append('<tr id="row' + i +
                    '" class="dynamic-added"><td><input type="text" name="posisi[]" placeholder="" class="form-control" /></td><td><button type="button" name="remove" id="' +
                    i + '" class="btn btn-danger btn_remove btn-block">Remove</button></td></tr>');
            });

            $(document).on('click', '.btn_remove', function() {
                var button_id = $(this).attr("id");
                $('#row' + button_id + '').remove();
            });

        });
    </script>
@endsection
