@extends('layouts.app')
@section('title')
    Edit Divisi
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @include('layouts.notif')
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 style="float:left;">Edit Data Divisi</h5>
                        <a href="{{ route('divisi.index') }}" class="btn btn-sm btn-primary" style="float: right">Back</a>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('divisi.update', $divisi->id) }}" method="POST"
                            enctype="multipart/form-data" autocomplete="off">
                            @csrf
                            @method('PUT')
                            <h2 style="margin-top:0; text-align:center"><strong>Edit</strong></h2>
                            <hr>
                            <div class="row py-2">
                                <div class="col-6">
                                    <label style="font-weight: bold" for="">Divisi Name</label>
                                    @include('layouts.error', ['name' => 'name'])
                                    <input type="text" class="form-control" name="name" placeholder="Enter a name"
                                        value="{{ $divisi->name }}">
                                    <label style="color: forestgreen;font-size:10px" for="">Minimal 4 karakter
                                        (huruf)</label>

                                </div>
                                <div class="col-6">
                                    <div style="margin-top:3px" class="row pt-4">
                                        <button class="btn btn-primary col-md-2" type="submit">Update</button>
                                    </div>
                                </div>
                            </div>
                            {{-- <label style="font-weight: bold" for="">Posisi Divisi (Tidak Harus Di Isi)</label>
                            <div class="table-responsive">
                                <table class="table table-bordered col-6" id="dynamic_field">
                                    @foreach ($level as $level)
                                    <tr>
                                        <td><input type="text" name="posisi[]" placeholder="" class="form-control" value="{{$level->name}}" /></td>
                                        <td><button type="button" name="add" id="add" class="btn btn-primary btn-block">Add
                                                More</button></td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div> --}}

                            
                        </form>
                        <button type="button" class="btn btn-danger" data-toggle="modal"
                            data-target="#delete-{{ $divisi->id }}">Delete Divisi
                        </button>

                        <div class="modal fade" id="delete-{{ $divisi->id }}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">
                                            Delete Divisi</h5>
                                    </div>
                                    <div class="modal-body">
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            You are about to <strong>DELETE</strong> Divisi
                                            [{{ $divisi->name }}].
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="{{ route('divisi.destroy', $divisi->id) }}"
                                            class="btn btn-danger">Delete</a>
                                        {{-- <a href="#" class="btn btn-danger">Delete</a> --}}
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var postURL = "<?php echo url('addmore'); ?>";
            var i = 1;


            $('#add').click(function() {
                i++;
                $('#dynamic_field').append('<tr id="row' + i +
                    '" class="dynamic-added"><td><input type="text" name="posisi[]" placeholder="" class="form-control" /></td><td><button type="button" name="remove" id="' +
                    i + '" class="btn btn-danger btn_remove btn-block">Remove</button></td></tr>');
            });


            $(document).on('click', '.btn_remove', function() {
                var button_id = $(this).attr("id");
                $('#row' + button_id + '').remove();
            });


        });
    </script> --}}
@endsection
