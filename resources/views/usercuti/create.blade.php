@extends('layouts.app')
@section('css')
    <style>
        .calendarspan {
            float: right;
            margin-right: 15px;
            margin-top: -25px;
            position: relative;
        }
    </style>
@endsection
@section('title')
    Request Cuti
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @include('layouts.notif')

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 style="float:left">
                            Request Cuti
                        </h5>
                        {{-- <a href="{{ route('user.index') }}" class="btn btn-sm btn-primary" style="float: right">Back
                        </a> --}}
                    </div>
                    <div class="card-body">
                        <form action="{{ route('cuti.store') }}" method="POST" enctype="multipart/form-data"
                            autocomplete="off">
                            @csrf
                            <h2 style="margin-top:0;text-align:center"><strong>Request Cuti</strong></h2>
                            <hr>
                            <div class="row py-2">
                                <input type="text" value="{{ Auth::user()->id }}" name="user_id" hidden>
                                <div class="col-6">
                                    <label style="font-weight: bold" for="">Name</label>
                                    <input type="text" class="form-control" name="auth_name"
                                        value="{{ Auth::user()->name }}" required disabled>
                                </div>
                                <div class="col-6">
                                    <label style="font-weight: bold">Date Range</label>
                                    <input type="text" class="form-control" placeholder="mm/dd/yyyy - mm/dd/yyyy"
                                        name="daterange" value="{{ old('daterange') }}" required />
                                    <span class="fa fa-calendar-o calendarspan"></span>
                                </div>
                            </div>

                            <div class="row py-2">
                                <div class="col-6">
                                    <label style="font-weight: bold" for="">Description</label>
                                    <textarea class="form-control" name="description" required></textarea>
                                </div>
                                <div class="col-3">
                                    <label style="font-weight: bold" for="">Sisa Cuti</label>
                                    <input type="text" class="form-control" name="sisa_cuti"
                                        value="{{ Auth::user()->sisa_cuti }} Hari" required disabled>
                                </div>
                                <div class="col-3">
                                    <label style="font-weight: bold">Leave Days</label>
                                    <input type="text" id="manydays" class="form-control" value="-" disabled>
                                </div>
                            </div>

                            <div class="row pl-3">
                                <button class="btn btn-primary mt-2 col-md-2" type="submit">Request</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <script>
        $(function() {
            $('input[name="daterange"]').daterangepicker({
                autoUpdateInput: false,
            }, function(start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end
                    .format('YYYY-MM-DD'));
            });

            $('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format(
                    'MM/DD/YYYY'));
                myfunc();

                function myfunc() {
                    var startD = new Date(picker.startDate.format('YYYY-MM-DD'));
                    var endD = new Date(picker.endDate.format('YYYY-MM-DD'));

                    var diff = new Date(endD - startD);

                    var days = diff / (1000 * 60 * 60 * 24) + 1;
                    document.getElementById("manydays").value = days + " Hari";
                }
            });

            $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });
        });
    </script>
@endpush
