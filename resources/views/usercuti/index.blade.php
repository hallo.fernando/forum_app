@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
    <style>
        .calendarspan {
            float: right;
            margin-right: 15px;
            margin-top: -25px;
            position: relative;
        }

        label {
            font-weight: bold;
        }

        .notification {
            z-index: 1;
            float: right;
            top: 55px;
            right: 10px;
            background-color: #1c84c6;
            border-color: #1c84c6;
            color: white;
            text-decoration: none;
            padding: 7px 15px;
            position: relative;
            display: inline-block;
            border-radius: 5px;
        }

        .badge {
            position: absolute;
            top: -10px;
            right: -10px;
            padding: 5px 8px;
            color: white;
        }

        @media (max-width:640px) {
            .notification {
                top: 15px;
            }
        }

        @media (max-width:1260px) {
            .table-responsive-xl {
                display: block;
                width: 100%;
                overflow-x: auto;
                -webkit-overflow-scrolling: touch
            }

            .table-responsive-xl>.table-bordered {
                border: 0
            }
        }
    </style>
@endsection
@section('title')
    Data Cuti
@endsection
@section('content')
    <div class="">
        <div class="row justify-content-center">

            @include('layouts.notif')

            <div class="col-md-12 mt-2">
                <div class="card">
                    <div class="card-header">
                        Data Cuti
                    </div>
                    <div class="card-body">
                        @php
                            use Carbon\Carbon;
                        @endphp

                        {{-- filter:start --}}
                        <form class="row g-3" method="GET" autocomplete="off">

                            {{-- Date Range --}}
                            <div class="col-md-3">
                                <label>Dari Tanggal</label>
                                <input type="date" id="Sstart" class="form-control" name="start_date"
                                    value="@if(request('start_date')){{ request('start_date') }}@else{{ Carbon::now()->toDateString() }}@endif">
                            </div>
                            <div class="col-md-3">
                                <label>Sampai Tanggal</label>
                                <input type="date" id="Send" class="form-control" name="end_date"
                                    value="@if(request('end_date')){{ request('end_date') }}@else{{ Carbon::now()->toDateString() }}@endif">
                            </div>

                            <div class="col-md-3">
                                <label>Name</label>
                                <input name="Sname" value="{{ request('Sname') }}" type="search" id="Sname"
                                    class="form-control" placeholder="Full Name">
                            </div>

                            {{-- Select Divisi Query --}}
                            <div class="col-xl-3 col-md-3">
                                <label>Divisi</label>
                                <select name="Sdivisi" id="Sdivisi" class="form-control">
                                    <option value="" disabled selected>Select Divisi</option>
                                    @forelse ($divisi as $divisi)
                                        @php
                                            if ($divisi->name == request('Sdivisi')) {
                                                $select = 'selected';
                                            } else {
                                                $select = '';
                                            }
                                            echo "<option $select value='$divisi->name'>" . $divisi['name'] . '</option>';
                                        @endphp
                                    @empty
                                        <option value="" selected>No Data</option>
                                    @endforelse
                                </select>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                <button style="margin-top:1rem" type="submit" class="btn btn-primary mb-3">Search</button>
                                <button style="margin-top:1rem" type="submit"
                                    class="btn btn-danger mb-3 reset-btn">Reset</button>
                                @if (Auth::user()->GetLevel->edit_usercuti)
                                    <button type="button" class="notification" data-toggle="modal"
                                        data-target="#request-cuti">
                                        @php
                                            $count = 0;
                                            foreach ($usercuti as $cuti) {
                                                if (!$cuti->isApproved && empty($cuti->approvedBy)) {
                                                    $count++;
                                                }
                                            }
                                        @endphp
                                        <i class="fa fa-info"></i>
                                        @if ($count == 0)
                                            <span class="badge"
                                                style="background-color: gray">{{ $count }}</span>
                                        @else
                                            <span class="badge"
                                                style="background-color: red">{{ $count }}</span>
                                        @endif
                                    </button>
                                @endif
                            </div>
                        </form>
                        {{-- filter:end --}}


                        {{-- Request Cuti Modal --}}
                        <div class="modal fade" id="request-cuti" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-xl" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">
                                            Request Cuti</h5>
                                    </div>
                                    <div class="modal-body">
                                        <table class="table table-responsive-lg table-striped table-bordered text-center"
                                            id="requestTable">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Name</th>
                                                    <th>From</th>
                                                    <th>Till</th>
                                                    <th>Description</th>
                                                    <th>Leave Days</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $noReq = 1;
                                                @endphp
                                                @foreach ($usercuti as $cuti)
                                                    @if (!$cuti->isApproved && empty($cuti->approvedBy))
                                                        <tr>
                                                            <td>{{ $noReq++ }}</td>
                                                            <td>{{ $cuti->GetUser->name }}</td>
                                                            <td>{{ Carbon::parse($cuti->from)->format('d-M-Y') }}</td>
                                                            <td>{{ Carbon::parse($cuti->till)->format('d-M-Y') }}</td>
                                                            <td>{{ $cuti->description }}</td>
                                                            <td>{{ $cuti->leaveDay }} Hari</td>
                                                            <td>
                                                                <a class="btn btn-primary"
                                                                    href="{{ route('cuti.approve', $cuti->id) }}">
                                                                    Approve
                                                                </a>
                                                                <a href="#" class="btn btn-danger" data-toggle="modal"
                                                                    data-target="#show-{{ $cuti->id }}-reject">
                                                                    Reject
                                                                </a>
                                                                <div class="modal fade"
                                                                    id="show-{{ $cuti->id }}-reject" tabindex="-1"
                                                                    role="dialog" aria-labelledby="exampleModalLabel"
                                                                    aria-hidden="true">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h5 class="modal-title"
                                                                                    id="exampleModalLabel">
                                                                                    Reject Reason</h5>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <label for="">Describe a reason to reject
                                                                                    this request</label>
                                                                                <form
                                                                                    action="{{ route('cuti.reject', $cuti->id) }}"
                                                                                    method="POST">
                                                                                    @csrf
                                                                                    @method('PUT')
                                                                                    <textarea class="form-control" name="reason" rows="5" required></textarea>
                                                                                    <div class="row p-3">
                                                                                        <button class="btn btn-danger mt-2"
                                                                                            type="submit">Reject</button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button"
                                                                                    class="btn btn-secondary"
                                                                                    data-dismiss="modal">Close</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- End of Request Cuti Modal --}}

                        <table class="table-responsive-xl text-center table-bordered table-striped" id="cutitable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Divisi</th>
                                    <th>Posisi</th>
                                    <th>Office</th>
                                    <th>Sisa Cuti</th>
                                    {{-- <th>Description</th> --}}
                                    <th class="col-md-1">Date</th>
                                    <th class="col-md-1">Return</th>
                                    <th>Days</th>
                                    <th>Status</th>
                                    <th>Edit Status</th>
                                    @if (Auth::user()->GetLevel->edit_usercuti)
                                        <th>Action</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach ($usercuti as $cuti)
                                    @if (Auth::user()->name == $cuti->GetUser->name || Auth::user()->GetLevel->edit_usercuti)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $cuti->GetUser->name }}</td>
                                            <td>{{ $cuti->GetUser->GetDivisi->name }}</td>
                                            <td>{{ $cuti->GetUser->GetLevel->name }}</td>
                                            <td>{{ $cuti->GetUser->office }}</td>
                                            <td>
                                                {{ $cuti->GetUser->sisa_cuti }} Hari
                                            </td>
                                            {{-- <td>{{ $cuti->description }}</td> --}}
                                            <td>{{ Carbon::parse($cuti->from)->isoFormat('DD-MMMM-Y') }}</td>
                                            <td>{{ Carbon::parse($cuti->till)->isoFormat('DD-MMMM-Y') }}</td>
                                            <td>{{ $cuti->leaveDay }} Hari</td>
                                            <td>
                                                @if (empty($cuti->approvedBy))
                                                    <div style="padding:5px; border:1px solid; border-radius:30px"
                                                        class="btn-warning">Waiting...</div>
                                                @elseif ($cuti->isApproved)
                                                    <div style="padding:5px; border:1px solid; border-radius:30px"
                                                        class="btn-primary">Approved</div>
                                                    <span style="font-size:10px">[by {{ $cuti->approvedBy }}]</span>
                                                @else
                                                    <div tabindex="0" type="button" class="btn-danger"
                                                        style="padding:5px; border:1px solid; border-radius:30px"
                                                        data-toggle="popover" data-trigger="focus" title="Reject Reason"
                                                        data-content="{{ $cuti->reason }}">
                                                        Rejected
                                                    </div>
                                                    <span style="font-size:10px">[by {{ $cuti->approvedBy }}]</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($cuti->isEdited)
                                                    <div tabindex="0" type="button" class="btn-warning"
                                                        style="color:black; padding:5px; border:1px solid; border-radius:30px"
                                                        data-toggle="popover" data-trigger="focus" title="Edit Reason"
                                                        data-content="{{ $cuti->editReason }}">
                                                        Edited
                                                    </div>
                                                    <span style="font-size:10px">[by {{ $cuti->editedBy }}]</span>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            @if (Auth::user()->GetLevel->edit_usercuti)
                                                <td>
                                                    @if ($cuti->isApproved)
                                                        <a href="{{ route('cuti.edit', $cuti->id) }}">
                                                            <span class="fa fa-edit btn btn-success"></span>
                                                        </a>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                            @endif
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.12/sorting/datetime-moment.js"></script>

    <script>
        $(document).ready(function() {
            $.fn.dataTable.moment('DD-MMMM-Y');

            $('#cutitable').DataTable({
                searching: false,
                pageLength: 100,
            });
            $('#requestTable').DataTable({
                searching: false,
                paging: false,
                ordering: false,
                entries: false,
                info: false,
                language: {
                    emptyTable: "No request at the moment"
                }
            });
        });
        $('.reset-btn').on('click', function() {
            $('#Sstart').val(null).trigger("change");
            $('#Send').val(null).trigger("change");
            $('#Sname').val(null).trigger("change");
            $('#Sdivisi').val(null).trigger("change");
        });
    </script>
@endpush
