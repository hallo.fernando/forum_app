@extends('layouts.app')
@section('css')
    <style>
        .calendarspan {
            float: right;
            margin-right: 15px;
            margin-top: -25px;
            position: relative;
        }
    </style>
@endsection
@section('title')
    Edit User Cuti
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @include('layouts.notif')

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 style="float:left">
                            Request Cuti
                        </h5>
                        <a href="{{ route('cuti.index') }}" class="btn btn-sm btn-primary" style="float: right">Back</a>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('cuti.update', $cuti->id) }}" method="POST" enctype="multipart/form-data"
                            autocomplete="off">
                            @csrf
                            @method('PUT')
                            <h2 style="margin-top:0;text-align:center"><strong>Edit Request Cuti</strong></h2>
                            <hr>
                            <div class="row py-2">
                                <div class="col-6">
                                    <label style="font-weight: bold" for="">Username</label>
                                    <input type="text" class="form-control" name="auth_name"
                                        value="{{ $cuti->GetUser->name }}" required disabled>
                                </div>
                                <div class="col-6">
                                    <label style="font-weight: bold">Date Range</label>
                                    <input type="text" class="form-control" placeholder="mm/dd/yyyy - mm/dd/yyyy"
                                        name="daterange" value="{{ $daterange }}" required />
                                    <span class="fa fa-calendar-o calendarspan"></span>
                                </div>
                            </div>

                            <div class="row py-2">
                                <div class="col-6">
                                    <label style="font-weight: bold" for="">Description</label>
                                    <textarea class="form-control" name="description" required disabled>{{ $cuti->description }}</textarea>
                                </div>
                                <div class="col-3">
                                    <label style="font-weight: bold" for="">Sisa Cuti</label>
                                    <input type="text" class="form-control" id="sisacuti" name="sisa_cuti"
                                        value="{{ $cuti->GetUser->sisa_cuti }} Hari" required disabled>
                                </div>
                                <div class="col-3">
                                    <label style="font-weight: bold">Leave Days</label>
                                    <input type="text" name="manyday" id="manydays" class="form-control"
                                        value="{{ $cuti->leaveDay }} Hari" disabled>
                                </div>
                            </div>

                            <hr>

                            <div class="row py-2">
                                <div class="col-6">
                                    <label style="font-weight: bold" for="">Reason for Editing Request</label>
                                    <textarea class="form-control" name="editReason" required>{{ $cuti->editReason }}</textarea>
                                </div>
                            </div>

                            <div class="row pl-3">
                                <button class="btn btn-primary mt-2 col-md-2" type="submit">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- This is value for calculate sisacuti function on jquery/javascript --}}
    <div hidden>
        <select class="form-select" id="totalcuti">
            <option selected value="{{ $cuti->GetUser->cuti }}" data-totalcuti="{{ $cuti->GetUser->cuti }}">
            </option>
        </select>
        <select class="form-select" id="uTotal">
            <option selected value="{{ $uTotal }}" data-utotal="{{ $uTotal }}">
            </option>
        </select>
        <select class="form-select" id="leaveday">
            <option selected value="{{ $cuti->leaveDay }}" data-leaveday="{{ $cuti->leaveDay }}">
            </option>
        </select>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script>
        $(function() {
            $('input[name="daterange"]').daterangepicker({
                autoUpdateInput: false,
            }, function(start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end
                    .format('YYYY-MM-DD'));
            });

            $('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format(
                    'MM/DD/YYYY'));
                myfunc();

                function myfunc() {
                    var startD = new Date(picker.startDate.format('YYYY-MM-DD'));
                    var endD = new Date(picker.endDate.format('YYYY-MM-DD'));

                    var diff = new Date(endD - startD);

                    var days = diff / (1000 * 60 * 60 * 24) + 1;
                    document.getElementById("manydays").value = days + " Hari";

                    const totalcuti = $('#totalcuti option:selected').data('totalcuti');
                    const utotal = $('#uTotal option:selected').data('utotal');
                    const leaveday = $('#leaveday option:selected').data('leaveday');

                    var sisacuti = totalcuti - (utotal - leaveday + days);
                    document.getElementById("sisacuti").value = sisacuti + " Hari";
                }
            });


            $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });
        });
    </script>
@endpush
