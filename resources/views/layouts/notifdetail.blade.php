@php
use Carbon\Carbon;
@endphp

@if (Auth::user()->getLevel->edit_overtime)
    @php
        $count = 0;
        foreach ($overtimefull as $ot) {
            // if (!$ot->isOTApproved && empty($ot->actionBy)) {
            if (empty($ot->actionBy)) {
                $count++;
            }
        }
    @endphp
    <i class="fa fa-info"></i>
    @if ($count == 0)
        <span class="badge" id="notification" style="background-color: gray">{{ $count }}</span>
    @else
        <span class="badge" id="notification" style="background-color: red">{{ $count }}</span>
    @endif
@endif

<div class="notifdetail">
    @forelse ($overtimedetail as $ot)
        {{-- @if (!$ot->isOTApproved && empty($ot->actionBy)) --}}
        @if (empty($ot->actionBy))
            <li class="notification-box" style="border-bottom:1px solid rgba(128, 128, 128, 0.2)">
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-12 text-center">
                        <strong style="color:#1ab394">{{ $ot->n1 }}</strong> - <small
                            class="text-warning">{{ $ot->n2 }}</small>
                        <div>
                            Melewati batas waktu kerja (OT Request)
                        </div>
                        @php
                            $chkin = Carbon::createFromFormat('Y-m-d H:i:s', $ot->d1);
                            if (empty($ot->d2)) {
                                $chkout = Carbon::now();
                            } else {
                                $chkout = Carbon::createFromFormat('Y-m-d H:i:s', $ot->d2);
                            }
                            $total_in_out = $chkin->diffInSeconds($chkout);
                            if ($total_in_out > 86400) {
                                $total_in_out = 0;
                            }
                        @endphp
                        <small>
                            @if ($total_in_out == 0)
                                -- Jam -- Menit
                            @else
                                {{ Carbon::parse($total_in_out)->format('H') }} Jam
                                {{ Carbon::parse($total_in_out)->format('i') }} Menit
                            @endif
                        </small>
                    </div>
                </div>
            </li>
        @endif
    @empty
        <li class="notification-box">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-12 text-center">
                    <div>
                        No Data
                    </div>
                </div>
            </div>
        </li>
    @endforelse
</div>
