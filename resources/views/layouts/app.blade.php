<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{-- <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests"> --}}

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- <title>{{ config('app.name', 'Laravel') }}</title> --}}
    <title>@yield('title') - Forum HKB</title>

    <!-- Scripts -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <style>
        span span#notification {
            position: absolute;
            top: 33px;
            right: 22px;
            padding: 2px 4px;
            color: white;
        }

        .menu-style {
            top: 60px;
            right: 0px;
            left: unset;
            width: 360px;
            box-shadow: 0px 5px 7px -1px #c1c1c1;
            padding-bottom: 0px;
            padding: 0px;
        }

        .dropdown-menu:before {
            content: "";
            position: absolute;
            top: -20px;
            right: 30px;
            border: 10px solid #2F4050;
            border-color: transparent transparent #2F4050 transparent;
        }

        .head {
            padding: 5px 15px;
            border-radius: 3px 3px 0px 0px;
        }

        .footer {
            margin-top: 20px;
            padding: 5px 15px;
            border-radius: 0px 0px 3px 3px;
        }

        .notification-box {
            padding: 10px 0px;
        }

        .bg-gray {
            background-color: #eee;
        }
    </style>
    @yield('css')
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}

</head>

<body>
    <div id="wrapper">
        @auth
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav metismenu" id="side-menu">
                        <li class="nav-header">
                            <div class="dropdown profile-element">
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                    <span class="block m-t-xs font-bold">{{ Auth::user()->name }}
                                        @if (empty(Auth::user()->status_level))
                                            <span style="font-size:10px;color:grey">(Guest)</span>
                                        @endif
                                    </span>
                                    <span class="text-muted text-xs block">Menu <b class="caret"></b></span>
                                </a>
                                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                    <li>
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                            Check Out
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                            class="d-none">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </div>
                            <div class="logo-element" style="font-size:15px">
                                Forum
                                {{-- <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    Check Out
                                </a> --}}
                            </div>
                        </li>
                        <li class="{{ Route::currentRouteNamed('home') ? 'active' : '' }}">
                            <a href="{{ url('/home') }}">
                                <div class="row">
                                    <div class="col-md-2">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <div class="col-md-10">
                                        <span class="nav-label">Profile</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="{{ Route::currentRouteNamed('') ? 'active' : '' }}">
                            <a href="#">
                                <div class="row">
                                    <div class="col-md-2">
                                        <i class="fa fa-wechat"></i>
                                    </div>
                                    <div class="col-md-10">
                                        <span class="nav-label">Meeting</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        {{-- @if (Auth::user()->already_change_password && Auth::user()->status_level) --}}
                        @if (Auth::user()->already_change_password)
                            @if (Auth::user()->getLevel->permission_view_user)
                                <li style="text-align: center">
                                    <a class="list-group-item list-group-item-action disabled active">
                                        <i class="fa fa-user"></i><span class="nav-label">User</span>
                                    </a>
                                </li>
                            @endif
                            @if (Auth::user()->getLevel->permission_edit_user)
                                <li class="{{ Route::currentRouteNamed('user.create') ? 'active' : '' }}">
                                    <a href="{{ route('user.create') }}">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <i class="fa fa-user-plus"></i>
                                            </div>
                                            <div class="col-md-10">
                                                <span class="nav-label">Create User</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            @endif
                            @if (Auth::user()->getLevel->permission_view_user)
                                <li class="{{ Route::currentRouteNamed('user.index', 'user.edit') ? 'active' : '' }}">
                                    <a href="{{ route('user.index') }}">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <i class="fa fa-th-list"></i>
                                            </div>
                                            <div class="col-md-10">
                                                <span class="nav-label">Data User</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            @endif
                            @if (Auth::user()->getLevel->view_overtime)
                                <li class="{{ Route::currentRouteNamed('ot.index') ? 'active' : '' }}">
                                    <a href="{{ route('ot.index') }}">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <i class="fa fa-th-list"></i>
                                            </div>
                                            <div class="col-md-10">
                                                <span class="nav-label">Data OT Request</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            @endif
                            @if (Auth::user()->getLevel->view_usercuti)
                                <li class="{{ Route::currentRouteNamed('cuti.create') ? 'active' : '' }}">
                                    <a href="{{ route('cuti.create') }}">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <i class="fa fa-plus-square"></i>
                                            </div>
                                            <div class="col-md-10">
                                                <span class="nav-label">Request Cuti</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            @endif
                            @if (Auth::user()->getLevel->view_usercuti)
                                <li class="{{ Route::currentRouteNamed('cuti.index') ? 'active' : '' }}">
                                    <a href="{{ route('cuti.index') }}">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <i class="fa fa-th-list"></i>
                                            </div>
                                            <div class="col-md-10">
                                                <span class="nav-label">Data Cuti</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            @endif

                            @if (Auth::user()->getLevel->permission_view_divisi)
                                <li style="text-align: center">
                                    <a class="list-group-item list-group-item-action disabled active">
                                        <i class="fa fa-users"></i><span class="nav-label">Divisi</span>
                                    </a>
                                </li>
                            @endif
                            @if (Auth::user()->getLevel->permission_edit_divisi)
                                <li class="{{ Route::currentRouteNamed('divisi.create') ? 'active' : '' }}">
                                    <a href="{{ route('divisi.create') }}">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <i class="fa fa-plus"></i>
                                            </div>
                                            <div class="col-md-10">
                                                <span class="nav-label">Create Divisi</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            @endif
                            @if (Auth::user()->getLevel->permission_view_divisi)
                                <li
                                    class="{{ Route::currentRouteNamed('divisi.index', 'divisi.edit') ? 'active' : '' }}">
                                    <a href="{{ route('divisi.index') }}">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <i class="fa fa-th-list"></i>
                                            </div>
                                            <div class="col-md-10">
                                                <span class="nav-label">Data Divisi</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            @endif

                            @if (Auth::user()->getLevel->permission_view_log || Auth::user()->getLevel->permission_view_user_tingkatan || Auth::user()->getLevel->view_forumsetting || Auth::user()->getLevel->view_whitelist)
                                <li style="text-align: center">
                                    <a class="list-group-item list-group-item-action disabled active">
                                        <i class="fa fa-gears"></i><span class="nav-label">Pengaturan</span>
                                    </a>
                                </li>
                            @endif
                            @if (Auth::user()->getLevel->permission_view_user_tingkatan)
                                <li class="{{ Route::currentRouteNamed('level.index', 'level.edit') ? 'active' : '' }}">
                                    <a href="{{ route('level.index') }}">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <i class="fa fa-sitemap"></i>
                                            </div>
                                            <div class="col-md-10">
                                                <span class="nav-label">Level Control</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            @endif
                            @if (Auth::user()->getLevel->permission_view_log)
                                <li class="{{ Route::currentRouteNamed('log.index') ? 'active' : '' }}">
                                    <a href="{{ route('log.index') }}">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <i class="fa fa-th-large"></i>
                                            </div>
                                            <div class="col-md-10">
                                                <span class="nav-label">Log</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            @endif
                            @if (Auth::user()->getLevel->view_forumsetting)
                                <li class="{{ Route::currentRouteNamed('inout.setting') ? 'active' : '' }}">
                                    <a href="{{ route('inout.setting') }}">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <i class="fa fa-cog"></i>
                                            </div>
                                            <div class="col-md-10">
                                                <span class="nav-label">Setting In-Out</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            @endif
                            @if (Auth::user()->getLevel->view_whitelist)
                                <li class="{{ Route::currentRouteNamed('wl.index') ? 'active' : '' }}">
                                    <a href="{{ route('wl.index') }}">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <i class="fa fa-sitemap"></i>
                                            </div>
                                            <div class="col-md-10">
                                                <span class="nav-label">Whitelist IP Control</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            @endif

                            @if (Auth::user()->getLevel->view_inoutlog || Auth::user()->getLevel->permission_view_performance)
                                <li style="text-align: center">
                                    <a class="list-group-item list-group-item-action disabled active">
                                        <i class="fa fa-television"></i><span class="nav-label">Monitoring
                                            User</span>
                                    </a>
                                </li>
                            @endif
                            @if (Auth::user()->getLevel->view_inoutlog)
                                <li class="{{ Route::currentRouteNamed('inout.log') ? 'active' : '' }}">
                                    <a href="{{ route('inout.log') }}">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <i class="fa fa-clipboard"></i>
                                            </div>
                                            <div class="col-md-10">
                                                <span class="nav-label">In-Out Log</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            @endif
                            {{-- <li class="{{ Route::currentRouteNamed('inout.report') ? 'active' : '' }}">
                                <a href="{{ route('inout.report') }}">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <i class="fa fa-file-text"></i>
                                        </div>
                                        <div class="col-md-10">
                                            <span class="nav-label">In-Out Report</span>
                                        </div>
                                    </div>
                                </a>
                            </li> --}}
                            @if (Auth::user()->getLevel->permission_edit_performance)
                                <li class="{{ Route::currentRouteNamed('perf.create') ? 'active' : '' }}">
                                    <a href="{{ route('perf.create') }}">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <i class="fa fa-plus"></i>
                                            </div>
                                            <div class="col-md-10">
                                                <span class="nav-label">Input Performance</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            @endif
                            @if (Auth::user()->getLevel->permission_view_performance)
                                <li class="{{ Route::currentRouteNamed('perf.index') ? 'active' : '' }}">
                                    <a href="{{ route('perf.index') }}">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <i class="fa fa-line-chart"></i>
                                            </div>
                                            <div class="col-md-10">
                                                <span class="nav-label">Performance Report</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            @endif

                            {{-- Project (Maintenance) --}}
                            <li style="text-align: center">
                                <a class="list-group-item list-group-item-action disabled active"><i
                                        class="fa fa-suitcase"></i><span class="nav-label">Project</span> </a>
                            </li>
                            <li class="{{ Route::currentRouteNamed('') ? 'active' : '' }}">
                                <a href="#">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <i class="fa fa-plus"></i>
                                        </div>
                                        <div class="col-md-10">
                                            <span class="nav-label">Create Project</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="{{ Route::currentRouteNamed('') ? 'active' : '' }}">
                                <a href="#">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <i class="fa fa-tasks"></i>
                                        </div>
                                        <div class="col-md-10">
                                            <span class="nav-label">Status Project</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        @endif
                    </ul>

                </div>
            </nav>

        @endauth

        <div id="page-wrapper" class="gray-bg" @guest style="width:inherit" @endguest>
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        @auth
                            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                                <i class="fa fa-bars"></i>
                            </a>
                        @endauth
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">Check In</a>
                                </li>
                            @endif
                        @else
                            @if (Auth::user()->GetLevel->edit_overtime)
                                <li class="nav-item dropdown">
                                    <a style="margin-right:15px" class="nav-link" href="#" id="navbarDropdown"
                                        role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-bell fa-lg"></i>
                                        @if (Auth::user()->GetLevel->edit_overtime)
                                            <span id="notifyme"></span>
                                        @endif
                                    </a>
                                    <ul class="dropdown-menu menu-style">
                                        <li class="head text-light" style="background:#2F4050">
                                            <div class="row">
                                                <div class="col-lg-12 col-sm-12 col-12">
                                                    <span>OT Request</span>
                                                </div>
                                            </div>
                                        </li>
                                        <div id="notifdetail"></div>
                                        <li style="background:#2F4050; padding:5px">
                                            <div class="row">
                                                <a class="col-lg-12 col-sm-12 col-12 text-center"
                                                    style="font-size:12px; color:white" href="{{ route('ot.index') }}">
                                                    View All
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            @endif
                            {{-- <li>
                                <a href="#" onclick="">
                                    <i class=""></i>
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    class="d-none">
                                    @csrf
                                </form>
                            </li> --}}
                        @endguest
                    </ul>
                </nav>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="m-t-lg m-b-lg">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('js/inspinia.js') }}"></script>
    <script src="{{ asset('js/plugins/pace/pace.min.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <script>
        $(document).ready(function() {
            @if (Session::has('msg'))
                toastr.options = {
                    "closeButton": true,
                    "progressBar": true,
                    "positionClass": "toast-bottom-right",
                }
                toastr.{{ Session::get('class') }}("{{ Session::get('msg') }}");
            @endif

            function myIntervalFunction() {
                $("#notifyme").load("../../notice .badge");
                $("#notifdetail").load("../../notice .notifdetail");
            }
            myIntervalFunction();
            realtime = setInterval(myIntervalFunction, 15000);

            $('button[type=submit]').click(function() {
                clearInterval(realtime);
            });
        });
    </script>

    @stack('scripts')
</body>

</html>
