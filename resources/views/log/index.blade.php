@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
    <style>
        label {
            font-weight: bold;
        }
    </style>
@endsection
@section('title')
    Log
@endsection
@section('content')
    <div class="">
        <div class="row justify-content-center">

            @include('layouts.notif')

            <div class="col-md-12 mt-2">
                <div class="card">
                    <div class="card-header">
                        Log
                    </div>
                    <div class="card-body">

                        {{-- filter:start --}}
                        <form class="row g-3" method="GET" autocomplete="off">
                            @php
                                use Carbon\Carbon;
                            @endphp

                            {{-- Date Range --}}
                            <div class="col-md-3">
                                <label>Dari Tanggal</label>
                                <input type="date" id="Sstart" class="form-control" name="start_date"
                                    value="@if(request('start_date')){{ request('start_date') }}@else{{ Carbon::now()->toDateString() }}@endif">
                            </div>
                            <div class="col-md-3">
                                <label>Sampai Tanggal</label>
                                <input type="date" id="Send" class="form-control" name="end_date"
                                    value="@if(request('end_date')){{ request('end_date') }}@else{{ Carbon::now()->toDateString() }}@endif">
                            </div>

                            <div class="col-md-2">
                                <label>Name</label>
                                <input name="Sname" value="{{ request('Sname') }}" type="search" id="Sname"
                                    class="form-control" placeholder="User...">
                            </div>

                            <div class="col-md-4">
                                <label>Description</label>
                                <input name="Sdesc" value="{{ request('Sdesc') }}" type="text" id="Sdesc"
                                    class="form-control" placeholder="Description...">
                            </div>

                            <div class="row pl-3 pt-2">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary mb-3">Search</button>
                                    <button type="submit" class="btn btn-danger mb-3 reset-btn">Reset</button>
                                </div>
                            </div>

                        </form>
                        {{-- filter:end --}}

                        <table id="logtable" class="table-responsive-lg text-center table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th class="">No</th>
                                    <th class="">Name</th>
                                    <th class="col-1 col-sm-1 col-md-1 col-lg-1 col-xl-1">Action</th>
                                    <th class="col-7 col-sm-7 col-md-7 col-lg-7 col-xl-7">Description</th>
                                    <th class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">DateTime</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                @endphp

                                @foreach ($applog as $applog)
                                    <tr>
                                        <td> {{ $i++ }} </td>
                                        <td>
                                            {{ $applog->GetUser->name }}
                                        </td>
                                        @if ($applog->type == 1)
                                            <td>
                                                <div style="padding:5px; border: 1px solid; border-radius:30px;"
                                                    class="btn-success">Create</div>
                                            </td>
                                        @elseif ($applog->type == 2)
                                            <td>
                                                <div style="padding:5px; border: 1px solid; border-radius:30px;"
                                                    class="btn-primary">Update</div>
                                            </td>
                                        @elseif ($applog->type == 3)
                                            <td>
                                                <div style="padding:5px; border: 1px solid; border-radius:30px; background-color:#00882f"
                                                    class="btn-primary">Approve</div>
                                            </td>
                                        @elseif ($applog->type == 4)
                                            <td>
                                                <div style="padding:5px; border: 1px solid; border-radius:30px;"
                                                    class="btn-danger">Reject</div>
                                            </td>
                                        @endif
                                        <td> {{ $applog->description }} </td>
                                        <td>
                                            @php
                                                $appDate = Carbon::createFromFormat('Y-m-d H:i:s', $applog->datetime);
                                            @endphp
                                            {{ $applog->datetime }}
                                            <br>
                                            <span style="font-size: 10px; color:gray">{{ $appDate->diffForHumans() }}
                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#logtable').DataTable({
                searching: false,
                pageLength: 100
            });
        });
        $('.reset-btn').on('click', function() {
            $('#Sstart').val(null).trigger("change");
            $('#Send').val(null).trigger("change");
            $('#Sname').val(null).trigger("change");
            $('#Stype').val(null).trigger("change");
            $('#Sdesc').val(null).trigger("change");
        });
    </script>
@endpush
