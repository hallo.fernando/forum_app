<?php

namespace Tests\Feature;

use App\Models\Divisi;
use App\Models\Level;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Tests\TestCase;

class UserTest extends TestCase
{
    use DatabaseTransactions;



    public $user;

    protected function setUp(): void
    {
        parent::setUp();
        Divisi::factory()->create();
        Level::factory()->create(['name'=>'testing',
        'permission_view_home' => 1,
        'permission_edit_home' => 1,
        'permission_view_user' => 1,
        'permission_edit_user' => 1,
        'permission_view_user_tingkatan' => 1,
        'permission_edit_user_tingkatan' => 1,
        'permission_view_divisi' => 1,
        'permission_edit_divisi' => 1,
        'permission_view_log' => 1,
        'permission_edit_log' => 1,
        'divisi' => 1]);
        $this->user = User::factory()->create([
            'name' => 'testing',
            'username' => 'testing123',
            'email' => 'testing@admin.com',
            'divisi' => 1,
            'status_level' => 1,
            'hak_akses_fitur' => 2,
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'already_change_password' => 1,
            'status' => 1,
            'status_work' => 1,
            'tanggal_gabung' => "2022-03-02 14:31:09",
            'remember_token' => Str::random(10),
        ]);
        $this->actingAs($this->user);
    }


    /**
     * A basic test example.
     *
     * @return void
     */

    public function testAllowUserIndex()
    {
        $response = $this->get(route('user.index'));
        $response->assertOk();
        $response->assertViewIs('user.index');
    }

    public function testAllowUserCreate()
    {
        $response = $this->get(route('user.create'));
        $response->assertOk();
        $response->assertViewIs('user.create');
    }

    public function testAllowUserStore()
    {
        $params = [
            'name' => 'testing2',
            'username' => 'testing2',
            'email' => 'testing2@admin.com',
            'divisi_id' => '1',
            'level_id' => '1',
            'hak_akses_fitur' => '1',
            'password' => 'password',
            'bank' => 'BCA',
            'gaji' => '123456',
            'nomor_rekening' => '123456',
            'office' => 'Office Depan',
            'um_harian' => '123456',
            'subsidi_lainnya' => '123456'
        ];
        $check = [
            'name' => 'testing2'
        ];

        $response = $this->post(route('user.store', $params));
        $response->assertRedirect(route('user.create'));

        $this->assertDatabaseHas('user', $check);
    }


    public function testAllowUserEdit()
    {
        $users = User::factory()->create([
            'name' => 'testing',
            'username' => 'testing12345',
            'email' => 'testings@admin.com',
            'divisi' => 1,
            'status_level' => 1,
            'hak_akses_fitur' => 2,
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'already_change_password' => 1,
            'status' => 1,
            'status_work' => 1,
            'tanggal_gabung' => "2022-03-02 14:31:09",
            'remember_token' => Str::random(10),
        ]);

        $response = $this->get(route('user.edit', $users->id));
        $response->assertOk();
        $response->assertViewIs('user.edit');
    }

    public function testAllowUserUpdate()
    {
        $users = User::factory()->create([
            'name' => 'testing',
            'username' => 'testing12345',
            'email' => 'testings@admin.com',
            'divisi' => 1,
            'status_level' => 1,
            'hak_akses_fitur' => 2,
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'already_change_password' => 1,
            'status' => 1,
            'status_work' => 1,
            'tanggal_gabung' => "2022-03-02 14:31:09",
            'remember_token' => Str::random(10),
        ]);

        $params = [
            'name' => 'jimmy fallon',
            'email' => 'testingsss@admin.com',
            'username' => 'testing123456',
            'divisi_id' => '1',
            'level_id' => '1',
            'bank' => 'BCA',
            'office' => 'Office Depan',
            'nomor_rekening' => '123123',
            'gaji' => '123123',
            'um_harian' => '123123',
            'subsidi_lainnya' => '123321',
            'hak_akses_id' => '1',
            'status_work' => '1'
        ];

        $response = $this->put(route('user.update', $users->id), $params);
        $response->assertRedirect(route('user.edit', $users->id));

        $check = [
            'name' => 'jimmy fallon'
        ];

        $this->assertDatabaseHas('user', $check);
    }

}
