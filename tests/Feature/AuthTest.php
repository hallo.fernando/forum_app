<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Str;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */

    public function test_user_can_view_a_login_form()
    {
        $response = $this->get('/login');
        $response->assertSuccessful();
        $response->assertViewIs('auth.login');
    }


    public function test_user_after_login_can_access_dashboard()
    {

        $this->user = User::factory()->create([
            'name' => 'testing',
            'username' => 'testing123',
            'email' => 'testing@admin.com',
            'divisi' => 1,
            'status_level' => 1,
            'hak_akses_fitur' => 2,
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'already_change_password' => 1,
            'status' => 1,
            'status_work' => 1,
            'tanggal_gabung' => "2022-03-02 14:31:09",
            'remember_token' => Str::random(10),
        ]);

        $this->actingAs($this->user);
        $this->assertAuthenticatedAs($this->user);

        $response = $this->get(route('home'));
        $response->assertOk();
        $response->assertViewIs('home');
    }
    public function test_user_cannot_login_if_wrong_credentials()
    {

        $this->user = User::factory()->create([
            'name' => 'testing',
            'username' => 'testing123',
            'email' => 'testing@admin.com',
            'divisi' => 1,
            'status_level' => 1,
            'hak_akses_fitur' => 2,
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'already_change_password' => 1,
            'status' => 1,
            'status_work' => 1,
            'tanggal_gabung' => "2022-03-02 14:31:09",
            'remember_token' => Str::random(10),
        ]);

        $credential = [
            'username' => 'testing123',
            'password' => 'wrongpassword'
        ];

        $response = $this->post('/login', $credential);

        $response->assertSessionHasErrors();
    }

    public function test_user_can_logout()
    {

        $this->user = User::factory()->create([
            'name' => 'testing',
            'username' => 'testing123',
            'email' => 'testing@admin.com',
            'divisi' => 1,
            'status_level' => 1,
            'hak_akses_fitur' => 2,
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'already_change_password' => 1,
            'status' => 1,
            'status_work' => 1,
            'tanggal_gabung' => "2022-03-02 14:31:09",
            'remember_token' => Str::random(10),
        ]);
        $this->actingAs($this->user);
        $this->assertAuthenticatedAs($this->user);
        $this->post('/logout');
        $response = $this->get(route('home'));
        $response->assertRedirect('/login');
    }
}
